import 'package:flutter/material.dart';

extension WidgetUtils on Widget {
  Widget wrapWithHero(String? tag) => tag != null ? Hero(tag: tag, child: this) : this;
}
