extension BoolUtils on bool {
  int asInt() => this ? 1 : 0;
  static int toInt(bool value) => value.asInt();
}
