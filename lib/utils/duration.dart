extension DurationUtils on Duration {
  static int toInt(Duration value) => value.inMilliseconds;

  int get daysFirstDigit {
    if (inDays == 0) {
      return 0;
    } else {
      return (inDays) ~/ 10;
    }
  }

  int get daysSecondDigit {
    if (inDays == 0) {
      return 0;
    } else {
      return (inDays) % 10;
    }
  }

  int get hoursFirstDigit {
    if (inHours == 0) {
      return 0;
    } else {
      return (inHours % 24) ~/ 10;
    }
  }

  int get hoursSecondDigit {
    if (inHours == 0) {
      return 0;
    } else {
      return (inHours % 24) % 10;
    }
  }

  int get minutesFirstDigit {
    if (inMinutes == 0) {
      return 0;
    } else {
      return (inMinutes % 60) ~/ 10;
    }
  }

  int get minutesSecondDigit {
    if (inMinutes == 0) {
      return 0;
    } else {
      return (inMinutes % 60) % 10;
    }
  }

  int get secondsFirstDigit {
    if (inSeconds == 0) {
      return 0;
    } else {
      return (inSeconds % 60) ~/ 10;
    }
  }

  int get secondsSecondDigit {
    if (inSeconds == 0) {
      return 0;
    } else {
      return (inSeconds % 60) % 10;
    }
  }
}
