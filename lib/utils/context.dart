import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/config/translations/gen/l10n.dart';

extension ContextUtils on BuildContext {
  // TRANSLATION
  Translation get translation => Translation.of(this);

  // ROUTER
  void Function()? get popRoute => Navigator.of(this).canPop() ? Navigator.of(this).pop : null;
  String get location => GoRouter.of(this).location;

  // THEME
  ThemeData get theme => Theme.of(this);
  TextTheme get textTheme => theme.textTheme;
  Color get primaryColor => theme.colorScheme.primary;
  Color get secondaryColor => theme.colorScheme.secondary;

  // SIZES
  MediaQueryData get mediaQuery => MediaQuery.of(this);
  Size get viewSize => MediaQuery.of(this).size;
  EdgeInsets get viewInsets => MediaQuery.of(this).viewInsets;
  EdgeInsets get viewPadding => MediaQuery.of(this).viewPadding;
  Size get screenSize => Size(viewSize.width + viewPadding.horizontal + viewInsets.horizontal,
      viewSize.height + viewPadding.vertical + viewInsets.vertical);

  // MODAL
  Future<Object?> showModal(
    Widget modal, {
    bool barrierDismissible = true,
    bool onWillPop = true,
    Color barrierColor = const Color(0x80000000),
  }) async {
    final dialog = await showGeneralDialog(
      context: this,
      barrierDismissible: barrierDismissible,
      barrierLabel: barrierDismissible ? 'Close' : null,
      barrierColor: barrierColor,
      pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
      ) =>
          WillPopScope(
        onWillPop: () => Future.value(onWillPop),
        child: modal,
      ),
    );
    return dialog;
  }

  // SNACKBAR
  void showSuccessSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.check_circle_outline_rounded, color: positiveColor),
        action: action,
        isInfinite: isInfinite,
      );

  void showInformationSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.info_outline_rounded, color: informativeColor),
        action: action,
        isInfinite: isInfinite,
      );

  void showWarningSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.warning_amber_rounded, color: warningColor),
        action: action,
        isInfinite: isInfinite,
      );

  void showErrorSnackBar(
    String text, {
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      showCustomSnackBar(
        text: text,
        icon: Icon(Icons.error_outline_rounded, color: negativeColor),
        action: action,
        isInfinite: isInfinite,
      );

  void showCustomSnackBar({
    required String text,
    required Widget icon,
    maxLines = 3,
    SnackBarAction? action,
    bool isInfinite = false,
  }) =>
      ScaffoldMessenger.of(this).showSnackBar(
        SnackBar(
          content: Row(
            children: [
              icon,
              $Gap.m,
              Expanded(
                child: AutoSizeText(text, style: textTheme.bodyText1, maxLines: maxLines),
              ),
            ],
          ),
          width: $Screen.size(screenSize.width) > s ? $MaxWidth.m : null,
          margin: $Screen.size(screenSize.width) <= s ? const EdgeInsets.all($Padding.m) : null,
          behavior: SnackBarBehavior.floating,
          backgroundColor: whiteColor,
          action: action,
          duration: isInfinite ? const Duration(days: 365) : const Duration(seconds: 4),
          dismissDirection: isInfinite ? DismissDirection.none : DismissDirection.down,
        ),
      );
}
