import 'package:dartx/dartx.dart';

extension StringUtils on String {
  bool get isValidName => RegExp(r"^[\p{L} ,.'-]*$", caseSensitive: false, unicode: true, dotAll: true).hasMatch(this);
  bool get isValidUrl {
    if (isEmpty || isBlank) return false;
    final uri = Uri.tryParse(this);
    return uri != null && uri.hasAbsolutePath && uri.scheme.startsWith('http');
  }
}
