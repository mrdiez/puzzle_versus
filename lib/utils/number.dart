extension IntUtils on int {
  bool asBool() => this > 0;
  static bool toBool(int value) => value.asBool();
  Duration asDuration() => Duration(milliseconds: this);
  static Duration toDuration(int value) => value.asDuration();
}
