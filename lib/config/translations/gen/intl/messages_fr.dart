// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a fr locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'fr';

  static String m0(moves) =>
      "${Intl.plural(moves, zero: 'Mouvement', one: 'Mouvement', other: 'Mouvements')}";

  static String m1(nb) => "${nb} caractères minimum";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____GAME_MODULE_ERRORS_____________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____GAME_MODULE_____________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME_VIEW_______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("fr"),
        "cancel": MessageLookupByLibrary.simpleMessage("Annuler"),
        "cheaterMode": MessageLookupByLibrary.simpleMessage("Mode tricheur"),
        "easy": MessageLookupByLibrary.simpleMessage("Facile"),
        "hard": MessageLookupByLibrary.simpleMessage("Difficile"),
        "invalidUrlOrProtectedImage": MessageLookupByLibrary.simpleMessage(
            "L\'url est invalide ou l\'image est protégée"),
        "localPlayer": MessageLookupByLibrary.simpleMessage("Local player"),
        "locale": MessageLookupByLibrary.simpleMessage("fr"),
        "moves": m0,
        "nbCharactersMinimum": m1,
        "networkError": MessageLookupByLibrary.simpleMessage(
            "Erreur réseau, merci de vérifier votre connexion."),
        "normal": MessageLookupByLibrary.simpleMessage("Normal"),
        "notAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Certains caractères ne sont pas autorisés"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "onlinePlayer": MessageLookupByLibrary.simpleMessage("Online player"),
        "orPasteImageUrlHere": MessageLookupByLibrary.simpleMessage(
            "OU collez ici l\'adresse URL d\'une image"),
        "playAgainsTheClock":
            MessageLookupByLibrary.simpleMessage("Jouez contre la montre"),
        "playTogetherOnSameDevice": MessageLookupByLibrary.simpleMessage(
            "Jouez à deux sur le même appareil"),
        "playVersusWorldsPlayers": MessageLookupByLibrary.simpleMessage(
            "Affrontez les meilleurs joueurs du monde"),
        "pleaseCheckGalleryAccessSettings": MessageLookupByLibrary.simpleMessage(
            "Veuillez vérifier que vos paramètres autorisent cette application à accéder à votre galerie"),
        "sendMyScore":
            MessageLookupByLibrary.simpleMessage("Envoyer mon score"),
        "sorryModeNotReadyYet": MessageLookupByLibrary.simpleMessage(
            "Désolé ce mode de jeu est en cours de développement et il n\'est pas encore prêt."),
        "start": MessageLookupByLibrary.simpleMessage("Démarrer"),
        "submit": MessageLookupByLibrary.simpleMessage("Envoyer"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("Ce champ est obligatoire"),
        "thisIsNotAValidUrl": MessageLookupByLibrary.simpleMessage(
            "Ce n\'est pas une URL valide"),
        "time": MessageLookupByLibrary.simpleMessage("Time"),
        "typeYourNameHere":
            MessageLookupByLibrary.simpleMessage("Ecrivez votre nom ici"),
        "unableToAccessImage": MessageLookupByLibrary.simpleMessage(
            "Impossible d\'accéder à l\'image"),
        "unableToSaveImage": MessageLookupByLibrary.simpleMessage(
            "Impossible d\'enregistrer l\'image"),
        "unableToSendScore": MessageLookupByLibrary.simpleMessage(
            "Impossible d\'envoyer votre score, merci de vérifier votre connexion"),
        "unknownErrorWhileSplittingImage": MessageLookupByLibrary.simpleMessage(
            "Une erreur inconnue est survenue lors du découpage de l\'image"),
        "uploadAnImage":
            MessageLookupByLibrary.simpleMessage("Importer une image"),
        "youWin": MessageLookupByLibrary.simpleMessage("Vous avez gagné !")
      };
}
