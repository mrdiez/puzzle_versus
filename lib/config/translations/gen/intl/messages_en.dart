// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(moves) =>
      "${Intl.plural(moves, zero: 'Move', one: 'Move', other: 'Moves')}";

  static String m1(nb) => "${nb} characters minimum";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "_____COMMON_ERRORS___________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____COMMON__________________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_____HOME_VIEW_______________________________":
            MessageLookupByLibrary.simpleMessage(""),
        "_locale": MessageLookupByLibrary.simpleMessage("en"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cheaterMode": MessageLookupByLibrary.simpleMessage("Cheater mode"),
        "easy": MessageLookupByLibrary.simpleMessage("Easy"),
        "hard": MessageLookupByLibrary.simpleMessage("Hard"),
        "invalidUrlOrProtectedImage": MessageLookupByLibrary.simpleMessage(
            "Provided URL is invalid or image is protected"),
        "localPlayer": MessageLookupByLibrary.simpleMessage("Local player"),
        "locale": MessageLookupByLibrary.simpleMessage("en"),
        "moves": m0,
        "nbCharactersMinimum": m1,
        "networkError": MessageLookupByLibrary.simpleMessage(
            "Network error, please check your connectivity"),
        "normal": MessageLookupByLibrary.simpleMessage("Normal"),
        "notAllowedCharacters": MessageLookupByLibrary.simpleMessage(
            "Some characters are not allowed"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "onlinePlayer": MessageLookupByLibrary.simpleMessage("Online player"),
        "orPasteImageUrlHere":
            MessageLookupByLibrary.simpleMessage("OR paste an image url here"),
        "playAgainsTheClock":
            MessageLookupByLibrary.simpleMessage("Play against the clock"),
        "playTogetherOnSameDevice": MessageLookupByLibrary.simpleMessage(
            "Play together on the same device"),
        "playVersusWorldsPlayers": MessageLookupByLibrary.simpleMessage(
            "Play versus world\'s best players"),
        "pleaseCheckGalleryAccessSettings": MessageLookupByLibrary.simpleMessage(
            "Please check that your settings allow this app to access your gallery"),
        "sendMyScore": MessageLookupByLibrary.simpleMessage("Send my score"),
        "sorryModeNotReadyYet": MessageLookupByLibrary.simpleMessage(
            "Sorry this game mode is still under development and not ready yet"),
        "start": MessageLookupByLibrary.simpleMessage("Start"),
        "submit": MessageLookupByLibrary.simpleMessage("Submit"),
        "thisFieldIsRequired":
            MessageLookupByLibrary.simpleMessage("This field is required"),
        "thisIsNotAValidUrl":
            MessageLookupByLibrary.simpleMessage("This is not a valid url"),
        "time": MessageLookupByLibrary.simpleMessage("Time"),
        "typeYourNameHere":
            MessageLookupByLibrary.simpleMessage("Type your name here"),
        "unableToAccessImage":
            MessageLookupByLibrary.simpleMessage("Unable to access image"),
        "unableToSaveImage":
            MessageLookupByLibrary.simpleMessage("Unable to save image"),
        "unableToSendScore": MessageLookupByLibrary.simpleMessage(
            "Unable to send score, please check connectivity"),
        "unknownErrorWhileSplittingImage": MessageLookupByLibrary.simpleMessage(
            "An unknown error occured while splitting image"),
        "uploadAnImage":
            MessageLookupByLibrary.simpleMessage("Upload an image"),
        "youWin": MessageLookupByLibrary.simpleMessage("You win !")
      };
}
