// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class Translation {
  Translation();

  static Translation? _current;

  static Translation get current {
    assert(_current != null,
        'No instance of Translation was loaded. Try to initialize the Translation delegate before accessing Translation.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<Translation> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = Translation();
      Translation._current = instance;

      return instance;
    });
  }

  static Translation of(BuildContext context) {
    final instance = Translation.maybeOf(context);
    assert(instance != null,
        'No instance of Translation present in the widget tree. Did you add Translation.delegate in localizationsDelegates?');
    return instance!;
  }

  static Translation? maybeOf(BuildContext context) {
    return Localizations.of<Translation>(context, Translation);
  }

  /// `fr`
  String get _locale {
    return Intl.message(
      'fr',
      name: '_locale',
      desc: '',
      args: [],
    );
  }

  /// `fr`
  String get locale {
    return Intl.message(
      'fr',
      name: 'locale',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON__________________________________ {
    return Intl.message(
      '',
      name: '_____COMMON__________________________________',
      desc: '',
      args: [],
    );
  }

  /// `Annuler`
  String get cancel {
    return Intl.message(
      'Annuler',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Envoyer`
  String get submit {
    return Intl.message(
      'Envoyer',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____COMMON_ERRORS___________________________ {
    return Intl.message(
      '',
      name: '_____COMMON_ERRORS___________________________',
      desc: '',
      args: [],
    );
  }

  /// `{nb} caractères minimum`
  String nbCharactersMinimum(Object nb) {
    return Intl.message(
      '$nb caractères minimum',
      name: 'nbCharactersMinimum',
      desc: '',
      args: [nb],
    );
  }

  /// `Ce champ est obligatoire`
  String get thisFieldIsRequired {
    return Intl.message(
      'Ce champ est obligatoire',
      name: 'thisFieldIsRequired',
      desc: '',
      args: [],
    );
  }

  /// `Certains caractères ne sont pas autorisés`
  String get notAllowedCharacters {
    return Intl.message(
      'Certains caractères ne sont pas autorisés',
      name: 'notAllowedCharacters',
      desc: '',
      args: [],
    );
  }

  /// `Erreur réseau, merci de vérifier votre connexion.`
  String get networkError {
    return Intl.message(
      'Erreur réseau, merci de vérifier votre connexion.',
      name: 'networkError',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____HOME_VIEW_______________________________ {
    return Intl.message(
      '',
      name: '_____HOME_VIEW_______________________________',
      desc: '',
      args: [],
    );
  }

  /// `Time`
  String get time {
    return Intl.message(
      'Time',
      name: 'time',
      desc: '',
      args: [],
    );
  }

  /// `Jouez contre la montre`
  String get playAgainsTheClock {
    return Intl.message(
      'Jouez contre la montre',
      name: 'playAgainsTheClock',
      desc: '',
      args: [],
    );
  }

  /// `Local player`
  String get localPlayer {
    return Intl.message(
      'Local player',
      name: 'localPlayer',
      desc: '',
      args: [],
    );
  }

  /// `Jouez à deux sur le même appareil`
  String get playTogetherOnSameDevice {
    return Intl.message(
      'Jouez à deux sur le même appareil',
      name: 'playTogetherOnSameDevice',
      desc: '',
      args: [],
    );
  }

  /// `Online player`
  String get onlinePlayer {
    return Intl.message(
      'Online player',
      name: 'onlinePlayer',
      desc: '',
      args: [],
    );
  }

  /// `Affrontez les meilleurs joueurs du monde`
  String get playVersusWorldsPlayers {
    return Intl.message(
      'Affrontez les meilleurs joueurs du monde',
      name: 'playVersusWorldsPlayers',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____GAME_MODULE_____________________________ {
    return Intl.message(
      '',
      name: '_____GAME_MODULE_____________________________',
      desc: '',
      args: [],
    );
  }

  /// `Facile`
  String get easy {
    return Intl.message(
      'Facile',
      name: 'easy',
      desc: '',
      args: [],
    );
  }

  /// `Normal`
  String get normal {
    return Intl.message(
      'Normal',
      name: 'normal',
      desc: '',
      args: [],
    );
  }

  /// `Difficile`
  String get hard {
    return Intl.message(
      'Difficile',
      name: 'hard',
      desc: '',
      args: [],
    );
  }

  /// `Importer une image`
  String get uploadAnImage {
    return Intl.message(
      'Importer une image',
      name: 'uploadAnImage',
      desc: '',
      args: [],
    );
  }

  /// `OU collez ici l'adresse URL d'une image`
  String get orPasteImageUrlHere {
    return Intl.message(
      'OU collez ici l\'adresse URL d\'une image',
      name: 'orPasteImageUrlHere',
      desc: '',
      args: [],
    );
  }

  /// `Ce n'est pas une URL valide`
  String get thisIsNotAValidUrl {
    return Intl.message(
      'Ce n\'est pas une URL valide',
      name: 'thisIsNotAValidUrl',
      desc: '',
      args: [],
    );
  }

  /// `Mode tricheur`
  String get cheaterMode {
    return Intl.message(
      'Mode tricheur',
      name: 'cheaterMode',
      desc: '',
      args: [],
    );
  }

  /// `Démarrer`
  String get start {
    return Intl.message(
      'Démarrer',
      name: 'start',
      desc: '',
      args: [],
    );
  }

  /// `{moves,plural, zero{Mouvement}one{Mouvement}other{Mouvements}}`
  String moves(num moves) {
    return Intl.plural(
      moves,
      zero: 'Mouvement',
      one: 'Mouvement',
      other: 'Mouvements',
      name: 'moves',
      desc: '',
      args: [moves],
    );
  }

  /// `Vous avez gagné !`
  String get youWin {
    return Intl.message(
      'Vous avez gagné !',
      name: 'youWin',
      desc: '',
      args: [],
    );
  }

  /// `Envoyer mon score`
  String get sendMyScore {
    return Intl.message(
      'Envoyer mon score',
      name: 'sendMyScore',
      desc: '',
      args: [],
    );
  }

  /// `Ecrivez votre nom ici`
  String get typeYourNameHere {
    return Intl.message(
      'Ecrivez votre nom ici',
      name: 'typeYourNameHere',
      desc: '',
      args: [],
    );
  }

  /// `Désolé ce mode de jeu est en cours de développement et il n'est pas encore prêt.`
  String get sorryModeNotReadyYet {
    return Intl.message(
      'Désolé ce mode de jeu est en cours de développement et il n\'est pas encore prêt.',
      name: 'sorryModeNotReadyYet',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get _____GAME_MODULE_ERRORS_____________________ {
    return Intl.message(
      '',
      name: '_____GAME_MODULE_ERRORS_____________________',
      desc: '',
      args: [],
    );
  }

  /// `Impossible d'enregistrer l'image`
  String get unableToSaveImage {
    return Intl.message(
      'Impossible d\'enregistrer l\'image',
      name: 'unableToSaveImage',
      desc: '',
      args: [],
    );
  }

  /// `Impossible d'accéder à l'image`
  String get unableToAccessImage {
    return Intl.message(
      'Impossible d\'accéder à l\'image',
      name: 'unableToAccessImage',
      desc: '',
      args: [],
    );
  }

  /// `Veuillez vérifier que vos paramètres autorisent cette application à accéder à votre galerie`
  String get pleaseCheckGalleryAccessSettings {
    return Intl.message(
      'Veuillez vérifier que vos paramètres autorisent cette application à accéder à votre galerie',
      name: 'pleaseCheckGalleryAccessSettings',
      desc: '',
      args: [],
    );
  }

  /// `L'url est invalide ou l'image est protégée`
  String get invalidUrlOrProtectedImage {
    return Intl.message(
      'L\'url est invalide ou l\'image est protégée',
      name: 'invalidUrlOrProtectedImage',
      desc: '',
      args: [],
    );
  }

  /// `Une erreur inconnue est survenue lors du découpage de l'image`
  String get unknownErrorWhileSplittingImage {
    return Intl.message(
      'Une erreur inconnue est survenue lors du découpage de l\'image',
      name: 'unknownErrorWhileSplittingImage',
      desc: '',
      args: [],
    );
  }

  /// `Impossible d'envoyer votre score, merci de vérifier votre connexion`
  String get unableToSendScore {
    return Intl.message(
      'Impossible d\'envoyer votre score, merci de vérifier votre connexion',
      name: 'unableToSendScore',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<Translation> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'fr'),
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<Translation> load(Locale locale) => Translation.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
