import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';

enum Sizes { zero, xxxs, xxs, xs, s, m, l, xl, xxl, xxxl }

/// PADDING (spaces around a widget)
abstract class $Padding {
  static const double zero = 0;
  static const double xxxs = 2.5;
  static const double xxs = 5;
  static const double xs = 7.5;
  static const double s = 15;
  static const double m = 20;
  static const double l = 25;
  static const double xl = 30;
  static const double xxl = 40;
  static const double xxxl = 50;
}

/// GAP (spaces between Column or Row items)
abstract class $Gap {
  static const zero = Gap(0);
  static const xxxs = Gap(2.5);
  static const xxs = Gap(5);
  static const xs = Gap(10);
  static const s = Gap(15);
  static const m = Gap(20);
  static const l = Gap(25);
  static const xl = Gap(30);
  static const xxl = Gap(40);
  static const xxxl = Gap(50);
}

/// RADIUS (rounding amount for rounded containers)
abstract class $Radius {
  static const zero = Radius.circular(0);
  static const xxxs = Radius.circular(2.5);
  static const xxs = Radius.circular(5);
  static const xs = Radius.circular(10);
  static const s = Radius.circular(15);
  static const m = Radius.circular(20);
  static const l = Radius.circular(25);
  static const xl = Radius.circular(30);
  static const xxl = Radius.circular(40);
  static const xxxl = Radius.circular(50);
}

/// SCREEN (layout's screen size steps)
abstract class $Screen {
  static const _availableSizes = [zero, xxxs, xxs, xs, s, m, l, xl, xxl, xxxl];

  static const double zero = 0;
  static const double xxxs = 260;
  static const double xxs = 400;
  static const double xs = 500;
  static const double s = 640;
  static const double m = 720;
  static const double l = 900;
  static const double xl = 1080;
  static const double xxl = 1440;
  static const double xxxl = 2160;

  static Sizes size(double value) => SizesUtils.getSizeFromValue(value, _availableSizes);
}

/// ICON SIZE (icon preset sizes)
abstract class $Icon {
  static const double zero = 0;
  static const double xxxs = 10;
  static const double xxs = 15;
  static const double xs = 20;
  static const double s = 25;
  static const double m = 35;
  static const double l = 50;
  static const double xl = 75;
  static const double xxl = 100;
  static const double xxxl = 150;
}

/// MAX ITEM WIDTH (limited width for expandable items)
abstract class $MaxWidth {
  static const double zero = 0;
  static const double xxxs = $Screen.xxs / 2;
  static const double xxs = $Screen.xxxs;
  static const double xs = $Screen.xxs;
  static const double s = $Screen.xs;
  static const double m = $Screen.s;
  static const double l = $Screen.m;
  static const double xl = $Screen.l;
  static const double xxl = $Screen.xl;
  static const double xxxl = $Screen.xxl;
}

/// LIST ITEM HEIGHT (max list items' height)
abstract class $ItemHeight {
  static const double xs = 50;
  static const double s = 70;
  static const double m = 90;
  static const double l = 110;
}

/// BUTTON HEIGHT (buttons' height)
abstract class $ButtonHeight {
  static const double s = 40;
  static const double m = 50;
  static const double l = 60;
}

/// INPUT HEIGHT (inputs' height)
abstract class $InputHeight {
  static const double s = 40;
  static const double m = 50;
  static const double l = 60;
}

/// GAME IMAGE SIZE (limit max size for performances)
abstract class $GameImageSize {
  static const int max = 640;
}

/// FONT (default fonts' size)
abstract class $Font {
  static const double zero = 0;
  static const double xxxs = 6;
  static const double xxs = 8;
  static const double xs = 12;
  static const double s = 16;
  static const double m = 20;
  static const double l = 25;
  static const double xl = 30;
  static const double xxl = 40;
  static const double xxxl = 50;
}

/// LOGO HEIGHT (main logo's height)
abstract class $LogoHeight {
  static const double xxs = 25;
  static const double xs = 35;
  static const double s = 50;
  static const double m = 75;
  static const double l = 100;
  static const double xl = 150;
  static const double xxl = 200;
}

/// APP BAR HEIGHT (main app bar's height)
abstract class $AppBarHeight {
  static const double xs = 50;
  static const double s = 60;
  static const double m = 70;
  static const double l = 80;
}

/// SHADOW SIZE (define common shadow's size)
abstract class $ShadowSize {
  static const double xs = 1;
  static const double s = 2.5;
  static const double m = 5;
}

/// ALIGNMENT (for people that don't need common alignments)
abstract class $Alignment {
  static const topLeftGradient = Alignment(-0.275, -3);
  static const bottomRightGradient = Alignment(0.275, 3);
}

/// DURATION (should I really describe this)
abstract class $Duration {
  static const instant = Duration.zero;
  static const almostInstant = Duration(milliseconds: 50);
  static const veryfast = Duration(milliseconds: 250);
  static const fast = Duration(milliseconds: 350);
  static const normal = Duration(milliseconds: 500);
  static const aBitLonger = Duration(milliseconds: 750);
  static const longer = Duration(seconds: 1);
  static const veryLonger = Duration(seconds: 2);
  static const lazy = Duration(seconds: 3);
  static const eternity = Duration(seconds: 10);
}

/// GOLDEN RATIO (for developers who also love design)
/// Check this to understand what is golden ratio and why we'll use it:
/// https://www.invisionapp.com/inside-design/golden-ratio-designers/
abstract class $GoldenRatio {
  static const zero = 0;
  static final xxs = 1 / _phi(4);
  static final xs = 1 / _phi(3);
  static final s = 1 / _phi(2);
  static const m = 1;
  static final l = phi;
  static final xl = _phi(2);
  static final xxl = _phi(3);
  static final xxxl = _phi(4);

  /// Phi is also known as the golden number
  static double phi = (1 + sqrt(5)) / 2;

  /// A method that return [phi] to the power of the given exponent
  static double _phi(int exponent) => exponent > 1 ? phi : pow(phi, exponent).toDouble();
}

extension SizesUtils on Sizes {
  Sizes operator +(int i) => Sizes.values[Sizes.values.indexOf(this) + i];
  bool operator >(Sizes size) => Sizes.values.indexOf(this) > Sizes.values.indexOf(size);
  bool operator <(Sizes size) => Sizes.values.indexOf(this) < Sizes.values.indexOf(size);
  bool operator >=(Sizes size) => Sizes.values.indexOf(this) >= Sizes.values.indexOf(size);
  bool operator <=(Sizes size) => Sizes.values.indexOf(this) <= Sizes.values.indexOf(size);

  static Sizes getSizeFromValue(double value, List<double> availableSizes) {
    assert(availableSizes.isNotEmpty);
    var i = 0;
    while (value >= availableSizes[i] && i < availableSizes.length - 1) {
      i++;
    }
    return Sizes.values[i];
  }
}

const zero = Sizes.zero;
const xxxs = Sizes.xxxs;
const xxs = Sizes.xxs;
const xs = Sizes.xs;
const s = Sizes.s;
const m = Sizes.m;
const l = Sizes.l;
const xl = Sizes.xl;
const xxl = Sizes.xxl;
const xxxl = Sizes.xxxl;
