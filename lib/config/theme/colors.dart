import 'package:flutter/material.dart';
import 'package:ms_material_color/ms_material_color.dart';
import 'package:puzzle_versus/config/enums.dart';

final primaryColor = blueColor;
final secondaryColor = yellowColor;

final blueColor = MsMaterialColor(0xff8888EB);
final yellowColor = MsMaterialColor(0xffFCA311);
final redColor = MsMaterialColor(0xffBF4D69);
final greenColor = MsMaterialColor(0xffBCCA6B);
final pinkColor = MsMaterialColor(0xffC289B7);

final positiveColor = greenColor;
final negativeColor = redColor;
final warningColor = yellowColor;
final informativeColor = blueColor;

const headerColor = whiteColor;
final backgroundColor = lightColor;

final lightShadowColor = whiteColor.withOpacity(0.75);
final darkShadowColor = lightGreyColor;

const blackColor = Colors.black;
final darkColor = MsMaterialColor(0xff414141);
final darkGreyColor = MsMaterialColor(0xff747474);
final greyColor = MsMaterialColor(0xffA7A7A7);
final lightGreyColor = MsMaterialColor(0xffD9D9D9);
final lightColor = MsMaterialColor(0xffECEBE8);
const whiteColor = Colors.white;
const transparentColor = Colors.transparent;

final gameColors = {
  GameMode.time: pinkColor,
  GameMode.local: greenColor,
  GameMode.online: redColor,
};
