import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';

import 'colors.dart';

ThemeData defaultTheme = getTheme(primaryColor, secondaryColor, backgroundColor);

ThemeData getTheme(MaterialColor _primaryColor, MaterialColor _secondaryColor, MaterialColor _backgroundColor) =>
    ThemeData(
      brightness: Brightness.light,
      backgroundColor: _backgroundColor,
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        backgroundColor: _primaryColor,
        selectedItemColor: whiteColor,
        selectedIconTheme: const IconThemeData(color: blackColor),
        unselectedIconTheme: const IconThemeData(color: whiteColor),
        unselectedLabelStyle: const TextStyle(color: whiteColor),
      ),
      colorScheme: ColorScheme.fromSwatch(
        accentColor: _secondaryColor,
        backgroundColor: _backgroundColor,
        brightness: Brightness.light,
        primarySwatch: _primaryColor,
        errorColor: negativeColor,
      ),
      iconTheme: IconThemeData(color: darkGreyColor),
      inputDecorationTheme: InputDecorationTheme(
        errorStyle: TextStyle(
          color: negativeColor,
        ),
      ),
      primarySwatch: _primaryColor,
      scaffoldBackgroundColor: _backgroundColor,
      textTheme: TextTheme(
        bodyText1: TextStyle(
          fontSize: $Font.m,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        bodyText2: TextStyle(
          fontSize: $Font.s,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        button: TextStyle(
          fontSize: $Font.m,
          fontWeight: FontWeight.w600,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        caption: TextStyle(
          fontSize: $Font.s,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline1: const TextStyle(
          fontSize: $Font.xxxl,
          fontWeight: FontWeight.w900,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline2: const TextStyle(
          fontSize: $Font.xxl,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline3: const TextStyle(
          fontSize: $Font.xl,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline4: const TextStyle(
          fontSize: $Font.l,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline5: const TextStyle(
          fontSize: $Font.m,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        headline6: const TextStyle(
          fontSize: $Font.s,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        subtitle1: TextStyle(
          fontSize: $Font.xl,
          fontWeight: FontWeight.w600,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
        subtitle2: TextStyle(
          fontSize: $Font.m,
          fontWeight: FontWeight.w600,
          color: darkGreyColor,
          overflow: TextOverflow.ellipsis,
          height: 1,
        ),
      ),
    );
