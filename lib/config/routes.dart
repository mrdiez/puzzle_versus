import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_view.dart';
import 'package:puzzle_versus/modules/game/play/game_play_view.dart';
import 'package:puzzle_versus/modules/game/win/game_win_view.dart';
import 'package:puzzle_versus/modules/home/home_view.dart';

// App routes
const notFoundRoute = '/404';
const loginRoute = '/login';
const homeRoute = '/home';
const gameRoute = '/game';
const _playRoute = 'play';
const _winRoute = 'win';
const gameRoutes = {
  GameMode.time: '/time',
  GameMode.local: '/local',
  GameMode.online: '/online',
};
final playRoutes = {
  GameMode.time: '${gameRoutes[GameMode.time]}/$_playRoute',
  GameMode.local: '${gameRoutes[GameMode.local]}/$_playRoute',
  GameMode.online: '${gameRoutes[GameMode.online]}/$_playRoute',
};
final winRoutes = {
  GameMode.time: '${gameRoutes[GameMode.time]}/$_winRoute',
  GameMode.local: '${gameRoutes[GameMode.local]}/$_winRoute',
  GameMode.online: '${gameRoutes[GameMode.online]}/$_winRoute',
};
// The error 404 page content
const notFoundScreen = Material(child: Center(child: Text('404')));
const notFoundPage = MaterialPage(child: notFoundScreen);

final routes = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      redirect: (_) => homeRoute,
    ),
    GoRoute(
      path: homeRoute,
      builder: (context, state) => const HomeView(),
    ),
    GoRoute(
      path: gameRoute,
      redirect: (state) => state.extra is GameMode ? gameRoutes[state.extra] : notFoundRoute,
    ),
    GoRoute(
      path: gameRoutes[GameMode.time]!,
      builder: (context, state) => const GameCreationView(GameMode.time),
      routes: [
        GoRoute(
          path: _playRoute,
          builder: (context, state) => const GamePlayView(GameMode.time),
        ),
        GoRoute(
          path: _winRoute,
          builder: (context, state) => const GameWinView(GameMode.time),
        ),
      ],
    ),
    GoRoute(
      path: gameRoutes[GameMode.local]!,
      builder: (context, state) => const GameCreationView(GameMode.local),
      routes: [
        // GoRoute(
        //   path: _playRoute,
        //   builder: (context, state) => const GamePlayView(GameMode.local),
        // ),
        // GoRoute(
        //   path: _winRoute,
        //   builder: (context, state) => const GameWinView(GameMode.local),
        // ),
      ],
    ),
    GoRoute(
      path: gameRoutes[GameMode.online]!,
      builder: (context, state) => const GameCreationView(GameMode.online),
      routes: [
        // GoRoute(
        //   path: _playRoute,
        //   builder: (context, state) => const GamePlayView(GameMode.online),
        // ),
        // GoRoute(
        //   path: _winRoute,
        //   builder: (context, state) => const GameWinView(GameMode.online),
        // ),
      ],
    ),
  ],
  errorPageBuilder: (context, state) => notFoundPage,
);
