abstract class Asset {
  static const verticalLogo = 'assets/logos/logo_vertical.png';
  static const horizontalLogo = 'assets/logos/logo_horizontal.png';
}
