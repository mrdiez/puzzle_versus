import 'package:freezed_annotation/freezed_annotation.dart';

enum GameMode {
  @JsonValue(1)
  time,
  @JsonValue(2)
  local,
  @JsonValue(3)
  online,
}

enum GameLevel {
  @JsonValue(1)
  easy,
  @JsonValue(2)
  normal,
  @JsonValue(3)
  hard,
}

const notReadyModes = [GameMode.local, GameMode.online];
