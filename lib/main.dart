import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/config/theme/theme.dart';
import 'package:puzzle_versus/config/translations/gen/l10n.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  // Lock rotation on small screens
  final windowSize = MediaQueryData.fromWindow(window).size;
  if ($Screen.size(min(windowSize.height, windowSize.width)) < xs) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      theme: defaultTheme,
      localizationsDelegates: const [
        Translation.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: Translation.delegate.supportedLocales,
      routeInformationParser: routes.routeInformationParser,
      routerDelegate: routes.routerDelegate,
    );
  }
}
