import 'package:flutter/material.dart';

class MouseListener extends StatefulWidget {
  const MouseListener({Key? key, required this.builder, this.onTapDown, this.onTapUp, this.onHover}) : super(key: key);

  final Function()? onTapDown;
  final Function()? onTapUp;
  final Function()? onHover;
  final Widget Function(BuildContext context, bool isHovered, bool isClicked) builder;

  @override
  _MouseListenerState createState() => _MouseListenerState();
}

class _MouseListenerState extends State<MouseListener> {
  bool hovered = false;
  bool clicked = false;

  void setHovered(bool isHovered) => setState(() => hovered = isHovered);
  void setClicked(bool isClicked) => setState(() => clicked = isClicked);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.deferToChild,
      onTapDown: (_) {
        setClicked(true);
        widget.onTapDown?.call();
      },
      onTapUp: (_) {
        setClicked(false);
        widget.onTapUp?.call();
      },
      onTapCancel: () {
        setClicked(false);
      },
      child: MouseRegion(
          cursor: SystemMouseCursors.click,
          onEnter: (_) {
            setHovered(true);
            widget.onHover?.call();
          },
          onExit: (_) => setHovered(false),
          child: widget.builder(context, hovered, clicked)),
    );
  }
}
