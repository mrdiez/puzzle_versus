import 'dart:math';

import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:vector_math/vector_math_64.dart';

class ShakeOnTrigger extends StatefulWidget {
  const ShakeOnTrigger({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  ShakeOnTriggerState createState() => ShakeOnTriggerState();
}

class ShakeOnTriggerState extends State<ShakeOnTrigger> with SingleTickerProviderStateMixin {
  late final AnimationController animationController;
  late final Animation<double> animation;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
      vsync: this,
      duration: $Duration.veryLonger,
    )..addListener(() {
        setState(() {});
      });

    animation = Tween<double>(
      begin: 50.0,
      end: 80.0,
    ).animate(animationController);
  }

  Matrix4 _shake() {
    var progress = animationController.value;
    var offset = sin(progress * pi * 10.0);
    return Matrix4.translation(Vector3(offset * 4, 0.0, 0.0));
  }

  bool get isShaking => animationController.isAnimating;

  // Call this method to trigger animation
  void animate() {
    animationController.reset();
    animationController.forward();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Transform(
        transform: _shake(),
        child: widget.child,
      );
}
