import 'package:easter_egg_trigger/easter_egg_trigger.dart';
import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/assets.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:url_launcher/url_launcher.dart';

class Logo extends StatelessWidget {
  const Logo({Key? key, this.onTap, this.height, this.axis = Axis.horizontal}) : super(key: key);

  final void Function()? onTap;
  final double? height;
  final Axis axis;

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'LOGO',
      child: Image.asset(
        axis == Axis.vertical ? Asset.verticalLogo : Asset.horizontalLogo,
        height: height ?? $LogoHeight.m,
      ),
    );
    return SizedBox(
        height: height ?? $LogoHeight.m,
        child: onTap != null
            ? InkWell(onTap: onTap!, child: logo)
            : EasterEggTrigger(
                action: () async {
                  const ohYeah = 'https://www.youtube.com/watch?v=GMy6Is5JPQU';
                  if (await canLaunch(ohYeah)) {
                    await launch(ohYeah);
                  }
                },
                child: logo,
              ));
  }
}
