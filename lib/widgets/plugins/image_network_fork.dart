import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/foundation.dart' show kDebugMode, kIsWeb;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_network/image_network.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:webviewimage/webviewimage.dart';

export 'package:image_network/src/web/box_fit_web.dart';

///Image Network for Flutter app (Android - Ios - Web)
///Flutter plugin based on the [webview_flutter] plugin

/// [image] is the url of the image you want to display.
///
///
/// [imageCache] is ImageProvider and can be used with CachedNetworkImageProvider.(Android && Ios)
///
///
/// [fitAndroidIos] How to inscribe the image into the space allocated during layout.(Android && Ios).
///
///
/// [fitWeb] How to inscribe the image into the space allocated during layout.(Web).
///
///
/// [height] is the height the image will occupy on the page.
///
///
/// [width] is the width the image will occupy on the page.
///
///
/// [duration] is the duration [milliseconds] of the animation.
///
///
/// [curve] is the animation curve.
///
///
/// [onPointer] true or false to display mouse focus.
///
///
/// [fullScreen] You can choose the option (true or false) to display the image
///              at 100% regardless of how much height or width you are using
///              the image.
///
/// [debugPrint] true or false to display debug information.
///
///
/// [onTap] void function to click on the image.
///
///
/// [borderRadius] The border radius of the rounded corners. (Android - Ios - Web)
///
///
/// [onLoading] Widget for custom loading. (Android - Ios - Web)
///
///
/// [onError] Widget for custom error. (Android - Ios - Web)
///
///
class ImageNetworkFork extends StatefulWidget {
  final String image;
  final ImageProvider? imageCache;
  final BoxFit fitAndroidIos;
  final BoxFitWeb fitWeb;
  final double height;
  final double width;
  final int duration;
  final Curve curve;
  final bool onPointer;
  final bool fullScreen;
  final bool debugPrint;
  final Function? onTap;
  final Future<void> Function(Uint8List?) onImageNetworkLoaded;
  final BorderRadius borderRadius;
  final Widget onLoading;
  final Widget onError;

  ///constructor
  ///
  ///
  const ImageNetworkFork({
    Key? key,
    required this.image,
    required this.height,
    required this.width,
    this.duration = 1200,
    this.curve = Curves.easeIn,
    this.onPointer = false,
    this.fitAndroidIos = BoxFit.cover,
    this.fitWeb = BoxFitWeb.cover,
    this.borderRadius = BorderRadius.zero,
    this.onLoading = const CircularProgressIndicator(),
    this.onError = const Icon(Icons.error),
    this.fullScreen = false,
    this.debugPrint = false,
    this.onTap,
    required this.onImageNetworkLoaded,
    this.imageCache,
  }) : super(key: key);

  @override
  State<ImageNetworkFork> createState() => ImageNetworkForkState();
}

class ImageNetworkForkState extends State<ImageNetworkFork> with TickerProviderStateMixin {
  late AnimationController _controller;
  late WebViewXController webviewController;
  late Animation<double> _animation;

  /// bool variable used to validate (overlay with loading widget)
  /// while loading the image
  bool loading = true;

  /// bool variable used to validate (overlay with error widget)
  /// if an error occurs when loading the image
  bool error = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this, duration: Duration(milliseconds: widget.duration));
    _animation = Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(parent: _controller, curve: widget.curve));
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Completer? screenShotCompleter;

  Future<Uint8List?> screenShot() async {
    try {
      screenShotCompleter = Completer<String>();
      webviewController.callJsMethod('takeScreenShot', []);
      final base64Image = await screenShotCompleter!.future;
      return base64.decode(base64Image);
    } catch (e) {
      if (kDebugMode) print(e);
    }
    return null;
  }

  static const String _resizeCanvasJS = '''
    var canvas = document.createElement('canvas'),
         ctx = canvas.getContext("2d"),
         oc = document.createElement('canvas'),
         octx = oc.getContext('2d'),
         landscape = img.width >= img.height,
         maxSize = ${$GameImageSize.max};
  
     canvas.width = landscape ? maxSize : maxSize * img.width / img.height;
     canvas.height = landscape ? maxSize * img.height / img.width : maxSize;
  
     var cur = {
       width: Math.floor(img.width * 0.5),
       height: Math.floor(img.height * 0.5)
     }
  
     oc.width = cur.width;
     oc.height = cur.height;
  
     octx.drawImage(img, 0, 0, cur.width, cur.height);
  
     while ((landscape && cur.width * 0.5 > maxSize) || (!landscape && cur.height * 0.5 > maxSize)) {
       cur = {
         width: Math.floor(cur.width * 0.5),
         height: Math.floor(cur.height * 0.5)
       };
       octx.drawImage(oc, 0, 0, cur.width * 2, cur.height * 2, 0, 0, cur.width, cur.height);
     }
  
     ctx.drawImage(oc, 0, 0, cur.width, cur.height, 0, 0, canvas.width, canvas.height);
  ''';

  static const String _screenShotJS = '''
      var screenShot = () => new Promise(function(resolve, reject) {
        try {
          var allImages = document.getElementsByTagName('img');
          if (!allImages.length) reject();
          var originalImg = document.getElementsByTagName('img')[0];
          var img = new Image();
          var canvas = document.createElement("canvas");
          var ctx = canvas.getContext("2d");
          img.crossOrigin = "anonymous";
          img.src = originalImg.src;
          img.onload = function() {
              $_resizeCanvasJS
              var base64ScreenShot = canvas.toDataURL();
              resolve(base64ScreenShot.split(',')[1]);
          }
          setTimeout(resolve, 5000);
        }
        catch(e) {
          reject();
        }
      });
  ''';

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
        opacity: _animation,
        child: kIsWeb == false
            ? AppImageFork(
                image: widget.image,
                height: widget.height,
                width: widget.width,
                fit: widget.fitAndroidIos,
                onTap: widget.onTap,
                borderRadius: widget.borderRadius,
                onLoading: widget.onLoading,
                onError: widget.onError,
                imageProvider: widget.imageCache,
                onImageNetworkLoaded: widget.onImageNetworkLoaded,
              )
            : ClipRRect(
                borderRadius: widget.borderRadius,
                child: Stack(
                  children: [
                    Align(
                      alignment: Alignment.center,
                      child: WebViewX(
                        key: const ValueKey('gabriel_patrick_souza'),
                        initialContent: _imagePage(
                          image: widget.image,
                          pointer: widget.onPointer,
                          fitWeb: widget.fitWeb,
                          fullScreen: widget.fullScreen,
                          height: widget.height,
                          width: widget.width,
                        ),
                        initialSourceType: SourceType.html,
                        height: widget.height,
                        width: widget.width,
                        javascriptMode: JavascriptMode.unrestricted,
                        onWebViewCreated: (controller) => webviewController = controller,
                        onPageFinished: (src) {
                          if (widget.debugPrint) {
                            debugPrint('✓ The page has finished loading!\n');
                          }
                        },
                        jsContent: const {
                          EmbeddedJsContent(
                            webJs: 'function onClick() { callback() }',
                            mobileJs: 'function onClick() { callback.postMessage() }',
                          ),
                          EmbeddedJsContent(
                            webJs: 'function onLoad(msg) { callbackLoad(msg) }',
                            mobileJs: 'function onLoad(msg) { callbackLoad.postMessage(msg) }',
                          ),
                          EmbeddedJsContent(
                            webJs: 'function onError(msg) { callbackError(msg) }',
                            mobileJs: 'function onError(msg) { callbackError.postMessage(msg) }',
                          ),
                          EmbeddedJsContent(
                            webJs: '''
                            function takeScreenShot() {
                              $_screenShotJS
                              screenShot().then(function(data) {
                                ScreenShotCallback(data);
                              })
                              .catch (function (err) {
                                ScreenShotCallback('');
                              });
                            }
                            ''',
                            mobileJs: '''
                            function takeScreenShot() { 
                              $_screenShotJS
                              screenShot().then(function(data) {
                                ScreenShotCallback.postMessage(data);
                              })
                              .catch (function (err) {
                                ScreenShotCallback.postMessage('');
                              });
                            ''',
                          ),
                        },
                        dartCallBacks: {
                          DartCallback(
                            name: 'callback',
                            callBack: (msg) {
                              if (widget.onTap != null) {
                                widget.onTap!();
                              }
                            },
                          ),
                          DartCallback(
                            name: 'callbackLoad',
                            callBack: (msg) async {
                              if (msg) {
                                final imageBytes = await screenShot();
                                await widget.onImageNetworkLoaded(imageBytes);
                                if (imageBytes?.isEmpty ?? true) {
                                  error = true;
                                }
                                setState(() {
                                  loading = false;
                                });
                              }
                            },
                          ),
                          DartCallback(
                            name: 'callbackError',
                            callBack: (msg) async {
                              if (msg) {
                                await widget.onImageNetworkLoaded(null);
                                setState(() {
                                  loading = false;
                                  error = true;
                                });
                              }
                            },
                          ),
                          DartCallback(
                            name: 'ScreenShotCallback',
                            callBack: (msg) {
                              screenShotCompleter?.complete(msg);
                            },
                          ),
                        },
                        webSpecificParams: const WebSpecificParams(),
                        mobileSpecificParams: const MobileSpecificParams(
                          androidEnableHybridComposition: true,
                        ),
                      ),
                    ),
                    Align(alignment: Alignment.center, child: loading ? widget.onLoading : Container()),
                    Align(alignment: Alignment.center, child: error ? widget.onError : Container()),
                  ],
                ),
              ));
  }

  ///web page containing image only
  ///
  String _imagePage(
      {required String image,
      required bool pointer,
      required bool fullScreen,
      required double height,
      required double width,
      required BoxFitWeb fitWeb}) {
    return """<!DOCTYPE html>
            <html>
              <head>
                <style  type="text/css" rel="stylesheet">
                  body {
                    margin: 0px;
                    height: 100%;
                    width: 100%;
	                  overflow: hidden;
                   }
                    #myImg {
                      cursor: ${pointer ? "pointer" : ""};
                      transition: 0.3s;
                      width: ${fullScreen ? "100%" : "$width" "px"};
                      height: ${fullScreen ? "100%" : "$height" "px"};
                      object-fit: ${fitWeb.name(fitWeb as Fit)};
                    }
                    #myImg:hover {opacity: ${pointer ? "0.7" : ""}};}
                </style>
                <meta charset="utf-8"
                <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
                <meta http-equiv="Content-Security-Policy" 
                content="default-src * gap:; script-src * 'unsafe-inline' 'unsafe-eval'; connect-src *; 
                img-src * data: blob: android-webview-video-poster:; style-src * 'unsafe-inline';">
             </head>
             <body>
                <img id="myImg" src="$image" frameborder="0" allow="fullscreen"  allowfullscreen onclick= onClick() onerror= onError(this)>
                <script>
                  window.onload = function onLoad(){ callbackLoad(true);}
                </script>
             </body> 
            <script>
                function onClick() { callback() }
                function onError(source) { 
                  source.src = "https://scaffoldtecnologia.com.br/wp-content/uploads/2021/12/transparente.png";
                  source.onerror = ""; 
                  callbackError(true);
                  return true; 
                 }
            </script>
        </html>
    """;
  }
}

class AppImageFork extends StatefulWidget {
  final String image;
  final BoxFit fit;
  final double height;
  final double width;
  final Function? onTap;
  final Future<void> Function(Uint8List?) onImageNetworkLoaded;
  final BorderRadius borderRadius;
  final Widget onLoading;
  final Widget onError;
  final ImageProvider? imageProvider;

  const AppImageFork({
    Key? key,
    required this.image,
    required this.height,
    required this.width,
    required this.fit,
    required this.onTap,
    required this.onImageNetworkLoaded,
    required this.borderRadius,
    required this.onLoading,
    required this.onError,
    this.imageProvider,
  }) : super(key: key);

  @override
  State<AppImageFork> createState() => _AppImageForkState();
}

class _AppImageForkState extends State<AppImageFork> {
  /// bool variable used to validate (overlay with error widget)
  /// if an error occurs when loading the image
  bool imageError = false;

  @override
  void initState() {
    checkImageProvider();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (widget.onTap != null) {
          widget.onTap!();
        }
      },
      child: ClipRRect(
        borderRadius: widget.borderRadius,
        child: SizedBox(
            height: widget.height,
            width: widget.width,
            child: widget.imageProvider != null
                ? imageError
                    ? widget.onError
                    : Image(
                        image: widget.imageProvider!,
                        height: widget.height,
                        width: widget.width,
                        fit: widget.fit,
                      )
                : FutureBuilder(
                    future: getUrlResponse(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      switch (snapshot.connectionState) {
                        case ConnectionState.none:
                        case ConnectionState.waiting:
                          return Center(child: widget.onLoading);
                        case ConnectionState.active:
                        case ConnectionState.done:
                          if (snapshot.hasError) return widget.onError;
                          if (!snapshot.hasData) return widget.onError;
                          return Image.memory(
                            snapshot.data!,
                            height: widget.height,
                            width: widget.width,
                            fit: widget.fit,
                          );
                      }
                    },
                  )),
      ),
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
    );
  }

  Future getUrlResponse() async {
    final response = await http.get(Uri.parse(widget.image));
    if (response.statusCode == 200) {
      await widget.onImageNetworkLoaded(response.bodyBytes);
      return response.bodyBytes;
    } else {
      debugPrint('Http request error! [${response.statusCode}]');
    }
  }

  Future checkImageProvider() async {
    final response = await http.get(Uri.parse(widget.image));
    if (response.statusCode != 200) {
      debugPrint('Http request error! [${response.statusCode}]');
      setState(() => imageError = true);
    }
  }
}
