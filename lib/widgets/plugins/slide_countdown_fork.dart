import 'dart:async';

import 'package:flutter/material.dart';
import 'package:slide_countdown/slide_countdown.dart';
import 'package:stream_duration/stream_duration.dart';

class SlideCountdownFork extends StatefulWidget {
  const SlideCountdownFork({
    Key? key,
    required this.initialDuration,
    required this.duration,
    this.fixed = false,
    this.textStyle = const TextStyle(color: Color(0xFFFFFFFF), fontWeight: FontWeight.bold),
    this.icon,
    this.sufixIcon,
    this.separator,
    this.onDone,
    this.durationTitle,
    this.separatorType = SeparatorType.symbol,
    this.slideDirection = SlideDirection.down,
    this.padding = const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
    this.separatorPadding = const EdgeInsets.symmetric(horizontal: 3),
    this.showZeroValue = false,
    this.fade = false,
    this.decoration = const BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(20)),
      color: Color(0xFFF23333),
    ),
    this.curve = Curves.easeOut,
    this.countUp = false,
    this.infinityCountUp = false,
    this.slideAnimationDuration = const Duration(milliseconds: 300),
    this.onDurationChanged,
  }) : super(key: key);

  final Duration initialDuration;
  final bool fixed;

  /// [Duration] is the duration of the countdown slide,
  /// if the duration has finished it will call [onDone]
  final Duration duration;

  /// [TextStyle] is a parameter for all existing text,
  /// if this is null [SlideCountdown] has a default
  /// text style which will be of all text
  final TextStyle textStyle;

  /// [icon] is a parameter that can be initialized by any widget e.g [Icon],
  /// this will be in the first order, default empty widget
  final Widget? icon;

  /// [icon] is a parameter that can be initialized by any widget e.g [Icon],
  /// this will be in the end order, default empty widget
  final Widget? sufixIcon;

  /// Separator is a parameter that will separate each [duration],
  /// e.g hours by minutes, and you can change the [SeparatorType] of the symbol or title
  final String? separator;

  /// function [onDone] will be called when countdown is complete
  final VoidCallback? onDone;

  /// if you want to change the separator type, change this value to
  /// [SeparatorType.title] or [SeparatorType.symbol].
  /// [SeparatorType.title] will display title between duration,
  /// e.g minutes or you can change to another language, by changing the value in [DurationTitle]
  final SeparatorType separatorType;

  /// change [Duration Title] if you want to change the default language,
  /// which is English, to another language, for example, into Indonesian
  /// pro tips: if you change to Indonesian, we have default values [DurationTitle.id()]
  final DurationTitle? durationTitle;

  /// The decoration to paint in front of the [child].
  final Decoration decoration;

  /// The amount of space by which to inset the child.
  final EdgeInsets padding;

  /// The amount of space by which to inset the [separator].
  final EdgeInsets separatorPadding;

  /// if you initialize it with false, the duration which is empty will not be displayed
  final bool showZeroValue;

  /// if you want [slideDirection] animation that is not rough set this value to true
  final bool fade;

  /// you can change the slide animation up or down by changing the enum value in this property
  final SlideDirection slideDirection;

  /// to customize curve in [TextAnimation] you can change the default value
  /// default [Curves.easeOut]
  final Curve curve;

  ///this property allows you to do a count up, give it a value of true to do it
  final bool countUp;

  /// if you set this property value to true, it will do the count up continuously or infinity
  /// and the [onDone] property will never be executed,
  /// before doing that you need to set true to the [countUp] property,
  final bool infinityCountUp;

  /// SlideAnimationDuration which will be the duration of the slide animation from above or below
  final Duration slideAnimationDuration;

  /// this method allows you to stream from remaining [Duration]  or current [Duration]
  final Function(Duration)? onDurationChanged;
  @override
  _SlideCountdownForkState createState() => _SlideCountdownForkState();
}

class _SlideCountdownForkState extends State<SlideCountdownFork> {
  final ValueNotifier<int> _daysFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _daysSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _hoursFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _hoursSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _minutesFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _minutesSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _secondsFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _secondsSecondDigitNotifier = ValueNotifier<int>(0);

  late DurationTitle _durationTitle;
  late StreamDurationFork _streamDuration;
  late NotifiyDuration _notifiyDuration;
  late Color _textColor;
  late Color _fadeColor;
  bool disposed = false;

  @override
  void initState() {
    super.initState();
    _notifiyDuration = NotifiyDuration(widget.duration);
    disposed = false;
    _streamDurationListener();
    _durationTitle = widget.durationTitle ?? DurationTitle.en();
    _textColor = widget.textStyle.color ?? Colors.white;
    _fadeColor = (widget.textStyle.color ?? Colors.white).withOpacity(widget.fade ? 0 : 1);
  }

  @override
  void didUpdateWidget(covariant SlideCountdownFork oldWidget) {
    if (widget.durationTitle != null) {
      _durationTitle = widget.durationTitle ?? DurationTitle.en();
    }
    if (widget.textStyle != oldWidget.textStyle || widget.fade != oldWidget.fade) {
      _textColor = widget.textStyle.color ?? Colors.white;
      _fadeColor = (widget.textStyle.color ?? Colors.white).withOpacity(widget.fade ? 0 : 1);
    }
    if (widget.countUp != oldWidget.countUp ||
        widget.infinityCountUp != oldWidget.infinityCountUp ||
        widget.onDurationChanged != oldWidget.onDurationChanged) {
      _streamDuration.dispose();
      _streamDurationListener();
    }
    super.didUpdateWidget(oldWidget);
  }

  void _streamDurationListener() {
    if (!widget.countUp) {
      _updateValue(widget.duration);
    }
    _updateValue(widget.duration);
    if (!widget.fixed) {
      _streamDuration = StreamDurationFork(
        widget.initialDuration,
        onDone: () {
          if (widget.onDone != null) {
            widget.onDone!();
          }
        },
        countUp: widget.countUp,
        infinity: widget.infinityCountUp,
      );
    } else {
      _streamDuration = StreamDurationFork(Duration.zero);
    }

    if (!disposed && !widget.fixed) {
      try {
        _streamDuration.durationLeft.listen((event) {
          if (widget.onDurationChanged != null) {
            widget.onDurationChanged!(event);
          }
          _notifiyDuration.streamDuration(event);
          _updateValue(event);
        });
      } catch (ex) {
        debugPrint(ex.toString());
      }
    }
  }

  static int daysFirstDigit(Duration duration) {
    if (duration.inDays == 0) {
      return 0;
    } else {
      return (duration.inDays) ~/ 10;
    }
  }

  void _daysFirstDigit(Duration duration) {
    try {
      int calculate = daysFirstDigit(duration);
      if (calculate != _daysFirstDigitNotifier.value) {
        _daysFirstDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int daysSecondDigit(Duration duration) {
    if (duration.inDays == 0) {
      return 0;
    } else {
      return (duration.inDays) % 10;
    }
  }

  void _daysSecondDigit(Duration duration) {
    try {
      int calculate = daysSecondDigit(duration);
      if (calculate != _daysSecondDigitNotifier.value) {
        _daysSecondDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int hoursFirstDigit(Duration duration) {
    if (duration.inHours == 0) {
      return 0;
    } else {
      return (duration.inHours % 24) ~/ 10;
    }
  }

  void _hoursFirstDigit(Duration duration) {
    try {
      int calculate = hoursFirstDigit(duration);
      if (calculate != _hoursFirstDigitNotifier.value) {
        _hoursFirstDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int hoursSecondDigit(Duration duration) {
    if (duration.inHours == 0) {
      return 0;
    } else {
      return (duration.inHours % 24) % 10;
    }
  }

  void _hoursSecondDigit(Duration duration) {
    try {
      int calculate = hoursSecondDigit(duration);
      if (calculate != _hoursSecondDigitNotifier.value) {
        _hoursSecondDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int minutesFirstDigit(Duration duration) {
    if (duration.inMinutes == 0) {
      return 0;
    } else {
      return (duration.inMinutes % 60) ~/ 10;
    }
  }

  void _minutesFirstDigit(Duration duration) {
    try {
      int calculate = minutesFirstDigit(duration);
      if (calculate != _minutesFirstDigitNotifier.value) {
        _minutesFirstDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int minutesSecondDigit(Duration duration) {
    if (duration.inMinutes == 0) {
      return 0;
    } else {
      return (duration.inMinutes % 60) % 10;
    }
  }

  void _minutesSecondDigit(Duration duration) {
    try {
      int calculate = minutesSecondDigit(duration);
      if (calculate != _minutesSecondDigitNotifier.value) {
        _minutesSecondDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int secondsFirstDigit(Duration duration) {
    if (duration.inSeconds == 0) {
      return 0;
    } else {
      return (duration.inSeconds % 60) ~/ 10;
    }
  }

  void _secondsFirstDigit(Duration duration) {
    try {
      int calculate = secondsFirstDigit(duration);
      if (calculate != _secondsFirstDigitNotifier.value) {
        _secondsFirstDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  static int secondsSecondDigit(Duration duration) {
    if (duration.inSeconds == 0) {
      return 0;
    } else {
      return (duration.inSeconds % 60) % 10;
    }
  }

  void _secondsSecondDigit(Duration duration) {
    try {
      int calculate = secondsSecondDigit(duration);
      if (calculate != _secondsSecondDigitNotifier.value) {
        _secondsSecondDigitNotifier.value = calculate;
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _disposeDaysNotifier() {
    _daysFirstDigitNotifier.dispose();
    _daysSecondDigitNotifier.dispose();
  }

  void _disposeHoursNotifier() {
    _hoursFirstDigitNotifier.dispose();
    _hoursSecondDigitNotifier.dispose();
  }

  void _disposeMinutesNotifier() {
    _minutesFirstDigitNotifier.dispose();
    _minutesSecondDigitNotifier.dispose();
  }

  void _disposeSecondsNotifier() {
    _secondsFirstDigitNotifier.dispose();
    _secondsSecondDigitNotifier.dispose();
  }

  void _updateValue(Duration duration) {
    _daysFirstDigit(duration);
    _daysSecondDigit(duration);

    _hoursFirstDigit(duration);
    _hoursSecondDigit(duration);

    _minutesFirstDigit(duration);
    _minutesSecondDigit(duration);

    _secondsFirstDigit(duration);
    _secondsSecondDigit(duration);
  }

  @override
  void dispose() {
    disposed = true;
    _disposeHoursNotifier();
    _disposeMinutesNotifier();
    _disposeSecondsNotifier();
    _disposeDaysNotifier();
    _streamDuration.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _notifiyDuration,
      builder: (BuildContext context, Duration duration, Widget? child) {
        return DecoratedBox(
          decoration: widget.decoration,
          child: ClipRect(
            child: ShaderMask(
              shaderCallback: (Rect rect) {
                return LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    _fadeColor,
                    _textColor,
                    _textColor,
                    _fadeColor,
                  ],
                  stops: const [0.05, 0.3, 0.7, 0.95],
                ).createShader(rect);
              },
              child: Padding(
                padding: widget.padding,
                child: Visibility(
                  visible: widget.fade,
                  child: countdown(duration),
                  replacement: ClipRect(
                    child: countdown(duration),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget countdown(Duration duration) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Visibility(
          visible: widget.icon != null,
          child: widget.icon ?? const SizedBox.shrink(),
        ),
        Builder(builder: (context) {
          if (duration.inDays < 1 && !widget.showZeroValue) {
            return const SizedBox.shrink();
          } else {
            return Row(
              children: [
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _daysFirstDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                ),
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _daysSecondDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                  showZeroValue: !(duration.inHours < 1 && widget.separatorType == SeparatorType.title),
                ),
                Padding(
                  padding: widget.separatorPadding,
                  child: Visibility(
                    visible: widget.separatorType == SeparatorType.symbol,
                    child: Text(widget.separator ?? ':', style: widget.textStyle),
                    replacement: Text(_durationTitle.days, style: widget.textStyle),
                  ),
                ),
              ],
            );
          }
        }),
        Builder(builder: (context) {
          if (duration.inHours < 1 && !widget.showZeroValue) {
            return const SizedBox.shrink();
          } else {
            return Row(
              children: [
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _hoursFirstDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                ),
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _hoursSecondDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                  showZeroValue: !(duration.inHours < 1 && widget.separatorType == SeparatorType.title),
                ),
                Padding(
                  padding: widget.separatorPadding,
                  child: Visibility(
                    visible: widget.separatorType == SeparatorType.symbol,
                    child: Text(widget.separator ?? ':', style: widget.textStyle),
                    replacement: Text(_durationTitle.hours, style: widget.textStyle),
                  ),
                ),
              ],
            );
          }
        }),
        Builder(builder: (context) {
          if (duration.inMinutes < 1 && !widget.showZeroValue) {
            return const SizedBox.shrink();
          } else {
            return Row(
              children: [
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _minutesFirstDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                  showZeroValue: !(duration.inMinutes < 1 && widget.separatorType == SeparatorType.title),
                ),
                TextAnimation(
                  slideAnimationDuration: widget.slideAnimationDuration,
                  value: _minutesSecondDigitNotifier,
                  textStyle: widget.textStyle,
                  slideDirection: widget.slideDirection,
                  curve: widget.curve,
                  countUp: widget.countUp,
                  showZeroValue: !(duration.inMinutes < 1 && widget.separatorType == SeparatorType.title),
                ),
                Padding(
                  padding: widget.separatorPadding,
                  child: Visibility(
                    visible: widget.separatorType == SeparatorType.symbol,
                    child: Text(widget.separator ?? ':', style: widget.textStyle),
                    replacement: Text(_durationTitle.minutes, style: widget.textStyle),
                  ),
                ),
              ],
            );
          }
        }),
        TextAnimation(
          slideAnimationDuration: widget.slideAnimationDuration,
          value: _secondsFirstDigitNotifier,
          textStyle: widget.textStyle,
          slideDirection: widget.slideDirection,
          curve: widget.curve,
          countUp: widget.countUp,
        ),
        TextAnimation(
          slideAnimationDuration: widget.slideAnimationDuration,
          value: _secondsSecondDigitNotifier,
          textStyle: widget.textStyle,
          slideDirection: widget.slideDirection,
          curve: widget.curve,
          countUp: widget.countUp,
        ),
        Visibility(
          visible: widget.separatorType == SeparatorType.title,
          child: Padding(
            padding: widget.separatorPadding,
            child: Text(_durationTitle.seconds, style: widget.textStyle),
          ),
        ),
        Visibility(
          visible: widget.sufixIcon != null,
          child: widget.sufixIcon ?? const SizedBox.shrink(),
        ),
      ],
    );
  }
}

///Duration Title is a class that contains day, hour, minute, second properties.
///which will later show in the view if you assign a value of [SeparatorType.title] to the [SeparatorType] enum
///and you can change the title of the day, hour, minute, and second. For example, you change to Indonesian.
///By default this title will display in English [DurationTitle.en()]
class DurationTitle {
  final String days;
  final String hours;
  final String minutes;
  final String seconds;

  const DurationTitle({
    required this.days,
    required this.hours,
    required this.minutes,
    required this.seconds,
  });

  DurationTitle copyWith({
    String? days,
    String? hours,
    String? minutes,
    String? seconds,
  }) =>
      DurationTitle(
        days: days ?? this.days,
        hours: hours ?? this.hours,
        minutes: minutes ?? this.minutes,
        seconds: seconds ?? this.seconds,
      );

  factory DurationTitle.id() => const DurationTitle(days: 'hari', hours: 'jam', minutes: 'menit', seconds: 'detik');
  factory DurationTitle.en() =>
      const DurationTitle(days: 'days', hours: 'hours', minutes: 'minutes', seconds: 'seconds');
}

enum SlideDirection { up, down }
enum SeparatorType { symbol, title }

class NotifiyDuration extends ValueNotifier<Duration> {
  NotifiyDuration(Duration value) : super(value);

  streamDuration(Duration duration) {
    value = duration;
    notifyListeners();
  }
}

class TextAnimation extends StatefulWidget {
  const TextAnimation({
    Key? key,
    required this.value,
    required this.textStyle,
    required this.slideDirection,
    required this.slideAnimationDuration,
    this.initValue = 0,
    this.showZeroValue = true,
    this.curve = Curves.easeOut,
    this.countUp = true,
  }) : super(key: key);

  final ValueNotifier<int> value;
  final TextStyle textStyle;
  final int initValue;
  final SlideDirection slideDirection;
  final bool showZeroValue;
  final Curve curve;
  final bool countUp;
  final Duration slideAnimationDuration;

  @override
  _TextAnimationState createState() => _TextAnimationState();
}

class _TextAnimationState extends State<TextAnimation> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Offset> _offsetAnimationOne;
  late Animation<Offset> _offsetAnimationTwo;
  int currentValue = 0;
  int nextValue = 0;
  bool disposed = false;

  @override
  void initState() {
    super.initState();
    disposed = false;
    _animationController = AnimationController(vsync: this, duration: widget.slideAnimationDuration);
    _offsetAnimationOne = Tween<Offset>(
      begin: const Offset(0.0, -1.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(parent: _animationController, curve: widget.curve));

    _offsetAnimationTwo = Tween<Offset>(
      begin: const Offset(0.0, 0.0),
      end: const Offset(0.0, 1.0),
    ).animate(CurvedAnimation(parent: _animationController, curve: widget.curve)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _animationController.reset();
        }
      }));

    if (!disposed) {
      widget.value.addListener(() {
        if (!_animationController.isCompleted) {
          if (mounted) {
            _animationController.forward();
          }
        }
      });
    }
  }

  void _digit(int value) {
    if (currentValue != value) {
      nextValue = value;
      if (value < 9) {
        currentValue = widget.countUp
            ? value < 1
                ? value
                : value - 1
            : value + 1;
      } else {
        currentValue = 0;
      }
    } else {
      currentValue = value;
      if (nextValue == 0) {
        currentValue = 1;
      }
    }

    if (_animationController.isDismissed) {
      currentValue = nextValue;
    }
  }

  @override
  void dispose() {
    disposed = true;
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.value,
      builder: (BuildContext context, int value, Widget? child) {
        return AnimatedBuilder(
          animation: _animationController,
          builder: (context, textWidget) {
            _digit(value);
            return Stack(
              alignment: Alignment.center,
              children: [
                FractionalTranslation(
                  translation: widget.slideDirection == SlideDirection.down
                      ? _offsetAnimationOne.value
                      : -_offsetAnimationOne.value,
                  child: Text(
                    '$nextValue',
                    style: widget.textStyle,
                    textScaleFactor: 1.0,
                  ),
                ),
                FractionalTranslation(
                  translation: widget.slideDirection == SlideDirection.down
                      ? _offsetAnimationTwo.value
                      : -_offsetAnimationTwo.value,
                  child: Text(
                    '$currentValue',
                    style: widget.textStyle,
                    textScaleFactor: 1.0,
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}

class SlideCountdownSeparated extends StatefulWidget {
  const SlideCountdownSeparated({
    Key? key,
    required this.duration,
    this.height = 30,
    this.width = 30,
    this.textStyle = const TextStyle(color: Color(0xFFFFFFFF), fontWeight: FontWeight.bold),
    this.separatorStyle = const TextStyle(color: Color(0xFF000000), fontWeight: FontWeight.bold),
    this.icon,
    this.sufixIcon,
    this.separator,
    this.onDone,
    this.durationTitle,
    this.separatorType = SeparatorType.symbol,
    this.slideDirection = SlideDirection.down,
    this.padding = const EdgeInsets.all(5),
    this.separatorPadding = const EdgeInsets.symmetric(horizontal: 3),
    this.withDays = false,
    this.showZeroValue = false,
    this.fade = false,
    this.decoration = const BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(4)),
      color: Color(0xFFF23333),
    ),
    this.curve = Curves.easeOut,
    this.countUp = false,
    this.infinityCountUp = false,
    this.slideAnimationDuration = const Duration(milliseconds: 300),
    this.onDurationChanged,
  }) : super(key: key);

  /// [Duration] is the duration of the countdown slide,
  /// if the duration has finished it will call [onDone]
  final Duration duration;

  /// height to set the size of height each [Container]
  /// [Container] will be the background of each a duration
  /// to decorate the [Container] on the [decoration] property
  final double height;

  /// width to set the size of width each [Container]
  /// [Container] will be the background of each a duration
  /// to decorate the [Container] on the [decoration] property
  final double width;

  /// [TextStyle] is a parameter for all existing text,
  /// if this is null [SlideCountdownSeparated] has a default
  /// text style which will be of all text
  final TextStyle textStyle;

  /// [TextStyle] is a parameter for all existing text,
  /// if this is null [SlideCountdownSeparated] has a default
  /// text style which will be of all text
  final TextStyle separatorStyle;

  /// [icon] is a parameter that can be initialized by any widget e.g [Icon],
  /// this will be in the first order, default empty widget
  final Widget? icon;

  /// [icon] is a parameter that can be initialized by any widget e.g [Icon],
  /// this will be in the end order, default empty widget
  final Widget? sufixIcon;

  /// Separator is a parameter that will separate each [duration],
  /// e.g hours by minutes, and you can change the [SeparatorType] of the symbol or title
  final String? separator;

  /// function [onDone] will be called when countdown is complete
  final VoidCallback? onDone;

  /// if you want to change the separator type, change this value to
  /// [SeparatorType.title] or [SeparatorType.symbol].
  /// [SeparatorType.title] will display title between duration,
  /// e.g minutes or you can change to another language, by changing the value in [DurationTitle]
  final SeparatorType separatorType;

  /// change [Duration Title] if you want to change the default language,
  /// which is English, to another language, for example, into Indonesian
  /// pro tips: if you change to Indonesian, we have default values [DurationTitle.id()]
  final DurationTitle? durationTitle;

  /// The decoration to paint in front of the [child].
  final Decoration decoration;

  /// The amount of space by which to inset the child.
  final EdgeInsets padding;

  /// The amount of space by which to inset the [separator].
  final EdgeInsets separatorPadding;

  /// if you give or the remaining [duration] is less than one hour
  /// and the property is set to true, the countdown hours will not show
  final bool withDays;

  /// if you initialize it with false, the duration which is empty will not be displayed
  final bool showZeroValue;

  /// if you want [slideDirection] animation that is not rough set this value to true
  final bool fade;

  /// you can change the slide animation up or down by changing the enum value in this property
  final SlideDirection slideDirection;

  /// to customize curve in [TextAnimation] you can change the default value
  /// default [Curves.easeOut]
  final Curve curve;

  ///this property allows you to do a count up, give it a value of true to do it
  final bool countUp;

  /// if you set this property value to true, it will do the count up continuously or infinity
  /// and the [onDone] property will never be executed,
  /// before doing that you need to set true to the [countUp] property,
  final bool infinityCountUp;

  /// SlideAnimationDuration which will be the duration of the slide animation from above or below
  final Duration slideAnimationDuration;

  /// this method allows you to stream from remaining [Duration]  or current [Duration]
  final Function(Duration)? onDurationChanged;

  @override
  _SlideCountdownSeparatedState createState() => _SlideCountdownSeparatedState();
}

class _SlideCountdownSeparatedState extends State<SlideCountdownSeparated> {
  final ValueNotifier<int> _daysFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _daysSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _hoursFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _hoursSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _minutesFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _minutesSecondDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _secondsFirstDigitNotifier = ValueNotifier<int>(0);
  final ValueNotifier<int> _secondsSecondDigitNotifier = ValueNotifier<int>(0);

  late DurationTitle _durationTitle;
  late StreamDuration _streamDuration;
  late NotifiyDuration _notifiyDuration;
  late Color _textColor;
  late Color _fadeColor;
  bool disposed = false;

  @override
  void initState() {
    super.initState();
    _notifiyDuration = NotifiyDuration(widget.duration);
    _streamDurationListener();
    _durationTitle = widget.durationTitle ?? DurationTitle.en();
    _textColor = widget.textStyle.color ?? Colors.white;
    _fadeColor = (widget.textStyle.color ?? Colors.white).withOpacity(widget.fade ? 0 : 1);
  }

  @override
  void didUpdateWidget(covariant SlideCountdownSeparated oldWidget) {
    if (widget.durationTitle != null) {
      _durationTitle = widget.durationTitle ?? DurationTitle.en();
    }
    if (widget.textStyle != oldWidget.textStyle || widget.fade != oldWidget.fade) {
      _textColor = widget.textStyle.color ?? Colors.white;
      _fadeColor = (widget.textStyle.color ?? Colors.white).withOpacity(widget.fade ? 0 : 1);
    }
    if (widget.countUp != oldWidget.countUp ||
        widget.infinityCountUp != oldWidget.infinityCountUp ||
        widget.onDurationChanged != oldWidget.onDurationChanged) {
      _streamDuration.dispose();
      _streamDurationListener();
    }
    super.didUpdateWidget(oldWidget);
  }

  void _streamDurationListener() {
    if (!widget.countUp) {
      _updateValue(widget.duration);
    }
    _streamDuration = StreamDuration(
      widget.duration,
      onDone: () {
        if (widget.onDone != null) {
          widget.onDone!();
        }
      },
      countUp: widget.countUp,
      infinity: widget.infinityCountUp,
    );

    if (!disposed) {
      try {
        _streamDuration.durationLeft.listen((event) {
          if (widget.onDurationChanged != null) {
            widget.onDurationChanged!(event);
          }

          _notifiyDuration.streamDuration(event);
          _updateValue(event);
        });
      } catch (ex) {
        debugPrint(ex.toString());
      }
    }
  }

  void _daysFirstDigit(Duration duration) {
    try {
      if (duration.inDays == 0) {
        _daysFirstDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inDays) ~/ 10;
        if (calculate != _daysFirstDigitNotifier.value) {
          _daysFirstDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _daysSecondDigit(Duration duration) {
    try {
      if (duration.inDays == 0) {
        _daysSecondDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inDays) % 10;
        if (calculate != _daysSecondDigitNotifier.value) {
          _daysSecondDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _hoursFirstDigit(Duration duration) {
    try {
      if (duration.inHours == 0) {
        _hoursFirstDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inHours % 24) ~/ 10;
        if (calculate != _hoursFirstDigitNotifier.value) {
          _hoursFirstDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _hoursSecondDigit(Duration duration) {
    try {
      if (duration.inHours == 0) {
        _hoursSecondDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inHours % 24) % 10;
        if (calculate != _hoursSecondDigitNotifier.value) {
          _hoursSecondDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _minutesFirstDigit(Duration duration) {
    try {
      if (duration.inMinutes == 0) {
        _minutesFirstDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inMinutes % 60) ~/ 10;
        if (calculate != _minutesFirstDigitNotifier.value) {
          _minutesFirstDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _minutesSecondDigit(Duration duration) {
    try {
      if (duration.inMinutes == 0) {
        _minutesSecondDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inMinutes % 60) % 10;
        if (calculate != _minutesSecondDigitNotifier.value) {
          _minutesSecondDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _secondsFirstDigit(Duration duration) {
    try {
      if (duration.inSeconds == 0) {
        _secondsFirstDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inSeconds % 60) ~/ 10;
        if (calculate != _secondsFirstDigitNotifier.value) {
          _secondsFirstDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _secondsSecondDigit(Duration duration) {
    try {
      if (duration.inSeconds == 0) {
        _secondsSecondDigitNotifier.value = 0;
        return;
      } else {
        int calculate = (duration.inSeconds % 60) % 10;
        if (calculate != _secondsSecondDigitNotifier.value) {
          _secondsSecondDigitNotifier.value = calculate;
        }
      }
    } catch (ex) {
      debugPrint(ex.toString());
    }
  }

  void _disposeDaysNotifier() {
    _daysFirstDigitNotifier.dispose();
    _daysSecondDigitNotifier.dispose();
  }

  void _disposeHoursNotifier() {
    _hoursFirstDigitNotifier.dispose();
    _hoursSecondDigitNotifier.dispose();
  }

  void _disposeMinutesNotifier() {
    _minutesFirstDigitNotifier.dispose();
    _minutesSecondDigitNotifier.dispose();
  }

  void _disposeSecondsNotifier() {
    _secondsFirstDigitNotifier.dispose();
    _secondsSecondDigitNotifier.dispose();
  }

  void _updateValue(Duration duration) {
    _daysFirstDigit(duration);
    _daysSecondDigit(duration);

    _hoursFirstDigit(duration);
    _hoursSecondDigit(duration);

    _minutesFirstDigit(duration);
    _minutesSecondDigit(duration);

    _secondsFirstDigit(duration);
    _secondsSecondDigit(duration);
  }

  @override
  void dispose() {
    disposed = true;
    _disposeHoursNotifier();
    _disposeMinutesNotifier();
    _disposeSecondsNotifier();
    _disposeDaysNotifier();
    _streamDuration.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: _notifiyDuration,
      builder: (BuildContext context, Duration duration, Widget? child) {
        return countdown(duration);
      },
    );
  }

  Widget countdown(Duration duration) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Visibility(
          visible: widget.icon != null,
          child: widget.icon ?? const SizedBox.shrink(),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            daysWidget(duration),
            separator(
              title: _durationTitle.days,
              visible: !(duration.inDays < 1 && !widget.showZeroValue && !widget.withDays),
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            hoursWidget(duration),
            separator(
              title: _durationTitle.hours,
              visible: !(duration.inHours < 1 && !widget.showZeroValue),
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            minutesWidget(duration),
            separator(
              title: _durationTitle.minutes,
              visible: !(duration.inMinutes < 1 && !widget.showZeroValue),
            ),
          ],
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            secondsWidget(duration),
            separatorSeconds(),
          ],
        ),
        Visibility(
          visible: widget.sufixIcon != null,
          child: widget.sufixIcon ?? const SizedBox.shrink(),
        ),
      ],
    );
  }

  Widget boxDecoration({required Widget child}) {
    return Container(
      height: widget.height,
      width: widget.width,
      decoration: widget.decoration,
      clipBehavior: Clip.hardEdge,
      alignment: Alignment.center,
      child: ShaderMask(
        shaderCallback: (Rect rect) {
          return LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              _fadeColor,
              _textColor,
              _textColor,
              _fadeColor,
            ],
            stops: const [0.05, 0.3, 0.7, 0.95],
          ).createShader(rect);
        },
        child: Visibility(
          visible: widget.fade,
          child: SizedBox.expand(child: child),
          replacement: ClipRect(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 2),
              child: child,
            ),
          ),
        ),
      ),
    );
  }

  Widget daysWidget(Duration duration) {
    return Builder(builder: (context) {
      if (duration.inDays < 1 && !widget.showZeroValue && !widget.withDays) {
        return const SizedBox.shrink();
      } else {
        return boxDecoration(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _daysFirstDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
              ),
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _daysSecondDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
                showZeroValue: !(duration.inHours < 1 && widget.separatorType == SeparatorType.title),
              ),
            ],
          ),
        );
      }
    });
  }

  Widget hoursWidget(Duration duration) {
    return Builder(builder: (context) {
      if (duration.inHours < 1 && !widget.showZeroValue) {
        return const SizedBox.shrink();
      } else {
        return boxDecoration(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _hoursFirstDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
              ),
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _hoursSecondDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
                showZeroValue: !(duration.inHours < 1 && widget.separatorType == SeparatorType.title),
              ),
            ],
          ),
        );
      }
    });
  }

  Widget minutesWidget(Duration duration) {
    return Builder(builder: (context) {
      if (duration.inMinutes < 1 && !widget.showZeroValue) {
        return const SizedBox.shrink();
      } else {
        return boxDecoration(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _minutesFirstDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
                showZeroValue: !(duration.inMinutes < 1 && widget.separatorType == SeparatorType.title),
              ),
              TextAnimation(
                slideAnimationDuration: widget.slideAnimationDuration,
                value: _minutesSecondDigitNotifier,
                textStyle: widget.textStyle,
                slideDirection: widget.slideDirection,
                curve: widget.curve,
                countUp: widget.countUp,
                showZeroValue: !(duration.inMinutes < 1 && widget.separatorType == SeparatorType.title),
              ),
            ],
          ),
        );
      }
    });
  }

  Widget secondsWidget(Duration duration) {
    return boxDecoration(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextAnimation(
            slideAnimationDuration: widget.slideAnimationDuration,
            value: _secondsFirstDigitNotifier,
            textStyle: widget.textStyle,
            slideDirection: widget.slideDirection,
            curve: widget.curve,
            countUp: widget.countUp,
          ),
          TextAnimation(
            slideAnimationDuration: widget.slideAnimationDuration,
            value: _secondsSecondDigitNotifier,
            textStyle: widget.textStyle,
            slideDirection: widget.slideDirection,
            curve: widget.curve,
            countUp: widget.countUp,
          ),
        ],
      ),
    );
  }

  Widget separator({required String title, bool visible = true}) {
    return Visibility(
      visible: visible,
      child: Padding(
        padding: widget.separatorPadding,
        child: Visibility(
          visible: widget.separatorType == SeparatorType.symbol,
          child: Text(widget.separator ?? ':', style: widget.separatorStyle),
          replacement: Text(title, style: widget.separatorStyle),
        ),
      ),
    );
  }

  Widget separatorSeconds() {
    return Visibility(
      visible: widget.separatorType == SeparatorType.title,
      child: Padding(
        padding: widget.separatorPadding,
        child: Text(_durationTitle.seconds, style: widget.separatorStyle),
      ),
    );
  }
}

class StreamDurationFork {
  late final StreamController<Duration> _streamController = StreamController<Duration>();
  Stream<Duration> get durationLeft => _streamController.stream;
  StreamSubscription<Duration>? _streamSubscription;

  StreamDurationFork(
    Duration duration, {
    Function? onDone,
    bool countUp = false,
    bool infinity = false,
  }) {
    try {
      var _durationLeft = duration;
      if (countUp) {
        _durationLeft += const Duration(seconds: 1);
      } else {
        _durationLeft -= const Duration(seconds: 1);
      }
      if (!_streamController.isClosed) {
        _streamController.add(_durationLeft);
      }
      _streamSubscription = Stream<Duration>.periodic(const Duration(seconds: 1), (_) {
        if (countUp) {
          return _durationLeft += const Duration(seconds: 1);
        } else {
          return _durationLeft -= const Duration(seconds: 1);
        }
      }).listen(
        (Duration event) {
          if (_streamController.isClosed) return;
          _streamController.add(event);

          if (countUp) {
            if (!infinity) {
              if (event.inSeconds == duration.inSeconds) {
                dispose();
                Future.delayed(const Duration(seconds: 1), () {
                  if (onDone != null) {
                    onDone();
                  }
                });
              }
            }
          } else {
            if (event.inSeconds == 0) {
              dispose();
              Future.delayed(const Duration(seconds: 1), () {
                if (onDone != null) {
                  onDone();
                }
              });
            }
          }
        },
      );
    } catch (e) {
      throw 'Stream Duration: error' + e.toString();
    }
  }

  void dispose() {
    _streamSubscription?.cancel();
    _streamController.close();
  }
}
