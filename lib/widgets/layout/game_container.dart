import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';

class GameContainer extends StatelessWidget {
  const GameContainer({Key? key, required this.gameMode, required this.child}) : super(key: key);

  final GameMode gameMode;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Hero(
          tag: gameMode.toString(),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.vertical(top: $Radius.xxl),
              gradient: LinearGradient(
                begin: $Alignment.topLeftGradient,
                end: $Alignment.bottomRightGradient,
                colors: [gameColors[gameMode]!, gameColors[gameMode]!.shade200],
              ),
            ),
          ),
        ),
        child,
      ],
    );
  }
}
