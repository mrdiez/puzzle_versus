import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';

class SimpleContainer extends StatelessWidget {
  const SimpleContainer({Key? key, required this.child, this.color, this.shadow = true, this.elevation = $ShadowSize.m})
      : super(key: key);

  final Color? color;
  final double elevation;
  final bool shadow;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all($Radius.m),
        color: color,
        boxShadow: [
          if (shadow)
            BoxShadow(
              color: darkShadowColor,
              spreadRadius: elevation,
              blurRadius: 0,
              offset: Offset(elevation, elevation), // changes position of shadow
            ),
        ],
      ),
      child: child,
    );
  }
}
