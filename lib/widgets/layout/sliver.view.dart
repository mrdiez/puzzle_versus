import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SliverView extends StatelessWidget {
  final Key? scrollViewKey;
  final ScrollController? controller;
  final Widget? child;
  final Widget? appBar;
  final bool fillRemaining;
  final bool overlap;
  final bool nested;
  final ScrollPhysics? physics;
  final List<Widget>? slivers;
  final Future<void> Function()? onRefresh;
  final GlobalKey<RefreshIndicatorState>? refreshKey;

  const SliverView(
      {Key? key,
      this.scrollViewKey,
      this.controller,
      this.child,
      this.appBar,
      this.fillRemaining = false,
      this.overlap = false,
      this.nested = false,
      this.physics,
      this.slivers,
      this.onRefresh,
      this.refreshKey})
      : super(key: key);

  Widget wrapWithRefresh(BuildContext context, {required Widget child}) => onRefresh != null
      ? RefreshIndicator(
          key: refreshKey, displacement: MediaQuery.of(context).size.width / 2, onRefresh: onRefresh!, child: child)
      : child;

  List<Widget> injectOverlap(BuildContext context) {
    var result = <Widget>[];
    try {
      result.add(SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)));
    } catch (e) {
      if (kDebugMode) {
        print("NestedScrollView's context not found\n\n$e");
      }
    }
    return result;
  }

  Widget getAbsorber(BuildContext context) =>
      SliverOverlapAbsorber(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context), sliver: appBar);

  List<Widget> getSlivers(BuildContext context, [bool? innerBoxIsScrolled]) => [
        if (overlap) ...injectOverlap(context),
        if (nested && appBar != null) getAbsorber(context),
        if (!nested && appBar != null) appBar!,
        if (slivers != null && slivers!.isNotEmpty) ...slivers!,
        if (!nested && child != null && !fillRemaining) SliverToBoxAdapter(child: child),
        if (!nested && child != null && fillRemaining) SliverFillRemaining(child: child),
      ];

  @override
  Widget build(BuildContext context) {
    return wrapWithRefresh(
      context,
      child: nested
          ? NestedScrollView(
              key: scrollViewKey,
              controller: controller,
              physics: physics,
              headerSliverBuilder: getSlivers,
              body: child!)
          : CustomScrollView(
              controller: controller,
              key: scrollViewKey,
              physics: physics,
              slivers: getSlivers(context),
            ),
    );
  }
}
