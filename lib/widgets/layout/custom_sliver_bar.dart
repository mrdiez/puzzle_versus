import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';

class CustomSliverBar extends StatelessWidget {
  const CustomSliverBar({
    Key? key,
    this.pinned = true,
    this.floating = true,
    this.snap = true,
    this.child,
    this.alignment,
    this.flexibleChild,
    this.flexibleAlignment,
    this.collapsedHeight,
    this.expandedHeight,
    this.color,
    this.borderRadius,
    this.boxShadow,
  }) : super(key: key);

  final bool pinned;
  final bool floating;
  final bool snap;
  final Widget? child;
  final Alignment? alignment;
  final Widget? flexibleChild;
  final Alignment? flexibleAlignment;
  final double? collapsedHeight;
  final double? expandedHeight;
  final Color? color;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: pinned,
      floating: floating,
      snap: floating && snap,
      elevation: 0,
      collapsedHeight: collapsedHeight ?? $AppBarHeight.l,
      expandedHeight: expandedHeight ?? $AppBarHeight.l,
      automaticallyImplyLeading: false,
      backgroundColor: transparentColor,
      flexibleSpace: Stack(
        children: [
          Hero(
            tag: 'HEADER',
            child: Container(
              decoration: BoxDecoration(
                color: color,
                borderRadius: borderRadius,
                boxShadow: boxShadow,
              ),
            ),
          ),
          FlexibleSpaceBar(
            title: Stack(
              children: [
                Align(
                  alignment: alignment ?? Alignment.lerp(Alignment.centerLeft, Alignment.bottomLeft, 0.5)!,
                  child: child,
                ),
              ],
            ),
            titlePadding: EdgeInsets.zero,
            background: Stack(
              children: [
                Align(
                  alignment: flexibleAlignment ?? Alignment.bottomCenter,
                  child: flexibleChild,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
