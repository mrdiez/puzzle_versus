import 'package:flutter/cupertino.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/utils/context.dart';

class ConstrainedContainer extends StatelessWidget {
  const ConstrainedContainer({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final width = $Screen.size(context.screenSize.width) > s ? $MaxWidth.s : $MaxWidth.m;
    final padding = $Screen.size(context.screenSize.width) <= s
        ? const EdgeInsets.symmetric(horizontal: $Padding.xs)
        : EdgeInsets.zero;
    return Padding(
      padding: padding,
      child: ConstrainedBox(
        constraints: BoxConstraints.tightFor(width: width),
        child: child,
      ),
    );
  }
}
