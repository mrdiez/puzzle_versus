import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';

class CustomText extends StatelessWidget {
  const CustomText(this.text, {Key? key, this.style, this.align, this.maxLines = 1}) : super(key: key);

  final String text;
  final TextStyle? style;
  final TextAlign? align;
  final int maxLines;

  @override
  Widget build(BuildContext context) {
    return AutoSizeText(
      text,
      maxLines: 1,
      style: style,
      minFontSize: $Font.xxxs,
    );
  }
}
