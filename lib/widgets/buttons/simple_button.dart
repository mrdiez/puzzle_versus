import 'package:flutter/material.dart';
import 'package:ms_material_color/ms_material_color.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/animated_button.dart';
import 'package:puzzle_versus/widgets/texts/custom_text.dart';

class SimpleButton extends StatelessWidget {
  const SimpleButton({Key? key, required this.text, this.onTap, this.tag, this.color, this.height}) : super(key: key);

  final String text;
  final Function()? onTap;
  final String? tag;
  final MsMaterialColor? color;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: $MaxWidth.xxs, height: height ?? $ButtonHeight.m),
      child: AnimatedButton(
        tag: tag,
        color: color,
        onTap: onTap,
        builder: (context, isHovered, isClicked) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: $Padding.l),
            child: Center(
              child: CustomText(text, style: context.textTheme.button),
            ),
          );
        },
      ),
    );
  }
}
