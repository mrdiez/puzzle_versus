import 'package:flutter/material.dart';
import 'package:ms_material_color/ms_material_color.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/utils/widget.dart';
import 'package:puzzle_versus/widgets/misc/mouse_listener.dart';

class AnimatedButton extends StatelessWidget {
  const AnimatedButton(
      {Key? key, required this.builder, this.tag, this.onTap, this.color, this.elevation = $ShadowSize.m})
      : super(key: key);

  final String? tag;
  final double elevation;
  final MsMaterialColor? color;
  final Function()? onTap;
  final Widget Function(BuildContext context, bool isHovered, bool isClicked) builder;

  @override
  Widget build(BuildContext context) {
    final light = color?.shade200 ?? lightColor;
    final dark = color ?? lightGreyColor;
    return MouseListener(
        onTapUp: onTap,
        builder: (context, isHovered, isClicked) {
          final transform = Matrix4.identity();
          isHovered = onTap != null ? isHovered : false;
          isClicked = onTap != null ? isClicked : false;
          if (isClicked) {
            transform.translate(elevation, elevation, 0);
          } else if (isHovered) {
            transform.translate(-elevation / 2, -elevation / 2, 0);
          }
          return AnimatedContainer(
            duration: Duration(milliseconds: isClicked ? 50 : 250),
            transform: transform,
            decoration: BoxDecoration(
              boxShadow: isClicked
                  ? [
                      BoxShadow(
                        color: darkShadowColor,
                        spreadRadius: elevation / 2,
                        blurRadius: 0,
                        offset: Offset(-elevation, -elevation), // changes position of shadow
                      ),
                    ]
                  : [
                      BoxShadow(
                        color: darkShadowColor,
                        spreadRadius: elevation / 2,
                        blurRadius: 0,
                        offset: isHovered
                            ? Offset(elevation * 1.5, elevation * 1.5)
                            : Offset(elevation, elevation), // changes position of shadow
                      ),
                      if (onTap != null)
                        BoxShadow(
                          color: lightShadowColor,
                          spreadRadius: 0,
                          blurRadius: isHovered ? 0 : elevation,
                          offset: isHovered
                              ? const Offset(0, 0)
                              : Offset(-elevation, -elevation), // changes position of shadow
                        ),
                    ],
              borderRadius: const BorderRadius.all($Radius.m),
              gradient: LinearGradient(
                  begin: isClicked ? $Alignment.bottomRightGradient / 3 : $Alignment.bottomRightGradient,
                  end: isClicked
                      ? $Alignment.topLeftGradient + $Alignment.topLeftGradient * 1 / 3
                      : $Alignment.topLeftGradient,
                  colors: [
                    onTap != null && (isClicked || isHovered) ? light : dark,
                    onTap == null || isClicked ? dark : light
                  ]),
              color: dark,
            ),
            child: Theme(
              data: context.theme.copyWith(
                  textTheme: context.textTheme.copyWith(
                      button: context.textTheme.button!.copyWith(color: isClicked || isHovered ? blackColor : null))),
              child: Builder(builder: (context) {
                return builder(context, isHovered, isClicked);
              }),
            ),
          ).wrapWithHero(tag);
        });
  }
}
