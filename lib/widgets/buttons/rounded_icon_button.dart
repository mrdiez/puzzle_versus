import 'package:flutter/material.dart';
import 'package:ms_material_color/ms_material_color.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/widgets/buttons/animated_button.dart';

class RoundedIconButton extends StatelessWidget {
  const RoundedIconButton({Key? key, required this.icon, this.onTap, this.tag, this.color, this.height}) : super(key: key);

  final IconData icon;
  final Function()? onTap;
  final String? tag;
  final MsMaterialColor? color;
  final double? height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? $ButtonHeight.m,
      width: height ?? $ButtonHeight.m,
      child: AnimatedButton(
        tag: tag,
        color: color,
        onTap: onTap,
        builder: (context, isHovered, isClicked) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: $Padding.xxxs),
            child: Center(
              child: Icon(icon, size: $Icon.m),
            ),
          );
        },
      ),
    );
  }
}
