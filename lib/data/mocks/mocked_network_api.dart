import 'package:dartx/dartx.dart';
import 'package:puzzle_versus/modules/game/data/mocks/mocked_scores.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/services/network_api.dart';

class MockedNetworkApi<T> extends NetworkApi<T> {
  MockedNetworkApi({
    required T Function(Map<String, dynamic> data) decode,
    required Map<String, dynamic> Function(T data) encode,
  }) : super(decode: decode, encode: encode);

  @override
  Future<T> get(int id, [String? _path]) => Future.value(getModel<T>(id));

  @override
  Future<List<T>> getMany([String? _path]) => Future.value(getManyModels<T>());

  @override
  Future<Map<String, dynamic>> post(Map<String, dynamic> data, [String? _path]) => Future.value({});

  static final Map<Type, List<dynamic>> _allData = {
    Score: mockedScoresData,
  };
  static final Map<Type, List<dynamic>> _allModels = {
    Score: mockedScores,
  };

  static Map<String, dynamic>? getData<T>(int id) =>
      _allData.containsKey(T) ? _allData[T]!.firstOrNullWhere((mock) => mock.id == id) : null;
  static List<Map<String, dynamic>> getManyData<T>() =>
      _allData.containsKey(T) ? _allData[T] as List<Map<String, dynamic>> : <Map<String, dynamic>>[];

  static T? getModel<T>(int id) =>
      _allModels.containsKey(T) ? _allModels[T]!.firstOrNullWhere((mock) => mock.id == id) : null;
  static List<T> getManyModels<T>() => _allModels.containsKey(T) ? _allModels[T] as List<T> : <T>[];
}
