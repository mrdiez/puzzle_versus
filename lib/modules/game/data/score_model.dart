import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/utils/bool.dart';
import 'package:puzzle_versus/utils/duration.dart';
import 'package:puzzle_versus/utils/number.dart';

part 'score_model.g.dart';

/// To generate @JsonSerializable classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@JsonSerializable()
class Score {
  Score({
    this.id = 0,
    required this.name,
    required this.duration,
    required this.moves,
    required this.gameMode,
    required this.level,
    required this.cheater,
  });
  final int id;
  final String name;
  @JsonKey(fromJson: IntUtils.toDuration, toJson: DurationUtils.toInt)
  final Duration duration;
  final int moves;
  final GameMode gameMode;
  final GameLevel level;
  @JsonKey(fromJson: IntUtils.toBool, toJson: BoolUtils.toInt)
  final bool cheater;

  factory Score.fromJson(Map<String, dynamic> json) => _$ScoreFromJson(json);
  Map<String, dynamic> toJson() => _$ScoreToJson(this);
}
