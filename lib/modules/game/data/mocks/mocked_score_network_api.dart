import 'package:dartx/dartx.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/data/mocks/mocked_network_api.dart';
import 'package:puzzle_versus/modules/game/data/mocks/mocked_scores.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';

class MockedScoreNetworkApi extends MockedNetworkApi<Score> {
  MockedScoreNetworkApi()
      : super(decode: (Map<String, dynamic> data) => Score.fromJson(data), encode: (Score data) => data.toJson());

  @override
  Future<List<Score>> getMany([String? _path]) {
    _path = '${path ?? ''}${_path ?? ''}';
    final levels = GameLevel.values.map((e) => e.name.toString()).toList();
    final scores = MockedNetworkApi.getManyModels<Score>();
    final level = levels.firstOrNullWhere((l) => _path!.contains(l))!;
    return Future.value(scores.where((s) => s.level.name == level).toList());
  }

  @override
  Future<Map<String, dynamic>> post(Map<String, dynamic> data, [String? _path]) {
    mockedScoresData.add(data);
    mockedScores.add(Score.fromJson(data));
    return Future.value({});
  }
}
