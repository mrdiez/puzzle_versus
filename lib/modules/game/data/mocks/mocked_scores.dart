import 'package:puzzle_versus/modules/game/data/score_model.dart';

List<Map<String, dynamic>> mockedScoresData = [
  {'id': 0, 'name': 'Admin', 'duration': 6399900, 'moves': 666, 'gameMode': 1, 'level': 1, 'cheater': 0},
  {'id': 1, 'name': 'Johnny', 'duration': 86399900, 'moves': 14, 'gameMode': 1, 'level': 1, 'cheater': 1},
  {'id': 2, 'name': 'Marco', 'duration': 399900, 'moves': 20, 'gameMode': 1, 'level': 1, 'cheater': 1},
  {'id': 3, 'name': 'Juan', 'duration': 6344900, 'moves': 99, 'gameMode': 1, 'level': 1, 'cheater': 0},
  {'id': 4, 'name': 'Kylian', 'duration': 6244900, 'moves': 99, 'gameMode': 1, 'level': 1, 'cheater': 0},
  {'id': 5, 'name': 'Astrid', 'duration': 1399900, 'moves': 1287, 'gameMode': 1, 'level': 1, 'cheater': 1},
  {'id': 6, 'name': 'Kamel', 'duration': 195900, 'moves': 2, 'gameMode': 1, 'level': 1, 'cheater': 0},
  {'id': 7, 'name': 'Xiang', 'duration': 23399900, 'moves': 322, 'gameMode': 1, 'level': 1, 'cheater': 0},
  {'id': 8, 'name': 'Admin', 'duration': 6399900, 'moves': 666, 'gameMode': 1, 'level': 2, 'cheater': 1},
  {'id': 9, 'name': 'Johnny', 'duration': 86399900, 'moves': 14, 'gameMode': 1, 'level': 2, 'cheater': 0},
  {'id': 10, 'name': 'Marco', 'duration': 399900, 'moves': 20, 'gameMode': 1, 'level': 2, 'cheater': 0},
  {'id': 11, 'name': 'Juan', 'duration': 6344900, 'moves': 99, 'gameMode': 1, 'level': 2, 'cheater': 1},
  {'id': 12, 'name': 'Kylian', 'duration': 6244900, 'moves': 99, 'gameMode': 1, 'level': 2, 'cheater': 0},
  {'id': 13, 'name': 'Astrid', 'duration': 1399900, 'moves': 1287, 'gameMode': 1, 'level': 2, 'cheater': 1},
  {'id': 14, 'name': 'Kamel', 'duration': 195900, 'moves': 2, 'gameMode': 1, 'level': 2, 'cheater': 0},
  {'id': 15, 'name': 'Xiang', 'duration': 23399900, 'moves': 322, 'gameMode': 1, 'level': 2, 'cheater': 0},
  {'id': 16, 'name': 'Admin', 'duration': 6399900, 'moves': 666, 'gameMode': 1, 'level': 3, 'cheater': 0},
  {'id': 17, 'name': 'Johnny', 'duration': 86399900, 'moves': 14, 'gameMode': 1, 'level': 3, 'cheater': 0},
  {'id': 18, 'name': 'Marco', 'duration': 399900, 'moves': 20, 'gameMode': 1, 'level': 3, 'cheater': 1},
  {'id': 19, 'name': 'Juan', 'duration': 6344900, 'moves': 99, 'gameMode': 1, 'level': 3, 'cheater': 0},
  {'id': 20, 'name': 'Kylian', 'duration': 6244900, 'moves': 99, 'gameMode': 1, 'level': 3, 'cheater': 0},
  {'id': 21, 'name': 'Astrid', 'duration': 1399900, 'moves': 1287, 'gameMode': 1, 'level': 3, 'cheater': 1},
  {'id': 22, 'name': 'Kamel', 'duration': 195900, 'moves': 2, 'gameMode': 1, 'level': 3, 'cheater': 1},
  {'id': 23, 'name': 'Xiang', 'duration': 23399900, 'moves': 322, 'gameMode': 1, 'level': 3, 'cheater': 0},
];

List<Score> mockedScores = mockedScoresData.map((data) => Score.fromJson(data)).toList();
