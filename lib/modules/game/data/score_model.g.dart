// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'score_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Score _$ScoreFromJson(Map<String, dynamic> json) => Score(
      id: json['id'] as int? ?? 0,
      name: json['name'] as String,
      duration: IntUtils.toDuration(json['duration'] as int),
      moves: json['moves'] as int,
      gameMode: $enumDecode(_$GameModeEnumMap, json['gameMode']),
      level: $enumDecode(_$GameLevelEnumMap, json['level']),
      cheater: IntUtils.toBool(json['cheater'] as int),
    );

Map<String, dynamic> _$ScoreToJson(Score instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'duration': DurationUtils.toInt(instance.duration),
      'moves': instance.moves,
      'gameMode': _$GameModeEnumMap[instance.gameMode],
      'level': _$GameLevelEnumMap[instance.level],
      'cheater': BoolUtils.toInt(instance.cheater),
    };

const _$GameModeEnumMap = {
  GameMode.time: 1,
  GameMode.local: 2,
  GameMode.online: 3,
};

const _$GameLevelEnumMap = {
  GameLevel.easy: 1,
  GameLevel.normal: 2,
  GameLevel.hard: 3,
};
