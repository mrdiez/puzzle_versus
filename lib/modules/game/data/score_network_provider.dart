import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/environment.dart';
import 'package:puzzle_versus/modules/game/data/mocks/mocked_score_network_api.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/services/network_api.dart';
import 'package:puzzle_versus/services/reset_state_trigger.dart';

abstract class ScoreNetworkProvider {
  static final _api = Provider<NetworkApi<Score>>((ref) {
    return !Environment.isMock
        ? NetworkApi<Score>(
            path: '/scores',
            decode: (Map<String, dynamic> data) => Score.fromJson(data),
            encode: (Score data) => data.toJson(),
          )
        : MockedScoreNetworkApi();
  });

  static final all = {
    GameLevel.easy: FutureProvider<List<Score>>((ref) {
      ref.watch(ResetStateTrigger.provider);
      return ref.watch(_api).getMany('/easy');
    }),
    GameLevel.normal: FutureProvider<List<Score>>((ref) {
      ref.watch(ResetStateTrigger.provider);
      return ref.watch(_api).getMany('/normal');
    }),
    GameLevel.hard: FutureProvider<List<Score>>((ref) {
      ref.watch(ResetStateTrigger.provider);
      return ref.watch(_api).getMany('/hard');
    }),
  };

  static final save =
      FutureProvider.family<Map<String, dynamic>?, Score>((ref, data) => ref.read(_api).post(data.toJson()));
}
