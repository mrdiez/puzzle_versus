// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_creation_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GameCreationViewStateTearOff {
  const _$GameCreationViewStateTearOff();

  _GameCreationViewState call(
      {String? pickedImageName,
      Uint8List? imageBytes,
      String? url,
      bool isLoading = false,
      bool urlSubmitted = false,
      GameLevel level = GameLevel.easy,
      bool cheaterMode = false}) {
    return _GameCreationViewState(
      pickedImageName: pickedImageName,
      imageBytes: imageBytes,
      url: url,
      isLoading: isLoading,
      urlSubmitted: urlSubmitted,
      level: level,
      cheaterMode: cheaterMode,
    );
  }
}

/// @nodoc
const $GameCreationViewState = _$GameCreationViewStateTearOff();

/// @nodoc
mixin _$GameCreationViewState {
  String? get pickedImageName => throw _privateConstructorUsedError;
  Uint8List? get imageBytes => throw _privateConstructorUsedError;
  String? get url => throw _privateConstructorUsedError;
  bool get isLoading => throw _privateConstructorUsedError;
  bool get urlSubmitted => throw _privateConstructorUsedError;
  GameLevel get level => throw _privateConstructorUsedError;
  bool get cheaterMode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GameCreationViewStateCopyWith<GameCreationViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameCreationViewStateCopyWith<$Res> {
  factory $GameCreationViewStateCopyWith(GameCreationViewState value,
          $Res Function(GameCreationViewState) then) =
      _$GameCreationViewStateCopyWithImpl<$Res>;
  $Res call(
      {String? pickedImageName,
      Uint8List? imageBytes,
      String? url,
      bool isLoading,
      bool urlSubmitted,
      GameLevel level,
      bool cheaterMode});
}

/// @nodoc
class _$GameCreationViewStateCopyWithImpl<$Res>
    implements $GameCreationViewStateCopyWith<$Res> {
  _$GameCreationViewStateCopyWithImpl(this._value, this._then);

  final GameCreationViewState _value;
  // ignore: unused_field
  final $Res Function(GameCreationViewState) _then;

  @override
  $Res call({
    Object? pickedImageName = freezed,
    Object? imageBytes = freezed,
    Object? url = freezed,
    Object? isLoading = freezed,
    Object? urlSubmitted = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
  }) {
    return _then(_value.copyWith(
      pickedImageName: pickedImageName == freezed
          ? _value.pickedImageName
          : pickedImageName // ignore: cast_nullable_to_non_nullable
              as String?,
      imageBytes: imageBytes == freezed
          ? _value.imageBytes
          : imageBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      urlSubmitted: urlSubmitted == freezed
          ? _value.urlSubmitted
          : urlSubmitted // ignore: cast_nullable_to_non_nullable
              as bool,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$GameCreationViewStateCopyWith<$Res>
    implements $GameCreationViewStateCopyWith<$Res> {
  factory _$GameCreationViewStateCopyWith(_GameCreationViewState value,
          $Res Function(_GameCreationViewState) then) =
      __$GameCreationViewStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? pickedImageName,
      Uint8List? imageBytes,
      String? url,
      bool isLoading,
      bool urlSubmitted,
      GameLevel level,
      bool cheaterMode});
}

/// @nodoc
class __$GameCreationViewStateCopyWithImpl<$Res>
    extends _$GameCreationViewStateCopyWithImpl<$Res>
    implements _$GameCreationViewStateCopyWith<$Res> {
  __$GameCreationViewStateCopyWithImpl(_GameCreationViewState _value,
      $Res Function(_GameCreationViewState) _then)
      : super(_value, (v) => _then(v as _GameCreationViewState));

  @override
  _GameCreationViewState get _value => super._value as _GameCreationViewState;

  @override
  $Res call({
    Object? pickedImageName = freezed,
    Object? imageBytes = freezed,
    Object? url = freezed,
    Object? isLoading = freezed,
    Object? urlSubmitted = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
  }) {
    return _then(_GameCreationViewState(
      pickedImageName: pickedImageName == freezed
          ? _value.pickedImageName
          : pickedImageName // ignore: cast_nullable_to_non_nullable
              as String?,
      imageBytes: imageBytes == freezed
          ? _value.imageBytes
          : imageBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String?,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      urlSubmitted: urlSubmitted == freezed
          ? _value.urlSubmitted
          : urlSubmitted // ignore: cast_nullable_to_non_nullable
              as bool,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_GameCreationViewState extends _GameCreationViewState
    with DiagnosticableTreeMixin {
  const _$_GameCreationViewState(
      {this.pickedImageName,
      this.imageBytes,
      this.url,
      this.isLoading = false,
      this.urlSubmitted = false,
      this.level = GameLevel.easy,
      this.cheaterMode = false})
      : super._();

  @override
  final String? pickedImageName;
  @override
  final Uint8List? imageBytes;
  @override
  final String? url;
  @JsonKey()
  @override
  final bool isLoading;
  @JsonKey()
  @override
  final bool urlSubmitted;
  @JsonKey()
  @override
  final GameLevel level;
  @JsonKey()
  @override
  final bool cheaterMode;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GameCreationViewState(pickedImageName: $pickedImageName, imageBytes: $imageBytes, url: $url, isLoading: $isLoading, urlSubmitted: $urlSubmitted, level: $level, cheaterMode: $cheaterMode)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GameCreationViewState'))
      ..add(DiagnosticsProperty('pickedImageName', pickedImageName))
      ..add(DiagnosticsProperty('imageBytes', imageBytes))
      ..add(DiagnosticsProperty('url', url))
      ..add(DiagnosticsProperty('isLoading', isLoading))
      ..add(DiagnosticsProperty('urlSubmitted', urlSubmitted))
      ..add(DiagnosticsProperty('level', level))
      ..add(DiagnosticsProperty('cheaterMode', cheaterMode));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GameCreationViewState &&
            const DeepCollectionEquality()
                .equals(other.pickedImageName, pickedImageName) &&
            const DeepCollectionEquality()
                .equals(other.imageBytes, imageBytes) &&
            const DeepCollectionEquality().equals(other.url, url) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality()
                .equals(other.urlSubmitted, urlSubmitted) &&
            const DeepCollectionEquality().equals(other.level, level) &&
            const DeepCollectionEquality()
                .equals(other.cheaterMode, cheaterMode));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(pickedImageName),
      const DeepCollectionEquality().hash(imageBytes),
      const DeepCollectionEquality().hash(url),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(urlSubmitted),
      const DeepCollectionEquality().hash(level),
      const DeepCollectionEquality().hash(cheaterMode));

  @JsonKey(ignore: true)
  @override
  _$GameCreationViewStateCopyWith<_GameCreationViewState> get copyWith =>
      __$GameCreationViewStateCopyWithImpl<_GameCreationViewState>(
          this, _$identity);
}

abstract class _GameCreationViewState extends GameCreationViewState {
  const factory _GameCreationViewState(
      {String? pickedImageName,
      Uint8List? imageBytes,
      String? url,
      bool isLoading,
      bool urlSubmitted,
      GameLevel level,
      bool cheaterMode}) = _$_GameCreationViewState;
  const _GameCreationViewState._() : super._();

  @override
  String? get pickedImageName;
  @override
  Uint8List? get imageBytes;
  @override
  String? get url;
  @override
  bool get isLoading;
  @override
  bool get urlSubmitted;
  @override
  GameLevel get level;
  @override
  bool get cheaterMode;
  @override
  @JsonKey(ignore: true)
  _$GameCreationViewStateCopyWith<_GameCreationViewState> get copyWith =>
      throw _privateConstructorUsedError;
}
