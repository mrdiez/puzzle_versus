import 'dart:async';
import 'dart:typed_data';

import 'package:dartx/dartx.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_view.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/services/image_manager.dart';
import 'package:puzzle_versus/services/reset_state_trigger.dart';
import 'package:puzzle_versus/utils/string.dart';

part 'game_creation_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GameCreationEvent with _$GameCreationEvent {
  const factory GameCreationEvent.startGame() = StartGame;
  const factory GameCreationEvent.imageAccessError() = ImageAccessError;
  const factory GameCreationEvent.imageDecodeError() = ImageDecodeError;
}

class GameCreationLogic extends StateNotifier<GameCreationViewState> {
  static StreamController<GameCreationEvent>? eventStreamController;

  static final eventProvider = StreamProvider<GameCreationEvent>((ref) {
    eventStreamController = StreamController<GameCreationEvent>();
    ref.onDispose(eventStreamController!.close);
    return eventStreamController!.stream;
  });

  static final provider =
      StateNotifierProvider.family<GameCreationLogic, GameCreationViewState, GameMode>((ref, gameMode) {
    ref.watch(ResetStateTrigger.provider);
    return GameCreationLogic(initialState: const GameCreationViewState(), gameMode: gameMode);
  });

  GameCreationLogic({required GameCreationViewState initialState, required this.gameMode}) : super(initialState);

  final GameMode gameMode;
  final TextEditingController textEditingController = TextEditingController();
  final ScrollController scrollController = ScrollController();

  Future<void> ensureImageIsVisible() async {
    await Future.delayed($Duration.fast);
    scrollController.animateTo(
      scrollController.position.maxScrollExtent,
      duration: $Duration.fast,
      curve: Curves.fastOutSlowIn,
    );
  }

  Future<void> onUpload() async {
    state = state.copyWith(isLoading: true);
    try {
      final pickedImage = await ImagePicker().pickImage(source: ImageSource.gallery);
      final pickedImageBytes = await pickedImage?.readAsBytes();
      if (pickedImage != null && pickedImageBytes != null) {
        final resizedBytes = await compute(ImageManager.limitImageBytesSize, [$GameImageSize.max, pickedImageBytes]);
        textEditingController.clear();
        state = state.copyWith(
          pickedImageName: pickedImage.name,
          imageBytes: resizedBytes,
          url: null,
          urlSubmitted: false,
        );
        await ensureImageIsVisible();
      }
    } catch (e) {
      eventStreamController?.add(const GameCreationEvent.imageAccessError());
    }
    state = state.copyWith(isLoading: false);
  }

  bool get cheaterMode => state.cheaterMode;
  set cheaterMode(bool value) => state = state.copyWith(cheaterMode: value);

  GameLevel get level => state.level;
  set level(GameLevel value) => state = state.copyWith(level: value);

  String? get url => state.url;
  set url(String? value) {
    state = state.copyWith(url: value, urlSubmitted: false);
  }

  bool get isUrl => state.url != null && state.url!.isValidUrl;

  bool get isUrlError => state.url.isNotNullOrEmpty && !isUrl;
  bool get isImageNetworkLoaded => state.urlSubmitted && !state.isLoading;

  void onUrlSubmit() async {
    state = state.copyWith(
      pickedImageName: null,
      imageBytes: null,
      urlSubmitted: true,
      isLoading: true,
    );
    await ensureImageIsVisible();
  }

  void resetUrl() async {
    textEditingController.clear();
    state = state.copyWith(
      url: null,
      urlSubmitted: false,
    );
  }

  List<Score>? sortAndFilterScores(List<Score>? scores) {
    return scores?.where((s) {
      return s.cheater == state.cheaterMode;
    }).toList()
      ?..sort((a, b) {
        if (a.duration == b.duration) return a.moves > b.moves ? 1 : -1;
        return a.duration > b.duration ? 1 : -1;
      });
  }

  void startGame() => eventStreamController?.add(const GameCreationEvent.startGame());

  Future<void> onImageNetworkLoaded(Uint8List? imageBytes) async {
    if (imageBytes?.isNotEmpty ?? false) {
      state = state.copyWith(isLoading: false, imageBytes: imageBytes);
    } else {
      eventStreamController?.add(const GameCreationEvent.imageDecodeError());
      textEditingController.clear();
      state = state.copyWith(
        pickedImageName: null,
        imageBytes: null,
        url: null,
        urlSubmitted: false,
        isLoading: false,
      );
    }
  }
}
