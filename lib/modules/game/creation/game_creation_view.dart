import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/config/translations/gen/l10n.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_app_bar.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_footer.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_main.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/simple_button.dart';
import 'package:puzzle_versus/widgets/layout/sliver.view.dart';

part 'game_creation_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GameCreationViewState with _$GameCreationViewState {
  const factory GameCreationViewState({
    String? pickedImageName,
    Uint8List? imageBytes,
    String? url,
    @Default(false) bool isLoading,
    @Default(false) bool urlSubmitted,
    @Default(GameLevel.easy) GameLevel level,
    @Default(false) bool cheaterMode,
  }) = _GameCreationViewState;

  const GameCreationViewState._();
}

class GameCreationView extends ConsumerWidget {
  static Map<GameLevel, String> levelLabels = {
    GameLevel.easy: Translation.current.easy,
    GameLevel.normal: Translation.current.normal,
    GameLevel.hard: Translation.current.hard,
  };

  const GameCreationView(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  void onEvent(BuildContext context, GameCreationEvent event) {
    if (event is ImageDecodeError) {
      context.showErrorSnackBar(
          '${context.translation.unableToSaveImage}${kIsWeb ? '\n${context.translation.invalidUrlOrProtectedImage}' : ''}');
    } else if (event is ImageAccessError) {
      context.showErrorSnackBar(
          '${context.translation.unableToAccessImage}\n${context.translation.pleaseCheckGalleryAccessSettings}');
    } else if (event is StartGame) {
      context.go(playRoutes[gameMode]!);
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    final logic = ref.read(GameCreationLogic.provider(gameMode).notifier);
    ref.listen<AsyncValue<GameCreationEvent>>(
        GameCreationLogic.eventProvider, (previous, next) => onEvent(context, next.value!));
    final isLoading = ref.watch(GameCreationLogic.provider(gameMode).select((s) => s.isLoading));
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        body: !notReadyModes.contains(gameMode)
            ? Stack(
                children: [
                  LayoutBuilder(builder: (context, constraints) {
                    return SliverView(
                      controller: logic.scrollController,
                      nested: true,
                      appBar: GameCreationAppBar(gameMode, constraints.maxWidth),
                      child: GameCreationMain(gameMode),
                    );
                  }),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: GameCreationFooter(gameMode),
                  ),
                  if (isLoading) const Center(child: CircularProgressIndicator()),
                ],
              )
            : Padding(
                padding: const EdgeInsets.all($Padding.xxl),
                child: Stack(
                  children: [
                    Center(
                      child: Text(
                        context.translation.sorryModeNotReadyYet,
                        maxLines: 5,
                        style: context.textTheme.headline3,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: SimpleButton(
                        tag: gameMode.toString(),
                        color: gameColors[gameMode],
                        height: $ButtonHeight.l,
                        onTap: () {
                          if (context.popRoute != null) {
                            context.popRoute!.call();
                          } else {
                            context.go(homeRoute);
                          }
                        },
                        text: context.translation.cancel,
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
