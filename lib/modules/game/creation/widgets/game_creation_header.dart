import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_view.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/level_button.dart';
import 'package:puzzle_versus/utils/context.dart';

class GameCreationHeader extends StatelessWidget {
  const GameCreationHeader(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final logic = ref.read(GameCreationLogic.provider(gameMode).notifier);
      final state = ref.watch(GameCreationLogic.provider(gameMode));
      return Column(
        children: [
          Text(GameCreationView.levelLabels[state.level]!, style: context.textTheme.subtitle1),
          $Gap.xs,
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LevelButton(
                onTap: () => logic.level = GameLevel.easy,
                active: true,
              ),
              LevelButton(
                onTap: () => logic.level = GameLevel.normal,
                active: state.level != GameLevel.easy,
              ),
              LevelButton(
                onTap: () => logic.level = GameLevel.hard,
                active: state.level == GameLevel.hard,
              ),
            ],
          ),
          $Gap.xs,
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(context.translation.cheaterMode, style: context.textTheme.subtitle2),
              Switch(
                value: state.cheaterMode,
                onChanged: (value) => logic.cheaterMode = value,
              ),
            ],
          ),
        ],
      );
    });
  }
}
