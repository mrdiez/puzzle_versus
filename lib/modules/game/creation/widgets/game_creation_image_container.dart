import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_image.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/layout/simple_container.dart';

class GameCreationImageContainer extends StatelessWidget {
  const GameCreationImageContainer(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context) {
    return SimpleContainer(
      color: whiteColor,
      child: SizedBox(
        width: $MaxWidth.s,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Padding(
              padding:
                  const EdgeInsets.only(left: $Padding.s, top: $Padding.s, right: $Padding.s, bottom: $Padding.xxxl),
              child: Consumer(builder: (context, ref, child) {
                final logic = ref.watch(GameCreationLogic.provider(gameMode).notifier);
                final state = ref.watch(GameCreationLogic.provider(gameMode));
                return GameCreationImage(
                  onImageNetworkLoaded: logic.onImageNetworkLoaded,
                  imageBytes: state.imageBytes,
                  imageUrl: state.urlSubmitted ? state.url : null,
                );
              }),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: $Padding.m, vertical: $Padding.s),
              child: Consumer(builder: (context, ref, child) {
                final state = ref.watch(GameCreationLogic.provider(gameMode));
                return Text(
                  state.urlSubmitted ? state.url! : state.pickedImageName!,
                  style: context.textTheme.caption,
                  textAlign: TextAlign.center,
                  maxLines: 1,
                );
              }),
            )
          ],
        ),
      ),
    );
  }
}
