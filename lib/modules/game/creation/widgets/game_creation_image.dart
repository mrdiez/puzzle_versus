import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/plugins/image_network_fork.dart';

class GameCreationImage extends StatelessWidget {
  const GameCreationImage({Key? key, this.imageBytes, this.imageUrl, required this.onImageNetworkLoaded})
      : assert(imageBytes != null || imageUrl != null),
        super(key: key);

  final Uint8List? imageBytes;
  final String? imageUrl;
  final Future<void> Function(Uint8List?) onImageNetworkLoaded;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all($Radius.s),
      child: LayoutBuilder(builder: (context, constraints) {
        return Hero(
          tag: 'GAME_IMAGE',
          child: SizedBox(
            height: constraints.maxWidth,
            width: constraints.maxWidth,
            child: imageBytes?.isEmpty ?? true
                ? ImageNetworkFork(
                    image: imageUrl!,
                    fitAndroidIos: BoxFit.cover,
                    fitWeb: BoxFitWeb.cover,
                    height: constraints.maxWidth,
                    width: constraints.maxWidth,
                    onImageNetworkLoaded: onImageNetworkLoaded,
                    onLoading: Container(),
                    onError: Builder(
                      builder: (context) {
                        return Center(
                          child: Text(
                            context.translation.thisIsNotAValidUrl,
                            style: context.textTheme.bodyText1!.copyWith(color: negativeColor),
                          ),
                        );
                      },
                    ),
                  )
                : Image.memory(
                    imageBytes!,
                    fit: BoxFit.cover,
                    height: constraints.maxWidth,
                    width: constraints.maxWidth,
                  ),
          ),
        );
      }),
    );
  }
}
