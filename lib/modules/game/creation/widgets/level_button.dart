import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/widgets/misc/mouse_listener.dart';

class LevelButton extends StatelessWidget {
  const LevelButton({Key? key, required this.onTap, required this.active}) : super(key: key);

  final Function() onTap;
  final bool active;

  @override
  Widget build(BuildContext context) {
    return MouseListener(
      onTapUp: onTap,
      builder: (context, isHovered, isClicked) {
        return SizedBox(
          height: $Icon.l + $ShadowSize.m,
          width: $Icon.l + $ShadowSize.m,
          child: Stack(
            children: [
              if (isClicked)
                Align(
                  alignment: AlignmentDirectional.center,
                  child: Icon(
                    active ? Icons.star_rounded : Icons.star_border_rounded,
                    size: $Icon.l,
                    color: darkShadowColor,
                  ),
                ),
              if (!isClicked)
                Align(
                  alignment: AlignmentDirectional.bottomEnd,
                  child: Icon(
                    active ? Icons.star_rounded : Icons.star_border_rounded,
                    size: $Icon.l,
                    color: darkShadowColor,
                  ),
                ),
              Align(
                alignment: isClicked ? AlignmentDirectional.bottomEnd : AlignmentDirectional.center,
                child: Icon(
                  active ? Icons.star_rounded : Icons.star_border_rounded,
                  size: $Icon.l,
                  color: secondaryColor,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
