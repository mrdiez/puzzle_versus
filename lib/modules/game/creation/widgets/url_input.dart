import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/environment.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/rounded_icon_button.dart';

class UrlInput extends ConsumerWidget {
  const UrlInput(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(GameCreationLogic.provider(gameMode).notifier);
    ref.watch(GameCreationLogic.provider(gameMode));
    return Row(
      children: [
        Expanded(
          child: TextField(
            controller: logic.textEditingController,
            style: context.textTheme.bodyText1,
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.only(bottom: $Padding.xxs),
              constraints: BoxConstraints.tightFor(
                  height: logic.isUrlError && Environment.isMobile ? $InputHeight.m : $InputHeight.s),
              hintText: context.translation.orPasteImageUrlHere,
              hintStyle: context.textTheme.bodyText2,
              errorText: logic.isUrlError ? context.translation.thisIsNotAValidUrl : null,
              border: InputBorder.none,
            ),
            onChanged: (value) => logic.url = value,
            onSubmitted: logic.isUrl ? (value) => logic.onUrlSubmit() : null,
          ),
        ),
        if (logic.isUrl && !logic.isImageNetworkLoaded) ...[
          $Gap.s,
          RoundedIconButton(
            icon: Icons.check_rounded,
            color: positiveColor,
            onTap: logic.onUrlSubmit,
            height: $ButtonHeight.s,
          ),
        ],
        if (logic.isUrlError || logic.isImageNetworkLoaded) ...[
          $Gap.s,
          RoundedIconButton(
            icon: Icons.close_rounded,
            color: negativeColor,
            onTap: logic.resetUrl,
            height: $ButtonHeight.s,
          ),
        ]
      ],
    );
  }
}
