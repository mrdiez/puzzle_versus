import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/simple_button.dart';

class GameCreationFooter extends StatelessWidget {
  const GameCreationFooter(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: $Padding.xxl, right: $Padding.xxl, bottom: $Padding.xxl),
      child: ConstrainedBox(
        constraints: const BoxConstraints.tightFor(width: $MaxWidth.s),
        child: Consumer(builder: (context, ref, child) {
          final logic = ref.read(GameCreationLogic.provider(gameMode).notifier);
          final state = ref.watch(GameCreationLogic.provider(gameMode));
          return Row(
            children: [
              Expanded(
                child: SimpleButton(
                  height: $ButtonHeight.l,
                  onTap: () {
                    if (context.popRoute != null) {
                      context.popRoute!.call();
                    } else {
                      context.go(homeRoute);
                    }
                  },
                  text: context.translation.cancel,
                ),
              ),
              $Gap.m,
              Expanded(
                child: SimpleButton(
                  tag: gameMode.toString(),
                  color: gameColors[gameMode],
                  height: $ButtonHeight.l,
                  onTap: state.isLoading ? null : logic.startGame,
                  text: context.translation.start,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
