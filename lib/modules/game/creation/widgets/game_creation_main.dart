import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_header.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_creation_image_container.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/game_score_item.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/modules/game/data/score_network_provider.dart';
import 'package:puzzle_versus/widgets/layout/sliver.view.dart';

class GameCreationMain extends StatelessWidget {
  const GameCreationMain(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: $Padding.xl),
      child: ConstrainedBox(
        constraints: const BoxConstraints.tightFor(width: $MaxWidth.s),
        child: Padding(
          padding: const EdgeInsets.only(top: $Padding.xxl),
          child: Consumer(builder: (context, ref, child) {
            final logic = ref.watch(GameCreationLogic.provider(gameMode).notifier);
            final state = ref.watch(GameCreationLogic.provider(gameMode));
            final isImage = ((state.pickedImageName != null && state.imageBytes != null) ||
                (state.urlSubmitted && state.url != null));
            return isImage
                ? SliverView(
                    overlap: true,
                    child: Column(
                      children: [
                        GameCreationHeader(gameMode),
                        GameCreationImageContainer(gameMode),
                        $Gap.xxxl,
                        $Gap.xxxl,
                      ],
                    ),
                  )
                : FutureBuilder<List<Score>>(
                    future: ref.read(ScoreNetworkProvider.all[state.level]!.future),
                    builder: (context, snapshot) {
                      final scores = logic.sortAndFilterScores(snapshot.data);
                      return SliverView(
                        overlap: true,
                        slivers: [
                          SliverToBoxAdapter(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: $Padding.m),
                              child: GameCreationHeader(gameMode),
                            ),
                          ),
                          if (scores != null && scores.isNotEmpty)
                            SliverList(
                              delegate: SliverChildBuilderDelegate(
                                (context, i) {
                                  return Padding(
                                    padding:
                                        const EdgeInsets.only(left: $Padding.s, bottom: $Padding.l, right: $Padding.s),
                                    child: GameScoreItem(i + 1, scores[i]),
                                  );
                                },
                                childCount: scores.length,
                              ),
                            ),
                          SliverToBoxAdapter(
                            child: Column(
                              children: const [$Gap.xxxl, $Gap.xxxl, $Gap.xxxl],
                            ),
                          )
                        ],
                      );
                    });
          }),
        ),
      ),
    );
  }
}
