import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/environment.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/creation/widgets/url_input.dart';
import 'package:puzzle_versus/modules/game/widgets/game_app_bar.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/simple_button.dart';
import 'package:puzzle_versus/widgets/images/logo.dart';

class GameCreationAppBar extends ConsumerWidget {
  const GameCreationAppBar(this.gameMode, this.maxWidth, {Key? key}) : super(key: key);

  final GameMode gameMode;
  final double maxWidth;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(GameCreationLogic.provider(gameMode).notifier);
    final availableWidth = $Screen.size(maxWidth);
    final isLarge = availableWidth > l;
    final isSmall = availableWidth <= s;
    final heightExpansion = $ButtonHeight.s * (isLarge ? 0 : (isSmall ? 2 : 1));
    final uploadButton = Padding(
      padding: EdgeInsets.only(top: isLarge && Environment.isMobile ? $Padding.m : $Padding.zero),
      child: SimpleButton(
        color: secondaryColor,
        onTap: logic.onUpload,
        height: $ButtonHeight.s,
        text: context.translation.uploadAnImage,
      ),
    );
    final urlInput = Padding(
      padding: EdgeInsets.only(top: isLarge && Environment.isMobile ? $Padding.m : $Padding.zero),
      child: UrlInput(gameMode),
    );
    final logo = Padding(
      padding: EdgeInsets.only(top: Environment.isMobile ? $Padding.m : $Padding.zero),
      child: Logo(
          onTap: () {
            if (context.popRoute != null) {
              context.popRoute!.call();
            } else {
              context.go(homeRoute);
            }
          },
          height: isLarge
              ? $LogoHeight.s
              : isSmall
                  ? $LogoHeight.xxs
                  : $LogoHeight.xs),
    );
    return GameAppBar(
      expandedHeight: $AppBarHeight.l + heightExpansion,
      child: Padding(
        padding: const EdgeInsets.all($Padding.m),
        child: isLarge
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [$Gap.xs, logo, $Gap.m, uploadButton, $Gap.m, Expanded(child: urlInput), $Gap.xs])
            : logo,
      ),
      alignment: isSmall || !isLarge ? Alignment.centerLeft : null,
      flexibleChild: !isLarge
          ? SizedBox(
              height: $AppBarHeight.l + heightExpansion,
              child: Padding(
                padding: isSmall
                    ? const EdgeInsets.only(left: $Padding.l, top: $Padding.xl, right: $Padding.l, bottom: $Padding.xs)
                    : const EdgeInsets.all($Padding.m),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (isSmall) ...[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: $Padding.xs),
                        child: uploadButton,
                      ),
                      urlInput
                    ],
                    if (!isSmall) Row(children: [$Gap.xs, uploadButton, $Gap.m, Expanded(child: urlInput), $Gap.xs]),
                  ],
                ),
              ),
            )
          : null,
    );
  }
}
