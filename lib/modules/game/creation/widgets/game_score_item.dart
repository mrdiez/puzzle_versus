import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/utils/duration.dart';
import 'package:puzzle_versus/widgets/layout/simple_container.dart';
import 'package:puzzle_versus/widgets/texts/custom_text.dart';

class GameScoreItem extends StatelessWidget {
  const GameScoreItem(this.position, this.score, {Key? key}) : super(key: key);

  final int position;
  final Score score;

  @override
  Widget build(BuildContext context) {
    return SimpleContainer(
      color: whiteColor,
      child: ListTile(
        contentPadding: const EdgeInsets.all($Padding.s),
        leading: CircleAvatar(child: Text(position.toString())),
        title: CustomText(score.name),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText('${score.moves} ${context.translation.moves(score.moves)}'),
            $Gap.xxs,
            CustomText(
              '${score.duration.hoursFirstDigit}${score.duration.hoursSecondDigit}h${score.duration.minutesFirstDigit}${score.duration.minutesSecondDigit}m${score.duration.secondsFirstDigit}${score.duration.secondsSecondDigit}s',
            ),
          ],
        ),
      ),
    );
  }
}
