// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_creation_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GameCreationEventTearOff {
  const _$GameCreationEventTearOff();

  StartGame startGame() {
    return const StartGame();
  }

  ImageAccessError imageAccessError() {
    return const ImageAccessError();
  }

  ImageDecodeError imageDecodeError() {
    return const ImageDecodeError();
  }
}

/// @nodoc
const $GameCreationEvent = _$GameCreationEventTearOff();

/// @nodoc
mixin _$GameCreationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startGame,
    required TResult Function() imageAccessError,
    required TResult Function() imageDecodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartGame value) startGame,
    required TResult Function(ImageAccessError value) imageAccessError,
    required TResult Function(ImageDecodeError value) imageDecodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameCreationEventCopyWith<$Res> {
  factory $GameCreationEventCopyWith(
          GameCreationEvent value, $Res Function(GameCreationEvent) then) =
      _$GameCreationEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GameCreationEventCopyWithImpl<$Res>
    implements $GameCreationEventCopyWith<$Res> {
  _$GameCreationEventCopyWithImpl(this._value, this._then);

  final GameCreationEvent _value;
  // ignore: unused_field
  final $Res Function(GameCreationEvent) _then;
}

/// @nodoc
abstract class $StartGameCopyWith<$Res> {
  factory $StartGameCopyWith(StartGame value, $Res Function(StartGame) then) =
      _$StartGameCopyWithImpl<$Res>;
}

/// @nodoc
class _$StartGameCopyWithImpl<$Res>
    extends _$GameCreationEventCopyWithImpl<$Res>
    implements $StartGameCopyWith<$Res> {
  _$StartGameCopyWithImpl(StartGame _value, $Res Function(StartGame) _then)
      : super(_value, (v) => _then(v as StartGame));

  @override
  StartGame get _value => super._value as StartGame;
}

/// @nodoc

class _$StartGame with DiagnosticableTreeMixin implements StartGame {
  const _$StartGame();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GameCreationEvent.startGame()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GameCreationEvent.startGame'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is StartGame);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startGame,
    required TResult Function() imageAccessError,
    required TResult Function() imageDecodeError,
  }) {
    return startGame();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
  }) {
    return startGame?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
    required TResult orElse(),
  }) {
    if (startGame != null) {
      return startGame();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartGame value) startGame,
    required TResult Function(ImageAccessError value) imageAccessError,
    required TResult Function(ImageDecodeError value) imageDecodeError,
  }) {
    return startGame(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
  }) {
    return startGame?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
    required TResult orElse(),
  }) {
    if (startGame != null) {
      return startGame(this);
    }
    return orElse();
  }
}

abstract class StartGame implements GameCreationEvent {
  const factory StartGame() = _$StartGame;
}

/// @nodoc
abstract class $ImageAccessErrorCopyWith<$Res> {
  factory $ImageAccessErrorCopyWith(
          ImageAccessError value, $Res Function(ImageAccessError) then) =
      _$ImageAccessErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$ImageAccessErrorCopyWithImpl<$Res>
    extends _$GameCreationEventCopyWithImpl<$Res>
    implements $ImageAccessErrorCopyWith<$Res> {
  _$ImageAccessErrorCopyWithImpl(
      ImageAccessError _value, $Res Function(ImageAccessError) _then)
      : super(_value, (v) => _then(v as ImageAccessError));

  @override
  ImageAccessError get _value => super._value as ImageAccessError;
}

/// @nodoc

class _$ImageAccessError
    with DiagnosticableTreeMixin
    implements ImageAccessError {
  const _$ImageAccessError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GameCreationEvent.imageAccessError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GameCreationEvent.imageAccessError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ImageAccessError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startGame,
    required TResult Function() imageAccessError,
    required TResult Function() imageDecodeError,
  }) {
    return imageAccessError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
  }) {
    return imageAccessError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
    required TResult orElse(),
  }) {
    if (imageAccessError != null) {
      return imageAccessError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartGame value) startGame,
    required TResult Function(ImageAccessError value) imageAccessError,
    required TResult Function(ImageDecodeError value) imageDecodeError,
  }) {
    return imageAccessError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
  }) {
    return imageAccessError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
    required TResult orElse(),
  }) {
    if (imageAccessError != null) {
      return imageAccessError(this);
    }
    return orElse();
  }
}

abstract class ImageAccessError implements GameCreationEvent {
  const factory ImageAccessError() = _$ImageAccessError;
}

/// @nodoc
abstract class $ImageDecodeErrorCopyWith<$Res> {
  factory $ImageDecodeErrorCopyWith(
          ImageDecodeError value, $Res Function(ImageDecodeError) then) =
      _$ImageDecodeErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$ImageDecodeErrorCopyWithImpl<$Res>
    extends _$GameCreationEventCopyWithImpl<$Res>
    implements $ImageDecodeErrorCopyWith<$Res> {
  _$ImageDecodeErrorCopyWithImpl(
      ImageDecodeError _value, $Res Function(ImageDecodeError) _then)
      : super(_value, (v) => _then(v as ImageDecodeError));

  @override
  ImageDecodeError get _value => super._value as ImageDecodeError;
}

/// @nodoc

class _$ImageDecodeError
    with DiagnosticableTreeMixin
    implements ImageDecodeError {
  const _$ImageDecodeError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GameCreationEvent.imageDecodeError()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GameCreationEvent.imageDecodeError'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ImageDecodeError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() startGame,
    required TResult Function() imageAccessError,
    required TResult Function() imageDecodeError,
  }) {
    return imageDecodeError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
  }) {
    return imageDecodeError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? startGame,
    TResult Function()? imageAccessError,
    TResult Function()? imageDecodeError,
    required TResult orElse(),
  }) {
    if (imageDecodeError != null) {
      return imageDecodeError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StartGame value) startGame,
    required TResult Function(ImageAccessError value) imageAccessError,
    required TResult Function(ImageDecodeError value) imageDecodeError,
  }) {
    return imageDecodeError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
  }) {
    return imageDecodeError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StartGame value)? startGame,
    TResult Function(ImageAccessError value)? imageAccessError,
    TResult Function(ImageDecodeError value)? imageDecodeError,
    required TResult orElse(),
  }) {
    if (imageDecodeError != null) {
      return imageDecodeError(this);
    }
    return orElse();
  }
}

abstract class ImageDecodeError implements GameCreationEvent {
  const factory ImageDecodeError() = _$ImageDecodeError;
}
