import 'package:flutter/material.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/widgets/layout/custom_sliver_bar.dart';

class GameAppBar extends StatelessWidget {
  const GameAppBar(
      {Key? key,
      this.child,
      this.flexibleChild,
      this.expanded = false,
      this.alignment,
      this.flexibleAlignment,
      this.collapsedHeight,
      this.expandedHeight})
      : super(key: key);

  final Widget? child;
  final Alignment? alignment;
  final Widget? flexibleChild;
  final Alignment? flexibleAlignment;
  final double? collapsedHeight;
  final double? expandedHeight;
  final bool expanded;

  @override
  Widget build(BuildContext context) {
    return CustomSliverBar(
      color: headerColor,
      collapsedHeight: collapsedHeight,
      expandedHeight: expandedHeight,
      borderRadius: const BorderRadius.vertical(bottom: $Radius.xxl),
      boxShadow: [
        BoxShadow(
          color: darkShadowColor,
          spreadRadius: $ShadowSize.s,
          blurRadius: 0,
          offset: const Offset(0, $ShadowSize.m), // changes position of shadow
        ),
      ],
      child: child,
      alignment: alignment,
      flexibleChild: flexibleChild,
      flexibleAlignment: flexibleAlignment,
    );
  }
}
