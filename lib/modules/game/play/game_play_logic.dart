import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/services/image_manager.dart';

import 'game_play_view.dart';

part 'game_play_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GamePlayEvent with _$GamePlayEvent {
  const factory GamePlayEvent.error() = GamePlayEventError;
}

class GamePlayLogic extends StateNotifier<GamePlayViewState> {
  static StreamController<GamePlayEvent>? eventStreamController;

  static final eventProvider = StreamProvider<GamePlayEvent>((ref) {
    eventStreamController = StreamController<GamePlayEvent>();
    ref.onDispose(eventStreamController!.close);
    return eventStreamController!.stream;
  });

  static final allImageBytesProvider = FutureProvider.family<List<Uint8List>, GameMode>((ref, gameMode) {
    final settings = ref.watch(GameCreationLogic.provider(gameMode));
    // Compute allow us to run an heavy task in a separated thread in order to not freeze UI
    return compute(ImageManager.imageAsGrid, [axisCountByLevel[settings.level], settings.imageBytes]);
  });

  static final provider = StateNotifierProvider.family<GamePlayLogic, GamePlayViewState, GameMode>((ref, gameMode) {
    final creationState = ref.watch(GameCreationLogic.provider(gameMode));
    return GamePlayLogic(GamePlayViewState(
      counter: 0,
      duration: Duration.zero,
      level: creationState.level,
      gameMode: gameMode,
      cheaterMode: creationState.cheaterMode,
      imageBytes: creationState.imageBytes,
    ));
  });

  static Map<GameLevel, int> axisCountByLevel = {
    GameLevel.easy: 3,
    GameLevel.normal: 4,
    GameLevel.hard: 5,
  };

  GamePlayLogic(GamePlayViewState initialState) : super(initialState);

  int get counter => state.counter;
  set counter(int value) => state = state.copyWith(counter: value);

  int get count => axisCountByLevel[state.level]!;

  void storeAllImageBytes(List<Uint8List> allImageBytes) => state = state.copyWith(images: allImageBytes);

  void storeDuration(Duration duration) => state = state.copyWith(duration: duration);

  void isWon() => state = state.copyWith(isWon: true);
}
