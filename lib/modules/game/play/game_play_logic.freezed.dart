// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_play_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GamePlayEventTearOff {
  const _$GamePlayEventTearOff();

  GamePlayEventError error() {
    return const GamePlayEventError();
  }
}

/// @nodoc
const $GamePlayEvent = _$GamePlayEventTearOff();

/// @nodoc
mixin _$GamePlayEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePlayEventError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePlayEventError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePlayEventError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GamePlayEventCopyWith<$Res> {
  factory $GamePlayEventCopyWith(
          GamePlayEvent value, $Res Function(GamePlayEvent) then) =
      _$GamePlayEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePlayEventCopyWithImpl<$Res>
    implements $GamePlayEventCopyWith<$Res> {
  _$GamePlayEventCopyWithImpl(this._value, this._then);

  final GamePlayEvent _value;
  // ignore: unused_field
  final $Res Function(GamePlayEvent) _then;
}

/// @nodoc
abstract class $GamePlayEventErrorCopyWith<$Res> {
  factory $GamePlayEventErrorCopyWith(
          GamePlayEventError value, $Res Function(GamePlayEventError) then) =
      _$GamePlayEventErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePlayEventErrorCopyWithImpl<$Res>
    extends _$GamePlayEventCopyWithImpl<$Res>
    implements $GamePlayEventErrorCopyWith<$Res> {
  _$GamePlayEventErrorCopyWithImpl(
      GamePlayEventError _value, $Res Function(GamePlayEventError) _then)
      : super(_value, (v) => _then(v as GamePlayEventError));

  @override
  GamePlayEventError get _value => super._value as GamePlayEventError;
}

/// @nodoc

class _$GamePlayEventError
    with DiagnosticableTreeMixin
    implements GamePlayEventError {
  const _$GamePlayEventError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePlayEvent.error()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GamePlayEvent.error'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GamePlayEventError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePlayEventError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePlayEventError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePlayEventError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class GamePlayEventError implements GamePlayEvent {
  const factory GamePlayEventError() = _$GamePlayEventError;
}
