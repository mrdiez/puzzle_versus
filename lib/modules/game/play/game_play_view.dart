import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/environment.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/play/game_play_logic.dart';
import 'package:puzzle_versus/modules/game/play/puzzle/game_puzzle_logic.dart';
import 'package:puzzle_versus/modules/game/play/puzzle/game_puzzle_view.dart';
import 'package:puzzle_versus/modules/game/widgets/game_app_bar.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/images/logo.dart';
import 'package:puzzle_versus/widgets/layout/game_container.dart';
import 'package:puzzle_versus/widgets/layout/sliver.view.dart';
import 'package:puzzle_versus/widgets/plugins/slide_countdown_fork.dart';

part 'game_play_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GamePlayViewState with _$GamePlayViewState {
  const factory GamePlayViewState({
    required int counter,
    required GameMode gameMode,
    required Duration duration,
    Uint8List? imageBytes,
    List<Uint8List>? images,
    @Default(GameLevel.easy) GameLevel level,
    @Default(false) bool cheaterMode,
    @Default(false) bool isWon,
  }) = _GamePlayViewState;

  const GamePlayViewState._();
}

class GamePlayView extends ConsumerWidget {
  const GamePlayView(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  void onEvent(BuildContext context, GamePlayLogic logic, GamePuzzleEvent event) {
    if (event is GamePuzzleEventMove || event is GamePuzzleEventDrag) {
      logic.counter++;
    } else if (event is GamePuzzleEventSuccess) {
      logic.isWon();
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    final logic = ref.read(GamePlayLogic.provider(gameMode).notifier);
    final isWon = ref.watch(GamePlayLogic.provider(gameMode).select((s) => s.isWon));
    if (isWon) WidgetsBinding.instance?.addPostFrameCallback((_) => context.go(winRoutes[gameMode]!));
    ref.listen<AsyncValue<GamePuzzleEvent>>(
        GamePuzzleLogic.eventProvider, (previous, next) => onEvent(context, logic, next.value!));
    return Scaffold(
        body: Stack(
      children: [
        SliverView(
          fillRemaining: true,
          physics: const NeverScrollableScrollPhysics(),
          appBar: GameAppBar(
            expandedHeight: $AppBarHeight.l * 2,
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(
                left: $Padding.m,
                top: Environment.isMobile ? $Padding.l : $Padding.m,
                right: $Padding.m,
                bottom: $Padding.m,
              ),
              child: Logo(
                onTap: () {
                  if (context.popRoute != null) {
                    context.popRoute!.call();
                  } else {
                    context.go(gameRoutes[gameMode]!);
                  }
                },
                height: $LogoHeight.xxs,
              ),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: LayoutBuilder(builder: (context, constraints) {
              double gameSize = min(constraints.maxWidth, constraints.maxHeight);
              gameSize *= $Screen.size(gameSize) > xs ? 3 / 4 : 1;
              return SizedBox(
                height: constraints.maxHeight - $AppBarHeight.l,
                child: GameContainer(
                  gameMode: gameMode,
                  child: Padding(
                    padding: const EdgeInsets.all($Padding.m),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Consumer(builder: (context, ref, child) {
                            final counter = ref.watch(GamePlayLogic.provider(gameMode).select((s) => s.counter));
                            return Text('$counter ${context.translation.moves(counter)}',
                                style: context.textTheme.subtitle1!.copyWith(color: blackColor));
                          }),
                          Flexible(
                            child: ConstrainedBox(
                              constraints: BoxConstraints.loose(Size(gameSize, gameSize)),
                              child: Consumer(builder: (context, ref, child) {
                                final state = ref.read(GamePlayLogic.provider(gameMode));
                                if (state.imageBytes != null) {
                                  final allImageBytesFuture = ref.watch(GamePlayLogic.allImageBytesProvider(gameMode));
                                  return allImageBytesFuture.when(
                                    data: (allImageBytes) {
                                      return GamePuzzleView(gameMode: gameMode, allImageBytes: allImageBytes);
                                    },
                                    error: (error, stackTrace) {
                                      context.showErrorSnackBar(context.translation.unknownErrorWhileSplittingImage);
                                      context.go(homeRoute);
                                      return Container();
                                    },
                                    loading: () => const CircularProgressIndicator(),
                                  );
                                }
                                return GamePuzzleView(gameMode: gameMode);
                              }),
                            ),
                          ),
                          Consumer(builder: (context, ref, child) {
                            final initialDuration =
                                ref.watch(GamePlayLogic.provider(gameMode).select((s) => s.duration));
                            return SlideCountdownFork(
                              initialDuration: initialDuration,
                              duration: const Duration(days: 2),
                              countUp: true,
                              fixed: isWon,
                              decoration: const BoxDecoration(
                                color: transparentColor,
                              ),
                              onDurationChanged: logic.storeDuration,
                              textStyle: context.textTheme.subtitle1!,
                            );
                          })
                        ],
                      ),
                    ),
                  ),
                ),
              );
            })),
      ],
    ));
  }
}
