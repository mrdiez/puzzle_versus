import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/play/puzzle/game_puzzle_logic.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/layout/simple_container.dart';
import 'package:puzzle_versus/widgets/plugins/reorderable_builder_fork.dart';

part 'game_puzzle_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GamePuzzleViewState with _$GamePuzzleViewState {
  const factory GamePuzzleViewState({
    int? activeChild,
    required int axisCount,
    required List<Uint8List>? images,
    required List<int> originalChildren,
    required List<int?> currentChildren,
    required List<int> undraggableChildren,
  }) = _GamePuzzleViewState;

  const GamePuzzleViewState._();
}

class GamePuzzleView extends ConsumerWidget {
  const GamePuzzleView({Key? key, required this.gameMode, this.allImageBytes}) : super(key: key);

  final GameMode gameMode;
  final List<Uint8List>? allImageBytes;

  @override
  Widget build(BuildContext context, ref) {
    final logic = ref.read(GamePuzzleLogic.provider(gameMode).notifier);
    return LayoutBuilder(builder: (context, constraints) {
      return Consumer(
        builder: (context, ref, child) {
          final state = ref.watch(GamePuzzleLogic.provider(gameMode));
          final children = List.generate(state.currentChildren.length, (i) {
            final isNotNull = logic.isNotNull(i);
            final isImage = (allImageBytes?.isNotEmpty ?? false) && allImageBytes!.length > i && isNotNull;
            final child = SimpleContainer(
              shadow: false,
              color: isNotNull ? context.theme.colorScheme.primary : null,
              child: isImage
                  ? ClipRRect(
                      borderRadius: const BorderRadius.all($Radius.m),
                      child: Image.memory(allImageBytes![state.currentChildren[i]!], fit: BoxFit.cover))
                  : isNotNull
                      ? Center(
                          child: Text('${state.currentChildren[i]}',
                              style: context.textTheme.subtitle2!.copyWith(color: whiteColor)),
                        )
                      : null,
            );
            return Padding(
              key: Key(state.currentChildren[i].toString()),
              padding: const EdgeInsets.only(right: $Padding.s, bottom: $Padding.s),
              child: logic.isClickable(i)
                  ? InkResponse(
                      onTap: () => logic.onClick(i),
                      child: child,
                    )
                  : child,
            );
          });
          return Padding(
            padding: const EdgeInsets.only(top: $Padding.s, left: $Padding.s),
            child: ReorderableBuilderFork(
              children: children,
              onReorder: (previous, next) {}, //=> logic.handleReorder(previous, next, true),
              onCollision: logic.handleReorderOnDrag,
              lockedIndices: state.undraggableChildren,
              enableDraggable: state.undraggableChildren.length < state.originalChildren.length,
              enableLongPress: true,
              longPressDelay: const Duration(milliseconds: 50),
              enableAnimation: true,
              dragChildBoxDecoration: BoxDecoration(
                borderRadius: const BorderRadius.all($Radius.m),
                boxShadow: [
                  BoxShadow(
                    color: darkShadowColor,
                    spreadRadius: -$ShadowSize.m,
                    blurRadius: 0,
                    offset: const Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
              builder: (children, scrollController) {
                return GridView.extent(
                  padding: EdgeInsets.zero,
                  controller: scrollController,
                  children: children,
                  physics: const NeverScrollableScrollPhysics(),
                  maxCrossAxisExtent: constraints.maxWidth / state.axisCount,
                  childAspectRatio: constraints.maxWidth / constraints.maxHeight,
                );
              },
            ),
          );
        },
      );
    });
  }
}
