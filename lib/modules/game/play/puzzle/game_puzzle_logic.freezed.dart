// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_puzzle_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GamePuzzleEventTearOff {
  const _$GamePuzzleEventTearOff();

  GamePuzzleEventSuccess success() {
    return const GamePuzzleEventSuccess();
  }

  GamePuzzleEventMove move() {
    return const GamePuzzleEventMove();
  }

  GamePuzzleEventDrag drag() {
    return const GamePuzzleEventDrag();
  }

  GamePuzzleEventError error() {
    return const GamePuzzleEventError();
  }
}

/// @nodoc
const $GamePuzzleEvent = _$GamePuzzleEventTearOff();

/// @nodoc
mixin _$GamePuzzleEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() move,
    required TResult Function() drag,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePuzzleEventSuccess value) success,
    required TResult Function(GamePuzzleEventMove value) move,
    required TResult Function(GamePuzzleEventDrag value) drag,
    required TResult Function(GamePuzzleEventError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GamePuzzleEventCopyWith<$Res> {
  factory $GamePuzzleEventCopyWith(
          GamePuzzleEvent value, $Res Function(GamePuzzleEvent) then) =
      _$GamePuzzleEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePuzzleEventCopyWithImpl<$Res>
    implements $GamePuzzleEventCopyWith<$Res> {
  _$GamePuzzleEventCopyWithImpl(this._value, this._then);

  final GamePuzzleEvent _value;
  // ignore: unused_field
  final $Res Function(GamePuzzleEvent) _then;
}

/// @nodoc
abstract class $GamePuzzleEventSuccessCopyWith<$Res> {
  factory $GamePuzzleEventSuccessCopyWith(GamePuzzleEventSuccess value,
          $Res Function(GamePuzzleEventSuccess) then) =
      _$GamePuzzleEventSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePuzzleEventSuccessCopyWithImpl<$Res>
    extends _$GamePuzzleEventCopyWithImpl<$Res>
    implements $GamePuzzleEventSuccessCopyWith<$Res> {
  _$GamePuzzleEventSuccessCopyWithImpl(GamePuzzleEventSuccess _value,
      $Res Function(GamePuzzleEventSuccess) _then)
      : super(_value, (v) => _then(v as GamePuzzleEventSuccess));

  @override
  GamePuzzleEventSuccess get _value => super._value as GamePuzzleEventSuccess;
}

/// @nodoc

class _$GamePuzzleEventSuccess
    with DiagnosticableTreeMixin
    implements GamePuzzleEventSuccess {
  const _$GamePuzzleEventSuccess();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePuzzleEvent.success()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GamePuzzleEvent.success'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GamePuzzleEventSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() move,
    required TResult Function() drag,
    required TResult Function() error,
  }) {
    return success();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
  }) {
    return success?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePuzzleEventSuccess value) success,
    required TResult Function(GamePuzzleEventMove value) move,
    required TResult Function(GamePuzzleEventDrag value) drag,
    required TResult Function(GamePuzzleEventError value) error,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class GamePuzzleEventSuccess implements GamePuzzleEvent {
  const factory GamePuzzleEventSuccess() = _$GamePuzzleEventSuccess;
}

/// @nodoc
abstract class $GamePuzzleEventMoveCopyWith<$Res> {
  factory $GamePuzzleEventMoveCopyWith(
          GamePuzzleEventMove value, $Res Function(GamePuzzleEventMove) then) =
      _$GamePuzzleEventMoveCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePuzzleEventMoveCopyWithImpl<$Res>
    extends _$GamePuzzleEventCopyWithImpl<$Res>
    implements $GamePuzzleEventMoveCopyWith<$Res> {
  _$GamePuzzleEventMoveCopyWithImpl(
      GamePuzzleEventMove _value, $Res Function(GamePuzzleEventMove) _then)
      : super(_value, (v) => _then(v as GamePuzzleEventMove));

  @override
  GamePuzzleEventMove get _value => super._value as GamePuzzleEventMove;
}

/// @nodoc

class _$GamePuzzleEventMove
    with DiagnosticableTreeMixin
    implements GamePuzzleEventMove {
  const _$GamePuzzleEventMove();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePuzzleEvent.move()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GamePuzzleEvent.move'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GamePuzzleEventMove);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() move,
    required TResult Function() drag,
    required TResult Function() error,
  }) {
    return move();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
  }) {
    return move?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (move != null) {
      return move();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePuzzleEventSuccess value) success,
    required TResult Function(GamePuzzleEventMove value) move,
    required TResult Function(GamePuzzleEventDrag value) drag,
    required TResult Function(GamePuzzleEventError value) error,
  }) {
    return move(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
  }) {
    return move?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
    required TResult orElse(),
  }) {
    if (move != null) {
      return move(this);
    }
    return orElse();
  }
}

abstract class GamePuzzleEventMove implements GamePuzzleEvent {
  const factory GamePuzzleEventMove() = _$GamePuzzleEventMove;
}

/// @nodoc
abstract class $GamePuzzleEventDragCopyWith<$Res> {
  factory $GamePuzzleEventDragCopyWith(
          GamePuzzleEventDrag value, $Res Function(GamePuzzleEventDrag) then) =
      _$GamePuzzleEventDragCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePuzzleEventDragCopyWithImpl<$Res>
    extends _$GamePuzzleEventCopyWithImpl<$Res>
    implements $GamePuzzleEventDragCopyWith<$Res> {
  _$GamePuzzleEventDragCopyWithImpl(
      GamePuzzleEventDrag _value, $Res Function(GamePuzzleEventDrag) _then)
      : super(_value, (v) => _then(v as GamePuzzleEventDrag));

  @override
  GamePuzzleEventDrag get _value => super._value as GamePuzzleEventDrag;
}

/// @nodoc

class _$GamePuzzleEventDrag
    with DiagnosticableTreeMixin
    implements GamePuzzleEventDrag {
  const _$GamePuzzleEventDrag();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePuzzleEvent.drag()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GamePuzzleEvent.drag'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GamePuzzleEventDrag);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() move,
    required TResult Function() drag,
    required TResult Function() error,
  }) {
    return drag();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
  }) {
    return drag?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (drag != null) {
      return drag();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePuzzleEventSuccess value) success,
    required TResult Function(GamePuzzleEventMove value) move,
    required TResult Function(GamePuzzleEventDrag value) drag,
    required TResult Function(GamePuzzleEventError value) error,
  }) {
    return drag(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
  }) {
    return drag?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
    required TResult orElse(),
  }) {
    if (drag != null) {
      return drag(this);
    }
    return orElse();
  }
}

abstract class GamePuzzleEventDrag implements GamePuzzleEvent {
  const factory GamePuzzleEventDrag() = _$GamePuzzleEventDrag;
}

/// @nodoc
abstract class $GamePuzzleEventErrorCopyWith<$Res> {
  factory $GamePuzzleEventErrorCopyWith(GamePuzzleEventError value,
          $Res Function(GamePuzzleEventError) then) =
      _$GamePuzzleEventErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$GamePuzzleEventErrorCopyWithImpl<$Res>
    extends _$GamePuzzleEventCopyWithImpl<$Res>
    implements $GamePuzzleEventErrorCopyWith<$Res> {
  _$GamePuzzleEventErrorCopyWithImpl(
      GamePuzzleEventError _value, $Res Function(GamePuzzleEventError) _then)
      : super(_value, (v) => _then(v as GamePuzzleEventError));

  @override
  GamePuzzleEventError get _value => super._value as GamePuzzleEventError;
}

/// @nodoc

class _$GamePuzzleEventError
    with DiagnosticableTreeMixin
    implements GamePuzzleEventError {
  const _$GamePuzzleEventError();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePuzzleEvent.error()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties..add(DiagnosticsProperty('type', 'GamePuzzleEvent.error'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is GamePuzzleEventError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() success,
    required TResult Function() move,
    required TResult Function() drag,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? success,
    TResult Function()? move,
    TResult Function()? drag,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GamePuzzleEventSuccess value) success,
    required TResult Function(GamePuzzleEventMove value) move,
    required TResult Function(GamePuzzleEventDrag value) drag,
    required TResult Function(GamePuzzleEventError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GamePuzzleEventSuccess value)? success,
    TResult Function(GamePuzzleEventMove value)? move,
    TResult Function(GamePuzzleEventDrag value)? drag,
    TResult Function(GamePuzzleEventError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class GamePuzzleEventError implements GamePuzzleEvent {
  const factory GamePuzzleEventError() = _$GamePuzzleEventError;
}
