// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_puzzle_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GamePuzzleViewStateTearOff {
  const _$GamePuzzleViewStateTearOff();

  _GamePuzzleViewState call(
      {int? activeChild,
      required int axisCount,
      required List<Uint8List>? images,
      required List<int> originalChildren,
      required List<int?> currentChildren,
      required List<int> undraggableChildren}) {
    return _GamePuzzleViewState(
      activeChild: activeChild,
      axisCount: axisCount,
      images: images,
      originalChildren: originalChildren,
      currentChildren: currentChildren,
      undraggableChildren: undraggableChildren,
    );
  }
}

/// @nodoc
const $GamePuzzleViewState = _$GamePuzzleViewStateTearOff();

/// @nodoc
mixin _$GamePuzzleViewState {
  int? get activeChild => throw _privateConstructorUsedError;
  int get axisCount => throw _privateConstructorUsedError;
  List<Uint8List>? get images => throw _privateConstructorUsedError;
  List<int> get originalChildren => throw _privateConstructorUsedError;
  List<int?> get currentChildren => throw _privateConstructorUsedError;
  List<int> get undraggableChildren => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GamePuzzleViewStateCopyWith<GamePuzzleViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GamePuzzleViewStateCopyWith<$Res> {
  factory $GamePuzzleViewStateCopyWith(
          GamePuzzleViewState value, $Res Function(GamePuzzleViewState) then) =
      _$GamePuzzleViewStateCopyWithImpl<$Res>;
  $Res call(
      {int? activeChild,
      int axisCount,
      List<Uint8List>? images,
      List<int> originalChildren,
      List<int?> currentChildren,
      List<int> undraggableChildren});
}

/// @nodoc
class _$GamePuzzleViewStateCopyWithImpl<$Res>
    implements $GamePuzzleViewStateCopyWith<$Res> {
  _$GamePuzzleViewStateCopyWithImpl(this._value, this._then);

  final GamePuzzleViewState _value;
  // ignore: unused_field
  final $Res Function(GamePuzzleViewState) _then;

  @override
  $Res call({
    Object? activeChild = freezed,
    Object? axisCount = freezed,
    Object? images = freezed,
    Object? originalChildren = freezed,
    Object? currentChildren = freezed,
    Object? undraggableChildren = freezed,
  }) {
    return _then(_value.copyWith(
      activeChild: activeChild == freezed
          ? _value.activeChild
          : activeChild // ignore: cast_nullable_to_non_nullable
              as int?,
      axisCount: axisCount == freezed
          ? _value.axisCount
          : axisCount // ignore: cast_nullable_to_non_nullable
              as int,
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<Uint8List>?,
      originalChildren: originalChildren == freezed
          ? _value.originalChildren
          : originalChildren // ignore: cast_nullable_to_non_nullable
              as List<int>,
      currentChildren: currentChildren == freezed
          ? _value.currentChildren
          : currentChildren // ignore: cast_nullable_to_non_nullable
              as List<int?>,
      undraggableChildren: undraggableChildren == freezed
          ? _value.undraggableChildren
          : undraggableChildren // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ));
  }
}

/// @nodoc
abstract class _$GamePuzzleViewStateCopyWith<$Res>
    implements $GamePuzzleViewStateCopyWith<$Res> {
  factory _$GamePuzzleViewStateCopyWith(_GamePuzzleViewState value,
          $Res Function(_GamePuzzleViewState) then) =
      __$GamePuzzleViewStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? activeChild,
      int axisCount,
      List<Uint8List>? images,
      List<int> originalChildren,
      List<int?> currentChildren,
      List<int> undraggableChildren});
}

/// @nodoc
class __$GamePuzzleViewStateCopyWithImpl<$Res>
    extends _$GamePuzzleViewStateCopyWithImpl<$Res>
    implements _$GamePuzzleViewStateCopyWith<$Res> {
  __$GamePuzzleViewStateCopyWithImpl(
      _GamePuzzleViewState _value, $Res Function(_GamePuzzleViewState) _then)
      : super(_value, (v) => _then(v as _GamePuzzleViewState));

  @override
  _GamePuzzleViewState get _value => super._value as _GamePuzzleViewState;

  @override
  $Res call({
    Object? activeChild = freezed,
    Object? axisCount = freezed,
    Object? images = freezed,
    Object? originalChildren = freezed,
    Object? currentChildren = freezed,
    Object? undraggableChildren = freezed,
  }) {
    return _then(_GamePuzzleViewState(
      activeChild: activeChild == freezed
          ? _value.activeChild
          : activeChild // ignore: cast_nullable_to_non_nullable
              as int?,
      axisCount: axisCount == freezed
          ? _value.axisCount
          : axisCount // ignore: cast_nullable_to_non_nullable
              as int,
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<Uint8List>?,
      originalChildren: originalChildren == freezed
          ? _value.originalChildren
          : originalChildren // ignore: cast_nullable_to_non_nullable
              as List<int>,
      currentChildren: currentChildren == freezed
          ? _value.currentChildren
          : currentChildren // ignore: cast_nullable_to_non_nullable
              as List<int?>,
      undraggableChildren: undraggableChildren == freezed
          ? _value.undraggableChildren
          : undraggableChildren // ignore: cast_nullable_to_non_nullable
              as List<int>,
    ));
  }
}

/// @nodoc

class _$_GamePuzzleViewState extends _GamePuzzleViewState
    with DiagnosticableTreeMixin {
  const _$_GamePuzzleViewState(
      {this.activeChild,
      required this.axisCount,
      required this.images,
      required this.originalChildren,
      required this.currentChildren,
      required this.undraggableChildren})
      : super._();

  @override
  final int? activeChild;
  @override
  final int axisCount;
  @override
  final List<Uint8List>? images;
  @override
  final List<int> originalChildren;
  @override
  final List<int?> currentChildren;
  @override
  final List<int> undraggableChildren;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GamePuzzleViewState(activeChild: $activeChild, axisCount: $axisCount, images: $images, originalChildren: $originalChildren, currentChildren: $currentChildren, undraggableChildren: $undraggableChildren)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GamePuzzleViewState'))
      ..add(DiagnosticsProperty('activeChild', activeChild))
      ..add(DiagnosticsProperty('axisCount', axisCount))
      ..add(DiagnosticsProperty('images', images))
      ..add(DiagnosticsProperty('originalChildren', originalChildren))
      ..add(DiagnosticsProperty('currentChildren', currentChildren))
      ..add(DiagnosticsProperty('undraggableChildren', undraggableChildren));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GamePuzzleViewState &&
            const DeepCollectionEquality()
                .equals(other.activeChild, activeChild) &&
            const DeepCollectionEquality().equals(other.axisCount, axisCount) &&
            const DeepCollectionEquality().equals(other.images, images) &&
            const DeepCollectionEquality()
                .equals(other.originalChildren, originalChildren) &&
            const DeepCollectionEquality()
                .equals(other.currentChildren, currentChildren) &&
            const DeepCollectionEquality()
                .equals(other.undraggableChildren, undraggableChildren));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(activeChild),
      const DeepCollectionEquality().hash(axisCount),
      const DeepCollectionEquality().hash(images),
      const DeepCollectionEquality().hash(originalChildren),
      const DeepCollectionEquality().hash(currentChildren),
      const DeepCollectionEquality().hash(undraggableChildren));

  @JsonKey(ignore: true)
  @override
  _$GamePuzzleViewStateCopyWith<_GamePuzzleViewState> get copyWith =>
      __$GamePuzzleViewStateCopyWithImpl<_GamePuzzleViewState>(
          this, _$identity);
}

abstract class _GamePuzzleViewState extends GamePuzzleViewState {
  const factory _GamePuzzleViewState(
      {int? activeChild,
      required int axisCount,
      required List<Uint8List>? images,
      required List<int> originalChildren,
      required List<int?> currentChildren,
      required List<int> undraggableChildren}) = _$_GamePuzzleViewState;
  const _GamePuzzleViewState._() : super._();

  @override
  int? get activeChild;
  @override
  int get axisCount;
  @override
  List<Uint8List>? get images;
  @override
  List<int> get originalChildren;
  @override
  List<int?> get currentChildren;
  @override
  List<int> get undraggableChildren;
  @override
  @JsonKey(ignore: true)
  _$GamePuzzleViewStateCopyWith<_GamePuzzleViewState> get copyWith =>
      throw _privateConstructorUsedError;
}
