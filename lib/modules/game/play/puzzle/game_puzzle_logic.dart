import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/modules/game/creation/game_creation_logic.dart';
import 'package:puzzle_versus/modules/game/play/game_play_logic.dart';
import 'package:puzzle_versus/modules/game/play/game_play_view.dart';
import 'package:puzzle_versus/modules/game/play/puzzle/game_puzzle_view.dart';

part 'game_puzzle_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GamePuzzleEvent with _$GamePuzzleEvent {
  const factory GamePuzzleEvent.success() = GamePuzzleEventSuccess;
  const factory GamePuzzleEvent.move() = GamePuzzleEventMove;
  const factory GamePuzzleEvent.drag() = GamePuzzleEventDrag;
  const factory GamePuzzleEvent.error() = GamePuzzleEventError;
}

class GamePuzzleLogic extends StateNotifier<GamePuzzleViewState> {
  static StreamController<GamePuzzleEvent>? eventStreamController;

  static final eventProvider = StreamProvider<GamePuzzleEvent>((ref) {
    eventStreamController = StreamController<GamePuzzleEvent>();
    ref.onDispose(eventStreamController!.close);
    return eventStreamController!.stream;
  });

  static final provider = StateNotifierProvider.family<GamePuzzleLogic, GamePuzzleViewState, GameMode>((ref, gameMode) {
    ref.watch(GameCreationLogic.provider(gameMode));
    final playState = ref.read(GamePlayLogic.provider(gameMode));
    return GamePuzzleLogic.init(playState);
  });

  static Map<GameLevel, int> axisCountByLevel = {
    GameLevel.easy: 3,
    GameLevel.normal: 4,
    GameLevel.hard: 5,
  };

  GamePuzzleLogic({required GamePuzzleViewState initialState}) : super(initialState);

  factory GamePuzzleLogic.init(GamePlayViewState settings) {
    final axisCount = axisCountByLevel[settings.level]!;
    final length = axisCount * axisCount;
    final originalChildren = List.generate(length, (i) => i, growable: false);
    final undraggableChildren = [...originalChildren];
    final currentChildren = <int?>[...originalChildren]
      ..last = null
      ..shuffle();
    final result = GamePuzzleLogic(
      initialState: GamePuzzleViewState(
        axisCount: axisCount,
        images: settings.images,
        originalChildren: originalChildren,
        currentChildren: currentChildren,
        undraggableChildren: settings.cheaterMode ? [] : undraggableChildren,
      ),
    );
    while (result.isWon(currentChildren)) {
      currentChildren.shuffle();
    }
    return result;
  }

  List<int> get _clickableChildren {
    final emptyChildIndex = _emptyChildIndex;
    final clickableChildren = [emptyChildIndex - state.axisCount, emptyChildIndex + state.axisCount];
    if (emptyChildIndex % state.axisCount > 0) clickableChildren.add(emptyChildIndex - 1);
    if ((emptyChildIndex + 1) % state.axisCount > 0) clickableChildren.add(emptyChildIndex + 1);
    return clickableChildren;
  }

  void handleReorderOnClick(int oldIndex, int nextIndex) {
    eventStreamController!.add(const GamePuzzleEvent.move());

    final clickedItem = state.currentChildren[oldIndex];
    final nullItem = state.currentChildren[nextIndex];
    state.currentChildren[nextIndex] = clickedItem;
    state.currentChildren[oldIndex] = nullItem;
    state = state.copyWith(currentChildren: state.currentChildren);

    if (isWon(state.currentChildren)) {
      eventStreamController!.add(const GamePuzzleEvent.success());
    }
  }

  void handleReorderOnDrag(List<int?> newIndexes) {
    eventStreamController!.add(const GamePuzzleEvent.drag());

    if (!listEquals(newIndexes, state.currentChildren)) {
      state = state.copyWith(currentChildren: newIndexes);
      if (isWon(newIndexes)) {
        eventStreamController!.add(const GamePuzzleEvent.success());
      }
    }
  }

  void onClick(int i) => handleReorderOnClick(i, _emptyChildIndex);

  bool isWon(List<int?> listToCheck) {
    final children = [...listToCheck]..last = state.originalChildren.last;
    return listEquals(state.originalChildren, children);
  }

  bool isNotNull(int i) => state.currentChildren[i] != null;
  bool isActive(int i) => isNotNull(i) && state.currentChildren[i] == state.activeChild;
  bool isDraggable(int i) => !state.undraggableChildren.contains(i);
  bool isClickable(int i) => !isDraggable(i) && _clickableChildren.contains(i);
  int get _emptyChildIndex => state.currentChildren.indexOf(null);
}
