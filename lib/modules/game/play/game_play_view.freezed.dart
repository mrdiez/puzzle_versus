// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_play_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GamePlayViewStateTearOff {
  const _$GamePlayViewStateTearOff();

  _GamePlayViewState call(
      {required int counter,
      required GameMode gameMode,
      required Duration duration,
      Uint8List? imageBytes,
      List<Uint8List>? images,
      GameLevel level = GameLevel.easy,
      bool cheaterMode = false,
      bool isWon = false}) {
    return _GamePlayViewState(
      counter: counter,
      gameMode: gameMode,
      duration: duration,
      imageBytes: imageBytes,
      images: images,
      level: level,
      cheaterMode: cheaterMode,
      isWon: isWon,
    );
  }
}

/// @nodoc
const $GamePlayViewState = _$GamePlayViewStateTearOff();

/// @nodoc
mixin _$GamePlayViewState {
  int get counter => throw _privateConstructorUsedError;
  GameMode get gameMode => throw _privateConstructorUsedError;
  Duration get duration => throw _privateConstructorUsedError;
  Uint8List? get imageBytes => throw _privateConstructorUsedError;
  List<Uint8List>? get images => throw _privateConstructorUsedError;
  GameLevel get level => throw _privateConstructorUsedError;
  bool get cheaterMode => throw _privateConstructorUsedError;
  bool get isWon => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GamePlayViewStateCopyWith<GamePlayViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GamePlayViewStateCopyWith<$Res> {
  factory $GamePlayViewStateCopyWith(
          GamePlayViewState value, $Res Function(GamePlayViewState) then) =
      _$GamePlayViewStateCopyWithImpl<$Res>;
  $Res call(
      {int counter,
      GameMode gameMode,
      Duration duration,
      Uint8List? imageBytes,
      List<Uint8List>? images,
      GameLevel level,
      bool cheaterMode,
      bool isWon});
}

/// @nodoc
class _$GamePlayViewStateCopyWithImpl<$Res>
    implements $GamePlayViewStateCopyWith<$Res> {
  _$GamePlayViewStateCopyWithImpl(this._value, this._then);

  final GamePlayViewState _value;
  // ignore: unused_field
  final $Res Function(GamePlayViewState) _then;

  @override
  $Res call({
    Object? counter = freezed,
    Object? gameMode = freezed,
    Object? duration = freezed,
    Object? imageBytes = freezed,
    Object? images = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
    Object? isWon = freezed,
  }) {
    return _then(_value.copyWith(
      counter: counter == freezed
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
      gameMode: gameMode == freezed
          ? _value.gameMode
          : gameMode // ignore: cast_nullable_to_non_nullable
              as GameMode,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      imageBytes: imageBytes == freezed
          ? _value.imageBytes
          : imageBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<Uint8List>?,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
      isWon: isWon == freezed
          ? _value.isWon
          : isWon // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$GamePlayViewStateCopyWith<$Res>
    implements $GamePlayViewStateCopyWith<$Res> {
  factory _$GamePlayViewStateCopyWith(
          _GamePlayViewState value, $Res Function(_GamePlayViewState) then) =
      __$GamePlayViewStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int counter,
      GameMode gameMode,
      Duration duration,
      Uint8List? imageBytes,
      List<Uint8List>? images,
      GameLevel level,
      bool cheaterMode,
      bool isWon});
}

/// @nodoc
class __$GamePlayViewStateCopyWithImpl<$Res>
    extends _$GamePlayViewStateCopyWithImpl<$Res>
    implements _$GamePlayViewStateCopyWith<$Res> {
  __$GamePlayViewStateCopyWithImpl(
      _GamePlayViewState _value, $Res Function(_GamePlayViewState) _then)
      : super(_value, (v) => _then(v as _GamePlayViewState));

  @override
  _GamePlayViewState get _value => super._value as _GamePlayViewState;

  @override
  $Res call({
    Object? counter = freezed,
    Object? gameMode = freezed,
    Object? duration = freezed,
    Object? imageBytes = freezed,
    Object? images = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
    Object? isWon = freezed,
  }) {
    return _then(_GamePlayViewState(
      counter: counter == freezed
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
      gameMode: gameMode == freezed
          ? _value.gameMode
          : gameMode // ignore: cast_nullable_to_non_nullable
              as GameMode,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      imageBytes: imageBytes == freezed
          ? _value.imageBytes
          : imageBytes // ignore: cast_nullable_to_non_nullable
              as Uint8List?,
      images: images == freezed
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as List<Uint8List>?,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
      isWon: isWon == freezed
          ? _value.isWon
          : isWon // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_GamePlayViewState extends _GamePlayViewState {
  const _$_GamePlayViewState(
      {required this.counter,
      required this.gameMode,
      required this.duration,
      this.imageBytes,
      this.images,
      this.level = GameLevel.easy,
      this.cheaterMode = false,
      this.isWon = false})
      : super._();

  @override
  final int counter;
  @override
  final GameMode gameMode;
  @override
  final Duration duration;
  @override
  final Uint8List? imageBytes;
  @override
  final List<Uint8List>? images;
  @JsonKey()
  @override
  final GameLevel level;
  @JsonKey()
  @override
  final bool cheaterMode;
  @JsonKey()
  @override
  final bool isWon;

  @override
  String toString() {
    return 'GamePlayViewState(counter: $counter, gameMode: $gameMode, duration: $duration, imageBytes: $imageBytes, images: $images, level: $level, cheaterMode: $cheaterMode, isWon: $isWon)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GamePlayViewState &&
            const DeepCollectionEquality().equals(other.counter, counter) &&
            const DeepCollectionEquality().equals(other.gameMode, gameMode) &&
            const DeepCollectionEquality().equals(other.duration, duration) &&
            const DeepCollectionEquality()
                .equals(other.imageBytes, imageBytes) &&
            const DeepCollectionEquality().equals(other.images, images) &&
            const DeepCollectionEquality().equals(other.level, level) &&
            const DeepCollectionEquality()
                .equals(other.cheaterMode, cheaterMode) &&
            const DeepCollectionEquality().equals(other.isWon, isWon));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(counter),
      const DeepCollectionEquality().hash(gameMode),
      const DeepCollectionEquality().hash(duration),
      const DeepCollectionEquality().hash(imageBytes),
      const DeepCollectionEquality().hash(images),
      const DeepCollectionEquality().hash(level),
      const DeepCollectionEquality().hash(cheaterMode),
      const DeepCollectionEquality().hash(isWon));

  @JsonKey(ignore: true)
  @override
  _$GamePlayViewStateCopyWith<_GamePlayViewState> get copyWith =>
      __$GamePlayViewStateCopyWithImpl<_GamePlayViewState>(this, _$identity);
}

abstract class _GamePlayViewState extends GamePlayViewState {
  const factory _GamePlayViewState(
      {required int counter,
      required GameMode gameMode,
      required Duration duration,
      Uint8List? imageBytes,
      List<Uint8List>? images,
      GameLevel level,
      bool cheaterMode,
      bool isWon}) = _$_GamePlayViewState;
  const _GamePlayViewState._() : super._();

  @override
  int get counter;
  @override
  GameMode get gameMode;
  @override
  Duration get duration;
  @override
  Uint8List? get imageBytes;
  @override
  List<Uint8List>? get images;
  @override
  GameLevel get level;
  @override
  bool get cheaterMode;
  @override
  bool get isWon;
  @override
  @JsonKey(ignore: true)
  _$GamePlayViewStateCopyWith<_GamePlayViewState> get copyWith =>
      throw _privateConstructorUsedError;
}
