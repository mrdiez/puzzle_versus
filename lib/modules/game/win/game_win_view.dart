import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/modules/game/win/game_win_logic.dart';
import 'package:puzzle_versus/modules/game/win/widgets/game_win_app_bar.dart';
import 'package:puzzle_versus/modules/game/win/widgets/game_win_footer.dart';
import 'package:puzzle_versus/modules/game/win/widgets/game_win_main.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/layout/sliver.view.dart';

part 'game_win_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GameWinViewState with _$GameWinViewState {
  const factory GameWinViewState({
    required int counter,
    required Duration duration,
    String? name,
    required GameMode gameMode,
    @Default(GameLevel.easy) GameLevel level,
    @Default(false) bool cheaterMode,
  }) = _GameWinViewState;

  const GameWinViewState._();
}

class GameWinView extends ConsumerWidget {
  const GameWinView(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  void onEvent(BuildContext context, GameWinEvent event) {
    if (event is SendScoreError) {
      context.showErrorSnackBar(context.translation.unableToSendScore);
    } else if (event is ScoreSentSuccess) {
      context.go(gameRoutes[gameMode]!);
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    ref.listen<AsyncValue<GameWinEvent>>(GameWinLogic.eventProvider, (previous, next) => onEvent(context, next.value!));
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
          body: Stack(children: [
        SliverView(
          fillRemaining: true,
          physics: const NeverScrollableScrollPhysics(),
          appBar: GameWinAppBar(gameMode),
          child: GameWinMain(gameMode),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: GameWinFooter(gameMode),
        ),
      ])),
    );
  }
}
