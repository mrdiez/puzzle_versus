// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_win_logic.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GameWinEventTearOff {
  const _$GameWinEventTearOff();

  ScoreSentSuccess scoreSentSuccess() {
    return const ScoreSentSuccess();
  }

  SendScoreError sendScoreError() {
    return const SendScoreError();
  }
}

/// @nodoc
const $GameWinEvent = _$GameWinEventTearOff();

/// @nodoc
mixin _$GameWinEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() scoreSentSuccess,
    required TResult Function() sendScoreError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScoreSentSuccess value) scoreSentSuccess,
    required TResult Function(SendScoreError value) sendScoreError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameWinEventCopyWith<$Res> {
  factory $GameWinEventCopyWith(
          GameWinEvent value, $Res Function(GameWinEvent) then) =
      _$GameWinEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$GameWinEventCopyWithImpl<$Res> implements $GameWinEventCopyWith<$Res> {
  _$GameWinEventCopyWithImpl(this._value, this._then);

  final GameWinEvent _value;
  // ignore: unused_field
  final $Res Function(GameWinEvent) _then;
}

/// @nodoc
abstract class $ScoreSentSuccessCopyWith<$Res> {
  factory $ScoreSentSuccessCopyWith(
          ScoreSentSuccess value, $Res Function(ScoreSentSuccess) then) =
      _$ScoreSentSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class _$ScoreSentSuccessCopyWithImpl<$Res>
    extends _$GameWinEventCopyWithImpl<$Res>
    implements $ScoreSentSuccessCopyWith<$Res> {
  _$ScoreSentSuccessCopyWithImpl(
      ScoreSentSuccess _value, $Res Function(ScoreSentSuccess) _then)
      : super(_value, (v) => _then(v as ScoreSentSuccess));

  @override
  ScoreSentSuccess get _value => super._value as ScoreSentSuccess;
}

/// @nodoc

class _$ScoreSentSuccess implements ScoreSentSuccess {
  const _$ScoreSentSuccess();

  @override
  String toString() {
    return 'GameWinEvent.scoreSentSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is ScoreSentSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() scoreSentSuccess,
    required TResult Function() sendScoreError,
  }) {
    return scoreSentSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
  }) {
    return scoreSentSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
    required TResult orElse(),
  }) {
    if (scoreSentSuccess != null) {
      return scoreSentSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScoreSentSuccess value) scoreSentSuccess,
    required TResult Function(SendScoreError value) sendScoreError,
  }) {
    return scoreSentSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
  }) {
    return scoreSentSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
    required TResult orElse(),
  }) {
    if (scoreSentSuccess != null) {
      return scoreSentSuccess(this);
    }
    return orElse();
  }
}

abstract class ScoreSentSuccess implements GameWinEvent {
  const factory ScoreSentSuccess() = _$ScoreSentSuccess;
}

/// @nodoc
abstract class $SendScoreErrorCopyWith<$Res> {
  factory $SendScoreErrorCopyWith(
          SendScoreError value, $Res Function(SendScoreError) then) =
      _$SendScoreErrorCopyWithImpl<$Res>;
}

/// @nodoc
class _$SendScoreErrorCopyWithImpl<$Res>
    extends _$GameWinEventCopyWithImpl<$Res>
    implements $SendScoreErrorCopyWith<$Res> {
  _$SendScoreErrorCopyWithImpl(
      SendScoreError _value, $Res Function(SendScoreError) _then)
      : super(_value, (v) => _then(v as SendScoreError));

  @override
  SendScoreError get _value => super._value as SendScoreError;
}

/// @nodoc

class _$SendScoreError implements SendScoreError {
  const _$SendScoreError();

  @override
  String toString() {
    return 'GameWinEvent.sendScoreError()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is SendScoreError);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() scoreSentSuccess,
    required TResult Function() sendScoreError,
  }) {
    return sendScoreError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
  }) {
    return sendScoreError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? scoreSentSuccess,
    TResult Function()? sendScoreError,
    required TResult orElse(),
  }) {
    if (sendScoreError != null) {
      return sendScoreError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ScoreSentSuccess value) scoreSentSuccess,
    required TResult Function(SendScoreError value) sendScoreError,
  }) {
    return sendScoreError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
  }) {
    return sendScoreError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ScoreSentSuccess value)? scoreSentSuccess,
    TResult Function(SendScoreError value)? sendScoreError,
    required TResult orElse(),
  }) {
    if (sendScoreError != null) {
      return sendScoreError(this);
    }
    return orElse();
  }
}

abstract class SendScoreError implements GameWinEvent {
  const factory SendScoreError() = _$SendScoreError;
}
