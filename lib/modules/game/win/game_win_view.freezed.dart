// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'game_win_view.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$GameWinViewStateTearOff {
  const _$GameWinViewStateTearOff();

  _GameWinViewState call(
      {required int counter,
      required Duration duration,
      String? name,
      required GameMode gameMode,
      GameLevel level = GameLevel.easy,
      bool cheaterMode = false}) {
    return _GameWinViewState(
      counter: counter,
      duration: duration,
      name: name,
      gameMode: gameMode,
      level: level,
      cheaterMode: cheaterMode,
    );
  }
}

/// @nodoc
const $GameWinViewState = _$GameWinViewStateTearOff();

/// @nodoc
mixin _$GameWinViewState {
  int get counter => throw _privateConstructorUsedError;
  Duration get duration => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  GameMode get gameMode => throw _privateConstructorUsedError;
  GameLevel get level => throw _privateConstructorUsedError;
  bool get cheaterMode => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GameWinViewStateCopyWith<GameWinViewState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GameWinViewStateCopyWith<$Res> {
  factory $GameWinViewStateCopyWith(
          GameWinViewState value, $Res Function(GameWinViewState) then) =
      _$GameWinViewStateCopyWithImpl<$Res>;
  $Res call(
      {int counter,
      Duration duration,
      String? name,
      GameMode gameMode,
      GameLevel level,
      bool cheaterMode});
}

/// @nodoc
class _$GameWinViewStateCopyWithImpl<$Res>
    implements $GameWinViewStateCopyWith<$Res> {
  _$GameWinViewStateCopyWithImpl(this._value, this._then);

  final GameWinViewState _value;
  // ignore: unused_field
  final $Res Function(GameWinViewState) _then;

  @override
  $Res call({
    Object? counter = freezed,
    Object? duration = freezed,
    Object? name = freezed,
    Object? gameMode = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
  }) {
    return _then(_value.copyWith(
      counter: counter == freezed
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      gameMode: gameMode == freezed
          ? _value.gameMode
          : gameMode // ignore: cast_nullable_to_non_nullable
              as GameMode,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$GameWinViewStateCopyWith<$Res>
    implements $GameWinViewStateCopyWith<$Res> {
  factory _$GameWinViewStateCopyWith(
          _GameWinViewState value, $Res Function(_GameWinViewState) then) =
      __$GameWinViewStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int counter,
      Duration duration,
      String? name,
      GameMode gameMode,
      GameLevel level,
      bool cheaterMode});
}

/// @nodoc
class __$GameWinViewStateCopyWithImpl<$Res>
    extends _$GameWinViewStateCopyWithImpl<$Res>
    implements _$GameWinViewStateCopyWith<$Res> {
  __$GameWinViewStateCopyWithImpl(
      _GameWinViewState _value, $Res Function(_GameWinViewState) _then)
      : super(_value, (v) => _then(v as _GameWinViewState));

  @override
  _GameWinViewState get _value => super._value as _GameWinViewState;

  @override
  $Res call({
    Object? counter = freezed,
    Object? duration = freezed,
    Object? name = freezed,
    Object? gameMode = freezed,
    Object? level = freezed,
    Object? cheaterMode = freezed,
  }) {
    return _then(_GameWinViewState(
      counter: counter == freezed
          ? _value.counter
          : counter // ignore: cast_nullable_to_non_nullable
              as int,
      duration: duration == freezed
          ? _value.duration
          : duration // ignore: cast_nullable_to_non_nullable
              as Duration,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      gameMode: gameMode == freezed
          ? _value.gameMode
          : gameMode // ignore: cast_nullable_to_non_nullable
              as GameMode,
      level: level == freezed
          ? _value.level
          : level // ignore: cast_nullable_to_non_nullable
              as GameLevel,
      cheaterMode: cheaterMode == freezed
          ? _value.cheaterMode
          : cheaterMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_GameWinViewState extends _GameWinViewState {
  const _$_GameWinViewState(
      {required this.counter,
      required this.duration,
      this.name,
      required this.gameMode,
      this.level = GameLevel.easy,
      this.cheaterMode = false})
      : super._();

  @override
  final int counter;
  @override
  final Duration duration;
  @override
  final String? name;
  @override
  final GameMode gameMode;
  @JsonKey()
  @override
  final GameLevel level;
  @JsonKey()
  @override
  final bool cheaterMode;

  @override
  String toString() {
    return 'GameWinViewState(counter: $counter, duration: $duration, name: $name, gameMode: $gameMode, level: $level, cheaterMode: $cheaterMode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _GameWinViewState &&
            const DeepCollectionEquality().equals(other.counter, counter) &&
            const DeepCollectionEquality().equals(other.duration, duration) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.gameMode, gameMode) &&
            const DeepCollectionEquality().equals(other.level, level) &&
            const DeepCollectionEquality()
                .equals(other.cheaterMode, cheaterMode));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(counter),
      const DeepCollectionEquality().hash(duration),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(gameMode),
      const DeepCollectionEquality().hash(level),
      const DeepCollectionEquality().hash(cheaterMode));

  @JsonKey(ignore: true)
  @override
  _$GameWinViewStateCopyWith<_GameWinViewState> get copyWith =>
      __$GameWinViewStateCopyWithImpl<_GameWinViewState>(this, _$identity);
}

abstract class _GameWinViewState extends GameWinViewState {
  const factory _GameWinViewState(
      {required int counter,
      required Duration duration,
      String? name,
      required GameMode gameMode,
      GameLevel level,
      bool cheaterMode}) = _$_GameWinViewState;
  const _GameWinViewState._() : super._();

  @override
  int get counter;
  @override
  Duration get duration;
  @override
  String? get name;
  @override
  GameMode get gameMode;
  @override
  GameLevel get level;
  @override
  bool get cheaterMode;
  @override
  @JsonKey(ignore: true)
  _$GameWinViewStateCopyWith<_GameWinViewState> get copyWith =>
      throw _privateConstructorUsedError;
}
