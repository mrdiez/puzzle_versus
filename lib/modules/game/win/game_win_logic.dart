import 'dart:async';

import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/modules/game/data/score_model.dart';
import 'package:puzzle_versus/modules/game/data/score_network_provider.dart';
import 'package:puzzle_versus/modules/game/play/game_play_logic.dart';
import 'package:puzzle_versus/services/reset_state_trigger.dart';
import 'package:puzzle_versus/utils/string.dart';
import 'package:puzzle_versus/widgets/misc/shake_on_trigger.dart';

import 'game_win_view.dart';

part 'game_win_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class GameWinEvent with _$GameWinEvent {
  const factory GameWinEvent.scoreSentSuccess() = ScoreSentSuccess;
  const factory GameWinEvent.sendScoreError() = SendScoreError;
}

enum GameNameError { minLength, invalidCharacters }

class GameWinLogic extends StateNotifier<GameWinViewState> {
  static StreamController<GameWinEvent>? eventStreamController;

  static final eventProvider = StreamProvider<GameWinEvent>((ref) {
    eventStreamController = StreamController<GameWinEvent>();
    ref.onDispose(eventStreamController!.close);
    return eventStreamController!.stream;
  });

  static final provider = StateNotifierProvider.family<GameWinLogic, GameWinViewState, GameMode>((ref, gameMode) {
    final playState = ref.watch(GamePlayLogic.provider(gameMode));
    return GameWinLogic(
        reader: ref.read,
        initialState: GameWinViewState(
          counter: playState.counter,
          duration: playState.duration,
          level: playState.level,
          gameMode: playState.gameMode,
          cheaterMode: playState.cheaterMode,
        ));
  });

  GameWinLogic({
    required this.reader,
    required GameWinViewState initialState,
  }) : super(initialState);

  final Reader reader;
  final nameMinLength = 3;
  final TextEditingController nameInputController = TextEditingController();
  final FocusNode nameInputFocusNode = FocusNode();
  final GlobalKey<ShakeOnTriggerState> shakerInputKey = GlobalKey<ShakeOnTriggerState>();

  String? get name => state.name;
  set name(String? value) {
    state = state.copyWith(name: value?.trim());
  }

  GameNameError? getNameError() {
    if (state.name.isNullOrEmpty || state.name!.isBlank) return null;
    if (state.name!.length < nameMinLength) return GameNameError.minLength;
    if (!state.name!.isValidName) return GameNameError.invalidCharacters;
    return null;
  }

  void submitName() async {
    if (state.name.isNullOrEmpty || state.name!.isBlank || getNameError() != null) {
      nameInputFocusNode.requestFocus();
      shakerInputKey.currentState?.animate();
    } else {
      try {
        await reader(
          ScoreNetworkProvider.save(
            Score(
              name: nameInputController.text,
              moves: state.counter,
              duration: state.duration,
              level: state.level,
              gameMode: state.gameMode,
              cheater: state.cheaterMode,
            ),
          ).future,
        );
        reader(ResetStateTrigger.provider.notifier).reset();
        eventStreamController?.add(const GameWinEvent.scoreSentSuccess());
      } catch (e) {
        eventStreamController?.add(const GameWinEvent.sendScoreError());
      }
    }
  }
}
