import 'package:fireworks/fireworks.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/win/game_win_logic.dart';
import 'package:puzzle_versus/modules/game/win/widgets/name_input.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/utils/duration.dart';
import 'package:puzzle_versus/widgets/layout/simple_container.dart';
import 'package:puzzle_versus/widgets/texts/custom_text.dart';

class GameWinMain extends StatefulWidget {
  const GameWinMain(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  State<GameWinMain> createState() => _GameWinMainState();
}

class _GameWinMainState extends State<GameWinMain> with SingleTickerProviderStateMixin {
  late final FireworkController _fireworkController;

  @override
  void initState() {
    super.initState();
    _fireworkController = FireworkController(vsync: this)..start();
  }

  @override
  void dispose() {
    _fireworkController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Fireworks(controller: _fireworkController),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: $Padding.xl),
          child: ConstrainedBox(
            constraints: const BoxConstraints.tightFor(width: $MaxWidth.s),
            child: Padding(
              padding: const EdgeInsets.only(top: $Padding.xxl),
              child: Column(
                children: [
                  CustomText(context.translation.youWin, style: context.textTheme.headline2),
                  $Gap.xxl,
                  SizedBox(
                    height: $ItemHeight.m,
                    child: SimpleContainer(
                      color: whiteColor,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: $Padding.m),
                        child: Consumer(builder: (context, ref, child) {
                          final state = ref.read(GameWinLogic.provider(widget.gameMode));
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 3,
                                child: NameInput(widget.gameMode),
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    CustomText(
                                      '${state.counter} ${context.translation.moves(state.counter)}',
                                      style: context.textTheme.subtitle2!.copyWith(color: blackColor),
                                    ),
                                    $Gap.s,
                                    CustomText(
                                      '${state.duration.hoursFirstDigit}${state.duration.hoursSecondDigit}h${state.duration.minutesFirstDigit}${state.duration.minutesSecondDigit}m${state.duration.secondsFirstDigit}${state.duration.secondsSecondDigit}s',
                                      style: context.textTheme.subtitle2!.copyWith(color: blackColor),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          );
                        }),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
