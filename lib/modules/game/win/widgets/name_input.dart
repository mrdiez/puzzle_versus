import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/win/game_win_logic.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/misc/shake_on_trigger.dart';

class NameInput extends ConsumerWidget {
  const NameInput(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final logic = ref.read(GameWinLogic.provider(gameMode).notifier);
    ref.watch(GameWinLogic.provider(gameMode));
    final error = logic.getNameError();
    final errorMsg = error == null
        ? null
        : error == GameNameError.minLength
            ? context.translation.nbCharactersMinimum(logic.nameMinLength)
            : context.translation.notAllowedCharacters;
    return ShakeOnTrigger(
      key: logic.shakerInputKey,
      child: TextField(
        controller: logic.nameInputController,
        focusNode: logic.nameInputFocusNode,
        style: context.textTheme.bodyText1,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(bottom: $Padding.xxs),
          constraints: BoxConstraints.tightFor(height: errorMsg != null ? $InputHeight.m : $InputHeight.s),
          hintText: context.translation.typeYourNameHere,
          hintStyle: context.textTheme.bodyText2,
          errorText: errorMsg,
          border: InputBorder.none,
        ),
        onChanged: (value) => logic.name = value,
        onSubmitted: (_) => logic.submitName(),
      ),
    );
  }
}
