import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/environment.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/widgets/game_app_bar.dart';
import 'package:puzzle_versus/widgets/images/logo.dart';

class GameWinAppBar extends ConsumerWidget {
  const GameWinAppBar(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GameAppBar(
      child: Padding(
        padding: EdgeInsets.only(
          left: $Padding.m,
          top: Environment.isMobile ? $Padding.l : $Padding.m,
          right: $Padding.m,
          bottom: $Padding.m,
        ),
        child: Logo(
            onTap: () {
              context.go(gameRoutes[gameMode]!);
            },
            height: $LogoHeight.xxs),
      ),
    );
  }
}
