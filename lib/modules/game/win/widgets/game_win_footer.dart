import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/game/win/game_win_logic.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/simple_button.dart';

class GameWinFooter extends StatelessWidget {
  const GameWinFooter(this.gameMode, {Key? key}) : super(key: key);

  final GameMode gameMode;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: $Padding.xxl, right: $Padding.xxl, bottom: $Padding.xxl),
      child: ConstrainedBox(
        constraints: const BoxConstraints.tightFor(width: $MaxWidth.s),
        child: Consumer(builder: (context, ref, child) {
          final logic = ref.read(GameWinLogic.provider(gameMode).notifier);
          return Row(
            children: [
              Expanded(
                child: SimpleButton(
                  height: $ButtonHeight.l,
                  onTap: () {
                    if (context.popRoute != null) {
                      context.popRoute!.call();
                    } else {
                      context.go(gameRoutes[gameMode]!);
                    }
                  },
                  text: context.translation.cancel,
                ),
              ),
              $Gap.m,
              Expanded(
                child: SimpleButton(
                  tag: gameMode.toString(),
                  color: gameColors[gameMode],
                  height: $ButtonHeight.l,
                  onTap: () => logic.submitName(),
                  text: context.translation.sendMyScore,
                ),
              ),
            ],
          );
        }),
      ),
    );
  }
}
