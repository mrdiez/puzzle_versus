import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/modules/home/widgets/home_button.dart';
import 'package:puzzle_versus/services/reset_state_trigger.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/images/logo.dart';
import 'package:puzzle_versus/widgets/texts/custom_text.dart';

class HomeView extends ConsumerWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    WidgetsBinding.instance?.addPostFrameCallback((_) => ref.read(ResetStateTrigger.provider.notifier).reset());
    return Scaffold(
        backgroundColor: context.theme.backgroundColor,
        body: Stack(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: FutureBuilder<PackageInfo>(
                  future: PackageInfo.fromPlatform(),
                  builder: (context, snapshot) {
                    return Padding(
                      padding: const EdgeInsets.all($Padding.xxs),
                      child: CustomText('Version: ${snapshot.data?.version}'),
                    );
                  }),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: $Padding.xl),
                child: LayoutBuilder(builder: (context, constraints) {
                  final availableHeight = $Screen.size(constraints.maxHeight);
                  final gap = availableHeight > l
                      ? $Gap.xxxl
                      : availableHeight > m
                          ? $Gap.xl
                          : $Gap.m;
                  return ConstrainedBox(
                    constraints: const BoxConstraints.tightFor(width: $MaxWidth.s),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Flexible(child: Logo(axis: Axis.vertical, height: $LogoHeight.xxl)),
                        gap,
                        HomeButton(
                          gameMode: GameMode.time,
                          title: context.translation.time,
                          subtitle: context.translation.playAgainsTheClock,
                          icon: Icons.timer_outlined,
                        ),
                        gap,
                        HomeButton(
                          gameMode: GameMode.local,
                          title: context.translation.localPlayer,
                          subtitle: context.translation.playTogetherOnSameDevice,
                          icon: Icons.supervisor_account_rounded,
                        ),
                        gap,
                        HomeButton(
                          gameMode: GameMode.online,
                          title: context.translation.onlinePlayer,
                          subtitle: context.translation.playVersusWorldsPlayers,
                          icon: Icons.connect_without_contact_outlined,
                        ),
                        gap,
                      ],
                    ),
                  );
                }),
              ),
            ),
          ],
        ));
  }
}
