import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:puzzle_versus/config/enums.dart';
import 'package:puzzle_versus/config/routes.dart';
import 'package:puzzle_versus/config/theme/colors.dart';
import 'package:puzzle_versus/config/theme/sizes.dart';
import 'package:puzzle_versus/utils/context.dart';
import 'package:puzzle_versus/widgets/buttons/animated_button.dart';
import 'package:puzzle_versus/widgets/texts/custom_text.dart';

class HomeButton extends StatelessWidget {
  const HomeButton(
      {Key? key, this.onTap, required this.title, required this.gameMode, required this.subtitle, required this.icon})
      : super(key: key);

  final GameMode gameMode;
  final String title;
  final String subtitle;
  final Function()? onTap;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return AnimatedButton(
        tag: gameMode.toString(),
        color: gameColors[gameMode],
        onTap: () => context.go(gameRoutes[gameMode]!),
        builder: (context, isHovered, isClicked) {
          return Padding(
              padding: const EdgeInsets.all($Padding.m),
              child: Row(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      physics: const NeverScrollableScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            title.toUpperCase(),
                            style: context.textTheme.subtitle1!.copyWith(color: isHovered ? blackColor : null),
                          ),
                          CustomText(subtitle, style: context.textTheme.caption),
                        ],
                      ),
                    ),
                  ),
                  Icon(icon, size: $Icon.xl),
                ],
              ));
        });
  }
}
