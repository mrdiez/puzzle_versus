import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:puzzle_versus/config/environment.dart';

class NetworkApi<T> {
  NetworkApi({Dio? client, required this.decode, required this.encode, this.path})
      : client = client ?? Dio(BaseOptions(baseUrl: Environment.baseUrl));

  final Dio client;

  final String? path;
  final T Function(Map<String, dynamic> data) decode;
  final Map<String, dynamic> Function(T data) encode;

  final headers = {
    HttpHeaders.acceptHeader: 'json/application/json',
    HttpHeaders.contentTypeHeader: 'application/x-www-form-urlencoded'
  };

  Future<T?> get(int id, [String? _path]) async {
    final url = '${Environment.baseUrl}${path ?? ''}${_path ?? ''}/$id';
    debugPrint('$runtimeType get $id\n$url');
    try {
      final response = await client.get(url, options: Options(headers: headers));
      checkResponse(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      debugPrint('$runtimeType get response: $responseData');
      return decode(responseData);
    } catch (e, stackTrace) {
      debugPrint('$runtimeType get $id ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<List<T>> getMany([String? _path]) async {
    final url = '${Environment.baseUrl}${path ?? ''}${_path ?? ''}';
    debugPrint('$runtimeType getMany\n$url');
    try {
      final response = await client.get(url, options: Options(headers: headers));
      checkResponse(response);
      List<dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData.map((d) => decode(d as Map<String, dynamic>)).toList();
    } catch (e, stackTrace) {
      debugPrint('$runtimeType getMany ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  Future<Map<String, dynamic>> post(Map<String, dynamic> data, [String? _path]) async {
    final url = '${Environment.baseUrl}${path ?? ''}${_path ?? ''}';
    debugPrint('$runtimeType post: $data\n$url');
    try {
      final response = await client.post(url, data: data, options: Options(headers: headers));
      checkResponse(response);
      Map<String, dynamic> responseData = response.data is String ? json.decode(response.data) : response.data;
      return responseData;
    } catch (e, stackTrace) {
      debugPrint('$runtimeType post: $data ERROR:\n\n$e\n\n$stackTrace');
      rethrow;
    }
  }

  void checkResponse(Response response) {
    if (response.statusCode != 200) {
      throw 'Status: ${response.statusCode}\nResponse:${response.statusMessage}';
    }
  }
}
