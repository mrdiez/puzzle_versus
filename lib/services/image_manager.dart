import 'dart:math';
import 'dart:typed_data';

import 'package:image/image.dart' as img_lib;

abstract class ImageManager {
  static img_lib.Image limitImageSize(int maxSize, img_lib.Image image) {
    int size = max(image.width, image.height);
    if (size > maxSize) {
      if (image.width > image.height) {
        image = img_lib.copyResize(image, width: maxSize);
      } else {
        image = img_lib.copyResize(image, height: maxSize);
      }
    }
    return image;
  }

  /// [params] are given as list in order to be able to run this function in an isolate
  /// [params] 0 is max size in pixels
  /// [params] 1 is image as bytes
  static Uint8List limitImageBytesSize(List<Object?> params) {
    int maxSize = params[0] as int;
    Uint8List imageBytes = params[1] as Uint8List;
    img_lib.Image image = limitImageSize(maxSize, img_lib.decodeImage(imageBytes)!);
    return Uint8List.fromList(img_lib.encodeJpg(image));
  }

  static img_lib.Image imageAsSquare(img_lib.Image image) {
    if (image.width > image.height) {
      final spaceAround = (image.width - image.height) ~/ 2;
      image = img_lib.copyCrop(image, spaceAround, 0, image.height, image.height);
    } else if (image.height > image.width) {
      final spaceAround = (image.height - image.width) ~/ 2;
      image = img_lib.copyCrop(image, 0, spaceAround, image.width, image.width);
    }

    return image;
  }

  /// [params] are given as list in order to be able to run this function in an isolate
  /// [params] 0 is grid axis count
  /// [params] 1 is image as bytes
  static List<Uint8List> imageAsGrid(List<Object?> params) {
    int axisCount = params[0] as int;
    Uint8List imageBytes = params[1] as Uint8List;
    img_lib.Image image = imageAsSquare(img_lib.decodeImage(imageBytes)!);

    final int width = image.width ~/ axisCount;
    final int height = image.height ~/ axisCount;

    List<img_lib.Image> images = <img_lib.Image>[];

    for (int y = 0; y < axisCount; y++) {
      for (int x = 0; x < axisCount; x++) {
        images.add(
          img_lib.copyCrop(image, width * x, height * y, width, height),
        );
      }
    }

    return images.map((i) => Uint8List.fromList(img_lib.encodeJpg(i))).toList();
  }
}
