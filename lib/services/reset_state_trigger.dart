import 'package:flutter_riverpod/flutter_riverpod.dart';

class ResetStateTrigger extends StateNotifier<int> {
  static final provider = StateNotifierProvider<ResetStateTrigger, int>((ref) => ResetStateTrigger());
  ResetStateTrigger() : super(0);
  void reset() => state++;
}
