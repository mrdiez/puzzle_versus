# puzzle_versus

The flutter puzzle challenge

- PWA available here: https://puzzle-versus.herokuapp.com/
- APK available here: https://puzzle-versus.herokuapp.com/apk
- MOCKED APK available here: https://puzzle-versus.herokuapp.com/apk/mocked


## ANDROID STUDIO SETUP

### Install mandatory and useful plugins
- Go to Preferences/Plugins/Marketplace
- And type Flutter in the search bar
- And install the Flutter plugin
- Optional: install Flutter intl, snippets, enhancement suite & asset auto completion

### Enable auto formatting
- Go to Preferences/Languages and frameworks/Flutter
- And select both Format code on save & Organize imports on save

### Increase line length format rule
- Go to Preferences/Editor/Code Style/Dart
- And increase line length to 120

### Hide generated files
- Go to Preferences/Editor/File Types/Ignore files and folders
- And add "*.freezed.dart;*.g.dart;*.gr.dart;"

### Coding rules
- Configure your rules by editing ./analysis_options.yaml
- You can see that they're coming from the plugin pedantic
- And they're disabled for generated files


## ENVIRONMENT CONFIGURATION

### Switch endpoints
Environment define endpoints (url) where our app retrieve data from servers but while
developing we don't want to use productions servers in order to use them only when our
app is ready for prod.

These endpoints are managed into ./lib/constants/config/environment.dart

### To use this project
You will need to add "MOCK" configuration
OR you can edit ./lib/constants/config/environment.dart and force Mock property to true

### Add a configuration

- Go to Run/Edit Configurations
- Click the + button then choose Flutter
- Into the "Dart entrypoint" field type the path of the ./lib/main.dart file
- Into the "Additional run args" field type some arguments if needed
- For example: "--dart-define=MOCK=true" to activate mocks
- Or "--dart-define=PROD=true" to use PROD endpoints


## PACKAGES & PLUGINS

All external dependencies needed to build and run this app are listed in
the ./pubspec.yaml file
In order to install them, just execute:
- flutter pub get


## CONTINUOUS INTEGRATION

### Version control
Manage your project with your favorite tool: Github, Gitlab, Bitbucket, etc...
Whatever if you use your terminal, your IDE or a dedicated software

### Automated Testing, Build & Deployment
I suggest you to use Codemagic that allow to test, build and publish apps
automatically without effort
Check the guide that I wrote here:
- ./doc/Flutter_Apps_&_Codemagic.pdf


## CODE GENERATION

In order to create immutable objects with functions like
copyWith, toJson, fromJson
And to add dynamic methods and properties for states and events
We need to write a lot of boilerplate code
This is why some code generations plugins are used here

### How to generate code (with freezed)
- Configure your generator by editing ./build.yaml
- For full code generation documentation check: https://pub.dev/packages/freezed
- And this to understand why we'll use it: https://developer.school/how-to-use-freezed-with-flutter/
- Increase SDK version into ./pubspec.yaml: sdk: ">=2.13.0 <3.0.0"
- Don't forget to add: part 'filename.freezed.dart';
- To generate fromJson & toJson methods add: part 'filename.g.dart'; and the .fromJson factory
- Then execute: flutter pub run build_runner build --delete-conflicting-outputs
- You can also run a listener to generate code on file changes:
- flutter pub run build_runner watch --delete-conflicting-outputs


## TRANSLATIONS

### Setup Intl package (for a new project)
- Go to Preferences/Plugins/Marketplace and install Flutter intl package
- Add these lines at the end of ./pubspec.yaml:
  "flutter_intl:
  enabled: false    # Will be set to true after initialization
  class_name: Translation   # The class name to access translations ("S" by default)
  arb_dir: lib/constants/translations/l10n  # The translations folder
  output_dir: lib/constants/translations/generated    # The generated translations folder"
- Go to Tools/Flutter Intl and click "Initialize for the project"
- Add these properties to your ./lib/main.dart MaterialApp widget:
  "localizationsDelegates: [
  Translation.delegate, // Translation is the class name defined at previous step
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  GlobalCupertinoLocalizations.delegate,
  ],
  supportedLocales: Translation.delegate.supportedLocales,"
- Then follow the plugin setup here: https://plugins.jetbrains.com/plugin/13666-flutter-intl


## ICON & SPLASHSCREEN

### Setup icon and splash
In order to edit/add the app icon and/or splashscreen you just have to replace icon.png and
splash.png in the assets/launcher folder with your files.

Then run these commands:
- flutter pub run flutter_launcher_icons:main
- flutter pub pub run flutter_native_splash:create


## SOFTWARE ARCHITECTURE

### Once upon a time
Flutter don't have a specific architecture and 4 years ago (2018) best practices did not
exist but fortunately we've seen some packages and plugins coming to solve this problem
Whatever the chosen pattern we can do the same product so the best pattern
is the one where you and your dev team are comfortable
So I started with the pattern MVC that has proven itself and with the Provider
package because it was the first one approved by the Flutter team. Also to reduce
boilerplate code I created my own plugin: mvcprovider

### Lessons learned
By starting new projects I questioned my approach and then I realised that an
MVVM architecture should accelerate my developments by avoiding
manual refreshes of the view from another screen. By coupling this pattern
with immutable states and objects I also don't have to be worried about who is
using my data and I improve performances

### Design Patterns
So I had two choices:
- I implement manually the pattern with Provider or maybe another dependency
  injection package like Mobx or Riverpod
- I follow the public opinion in wonderland and I use the BLOC pattern with
  the BLOC package that already implement this concept for me

### Chosen solution
I choose to implement the BLOC pattern but with Riverpod :D


## FILE STRUCTURE

### Traditional MVVM
In theory ViewModels or "Blocs" can be shared across multiple views this is why
traditional Bloc projects have a layered file structure likes this:
- lib/
  - viewModels
  - models
  - screens
  - ...
    This approach allow us to get our code and logic going into the folder
    corresponding to layers "presentation", "business logic" and "data access"
    Feature are scattered across different layers of the app, hence it could
    be laborious to extract this feature in order to reuse it elsewhere
    When app will grow these folders will overflow and it will be hard to maintain
    or just find a piece of code

### Modular approach
In order to resolve this problem and keep a layered architecture we will
split logic into features and/or modules:
- lib/
  - modules/
    - my_module/
      - logic
      - model
      - ui
    - my_feature/
      - logic
      - model
      - ui
    - ...
  - ...

### Flutter structure

- .dart_tool        // Folder, application dependencies location and version
- .git              // Hidden folder, created by git to manage versioning
- .idea             // Folder, Android Studio parameters dedicated to the project
- android           // Folder, Android skeleton application
- assets            // Folder, images, raw data, videos, etc.
- build             // Folder, dependencies and generated applications
- doc               // Folder, all documentation related to this project
- ios               // Folder, iOS skeleton application
- lib               // Folder, the application, this is where we develop in .dart
- test              // File, unit tests and widget tests
- test_driver       // Folder, integration tests
- web               // Folder, web skeleton application
- .gitignore        // File, content not to be sent to git
- .metadata         // File, info on the version of Flutter, allows the updates
- .packages         // File, application dependencies location and version
- analysis_options.yaml // File, code analyser configuration
- build.yaml        // File, code generation configuration
- project_name.iml  // File, Android Studio dependencies for this project
- Flutter_01.log    // File, Flutter log file
- pubspec.lock      // File, detailed application dependencies
- pubspec.yaml      // File, editable application dependencies
- README.md         // File, general application documentation

### ./lib structure

- config            // Folder, environment, routes, assets, etc...
- translations      // Folder, translations an related generated classes
- theme             // Folder, theming, sizing and colors
- data              // Folder, dao, models, interfaces ...
- modules           // Folder, all features and modules of the app
- services          // Folder, isolated and/or common features
- utils             // Folder, practical utilities and type extensions
- widgets           // Folder, UI components
- main.dart         // File, application root widget


## HOW TO DEV (HOW TO ADD A MODULE)

* Create your module folder like: ./lib/modules/**/your_module
* Create two files:
  - ./lib/modules/**/your_module/your_module_logic.dart
  - ./lib/modules/**/your_module/your_module_view.dart
* Fill the logic file with:
```
import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'your_module_view.dart';

part 'your_module_logic.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class YourModuleEvent with _$YourModuleEvent {
  const factory YourModuleEvent.max([String? message]) = YourModuleEventMax;
}

class YourModuleLogic extends StateNotifier<YourModuleViewState> {
  static StreamController<YourModuleEvent>? eventStreamController;

  static final eventProvider = StreamProvider<YourModuleEvent>((ref) {
    eventStreamController = StreamController<YourModuleEvent>();
    ref.onDispose(eventStreamController!.close);
    return eventStreamController!.stream;
  });

  static final provider = StateNotifierProvider.family<YourModuleLogic, YourModuleViewState, int>(
      (ref, maxCounterValue) =>
          YourModuleLogic(initialState: const YourModuleViewState(counter: 0), maxCounterValue: maxCounterValue));

  YourModuleLogic({required YourModuleViewState initialState, required this.maxCounterValue}) : super(initialState);

  final int maxCounterValue;

  void incrementCounter() {
    if (state.counter >= maxCounterValue) {
      eventStreamController!.add(const YourModuleEvent.max('OMFG you reached max counter value'));
    } else {
      state = state.copyWith(counter: state.counter + 1);
    }
  }
}
```
* Fill the view file with:
```
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'your_module_logic.dart';

part 'your_module_view.freezed.dart';

/// To generate @freezed classes run:
/// flutter pub run build_runner build --delete-conflicting-outputs
@freezed
class YourModuleViewState with _$YourModuleViewState {
  const factory YourModuleViewState({
    required int counter,
  }) = _YourModuleViewState;

  const YourModuleViewState._();
}

class YourModuleView extends ConsumerWidget {
  const YourModuleView({Key? key, required this.maxCounterValue}) : super(key: key);

  final int maxCounterValue;

  void onEvent(YourModuleEvent e) {
    if (e is YourModuleEventMax && e.message != null) {
      print(e.message);
    }
  }

  @override
  Widget build(BuildContext context, ref) {
    final logic = ref.read(YourModuleLogic.provider(maxCounterValue).notifier);
    ref.listen<AsyncValue<YourModuleEvent>>(YourModuleLogic.eventProvider, (previous, next) => onEvent(next.value!));
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: logic.incrementCounter,
        child: const Text('+'),
      ),
      body: Center(
        child: Consumer(
          builder: (context, ref, child) {
            final state = ref.watch(YourModuleLogic.provider(maxCounterValue));
            return Text('Counter: ${state.counter}');
          },
        ),
      ),
    );
  }
}
```
* Add a route to ./lib/config/routes.dart:
```
...
const yourModuleRoute = '/your_module';
...
final routes = GoRouter(
  routes: [
    ...,
    GoRoute(
      path: yourModuleRoute,
      builder: (context, state) => const YourModuleView(maxCounterValue: 10),
    ),
  ],
);
```
* Then reach this route :)
